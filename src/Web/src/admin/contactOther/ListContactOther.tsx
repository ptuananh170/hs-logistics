import { useEffect, useState } from 'react';
import { Table, Space, Button, Pagination, Modal, Typography } from 'antd';
import type { TableColumnsType } from 'antd';
import './index.scss';
import { axiosInstance } from '../../service/axios-config';
import { toast } from 'react-toastify';
import { cookie } from '@/common/helpers/cookie/cookie';
import axios from 'axios';

const ListContactOther = () => {
  const columns: TableColumnsType<any> = [
    {
      title: 'First Name',
      dataIndex: 'firstName',
    },
    {
      title: 'Last Name',
      dataIndex: 'lastName',
    },
    {
      title: 'Email Address',
      dataIndex: 'emailAddress',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phoneNumber',
    },
    {
      title: 'Company',
      dataIndex: 'company',
    },
    {
      title: 'Level',
      dataIndex: 'level',
    },
    {
      title: 'Subject',
      dataIndex: 'subject',
    },
    {
      title: 'Note',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => showModal(record.notes)}>view Notes</a>
        </Space>
      ),
    },
    {
      title: '',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => showDeleteModal(record.id)} style={{ color: "red" }}>Delete</a>
        </Space>
      ),
    },
  ];
  const [datas, setDatas] = useState<any>();
  const [pageSize, setPageSize] = useState<any>(10);
  const [pageIndex, setPageIndex] = useState<any>(1);
  const [total, setTotal] = useState<any>(0)
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [idDelete, setIdDelete] = useState<any>();
  const [isModalDeleteOpen, setModalDeleteOpen] = useState<any>();
  const [message, setMessage] = useState<any>();
  const getLists = async () => {
    const res: any = await axiosInstance.post('/Contact/all-contacts', {
      pageIndex: pageIndex,
      pageSize: pageSize
    });
    setDatas(res.values);
    setTotal(res.totalCount);
  }

  useEffect(() => {
    getLists();
  }, [pageIndex, pageSize])

  const showModal = (message: any) => {
    setMessage(message);
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const showDeleteModal = (id: any) => {
    setIdDelete(id);
    setModalDeleteOpen(true);
  };

  const handleDelteOk = () => {
    handleDelete();
    setModalDeleteOpen(false);
  };

  const handleDeleteCancel = () => {
    setModalDeleteOpen(false);
  };

  const handleDelete = async () => {
    try {
      await axiosInstance.delete('/Contact/delete-contact/' + idDelete);
      getLists();
      toast.success("Delete success!")
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className='admin-list-post-container'>
      <Typography.Title level={3}>List Contact</Typography.Title>
      <Table columns={columns} dataSource={datas} pagination={false} />
      {total > 0 && <Pagination
        style={{ marginTop: "10px" }}
        defaultCurrent={pageIndex}
        pageSize={pageSize}
        total={total}
        onChange={(page, size) => {
          if (page != pageIndex) {
            setPageIndex(page);
          }
          if (pageSize != size) {
            setPageSize(pageSize)
          }
        }}
      />}
      <Modal title="Message"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText={"Cancel"}
        okText={"Ok"}
      >
        <p>{message}</p>
      </Modal>
      <Modal title="Message"
        open={isModalDeleteOpen}
        onOk={handleDelteOk}
        onCancel={handleDeleteCancel}
        cancelText={"Cancel"}
        okText={"Ok"}
      >
        <p>{'You want delete this user career?'}</p>
      </Modal>
    </div>
  );
};
export default ListContactOther;