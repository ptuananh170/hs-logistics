import { Layout, Button, Row, Col, Typography, Form, Input } from 'antd';
import styles from './login.module.scss';
import logo from '@/common/assets/svg/logocolor.png';
import { axiosInstance } from '@/service/axios-config';
import { toast } from 'react-toastify';
import { cookie } from '@/common/helpers/cookie/cookie';
import { useNavigate } from 'react-router-dom';
const { Title } = Typography;
const { Content } = Layout;

function SignIn() {
  const nagigate = useNavigate();
  const onFinish = async (values: A) => {
    try {
      const res: any = await axiosInstance.post('/User/login', values);
      if (res.statusCode == 200) {
        toast.success("Login success!")
        cookie.setCookie('accessToken', res.data, 1);
        cookie.setCookie('email', values.email, 1);
        nagigate('/admin')
      } else {
        toast.error("Your password or email is incorrect. Please try again!")
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className={styles.adminLayout}>
      <Layout className="layout-default layout-signin">
        <Content className="signin">
          <Row gutter={[24, 0]} justify="space-around">
            <Col xs={{ span: 24, offset: 0 }} lg={{ span: 6, offset: 2 }} md={{ span: 12 }}>
              <Title className="mb-15">Login Admin</Title>
              <Form onFinish={onFinish} layout="vertical" className="row-col">
                <Form.Item
                  className="username"
                  label="Email"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your email!'
                    }
                  ]}
                >
                  <Input placeholder="Email" />
                </Form.Item>

                <Form.Item
                  className="username"
                  label="Password"
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!'
                    }
                  ]}
                >
                  <Input.Password placeholder="Password" />
                </Form.Item>

                <Form.Item>
                  <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
                    Login
                  </Button>
                </Form.Item>
              </Form>
            </Col>
            <Col className="sign-img" style={{ padding: 12 }} xs={{ span: 24 }} lg={{ span: 12 }} md={{ span: 12 }}>
              <img src={logo} alt="" />
            </Col>
          </Row>
        </Content>
      </Layout>
    </div>
  );
}
export default SignIn;

