import { useEffect, useState } from 'react';
import { Table, Space, Button, Pagination, Modal, Typography } from 'antd';
import type { TableColumnsType } from 'antd';
import './listpost.scss';
import { axiosInstance } from '../../service/axios-config';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const ListPost = () => {
  const columns: TableColumnsType<any> = [
    {
      title: 'Title',
      dataIndex: 'title',
    },
    {
      title: 'Image title',
      key: 'coverImage',
      render: (_, record) => (
        <>
          <img width={50} height={50} src={import.meta.env.VITE_URL_FILE + '/' + record.coverImage} alt={"img"}></img>
        </>
      ),
    },
    {
      title: 'Description',
      dataIndex: 'description',
    },
    {
      title: 'Create Time',
      key: 'publishedTime',
      render: (_, record) => (
        <>
          {record.publishedTime}
        </>
      ),
    },
    {
      title: '',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => navigate('/admin/new/edit/' + record.id)}>Edit</a>
          <a onClick={() => showModal(record.id)} style={{ color: "red" }}>Delete</a>
        </Space>
      ),
    },
  ];
  const navigate = useNavigate();
  const [datas, setDatas] = useState<any>();
  const [pageSize, setPageSize] = useState<any>(10);
  const [pageIndex, setPageIndex] = useState<any>(1);
  const [total, setTotal] = useState<any>(0)
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [idDelete, setIdDelete] = useState<any>();
  const getLists = async () => {
    const res: any = await axiosInstance.post('/News/all-news', {
      pageIndex: pageIndex,
      pageSize: pageSize
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
    setDatas(res.values);
    setTotal(res.totalCount);
  }

  const handleDelete = async () => {
    try {
      await axiosInstance.delete('/News/delete-new/' + idDelete);
      getLists();
      toast.success("Delete success!")
    } catch (error) {
      // console.log(error);
      toast.error('Something is not ok...')
    }
  }

  const showModal = (id: any) => {
    setIdDelete(id);
    setIsModalOpen(true);
  };

  const handleOk = () => {
    handleDelete();
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    getLists();
  }, [pageIndex, pageSize])
  return (
    <div className='admin-list-post-container'>
      <Typography.Title level={3}>List New</Typography.Title>
      <div className='admin-list-post-add'>
        <Button type="primary" onClick={() => navigate("/admin/new/create")}>Add new</Button>
      </div>
      <Table columns={columns} dataSource={datas} pagination={false} />
      {total > 0 && <Pagination
        style={{ marginTop: "10px" }}
        defaultCurrent={pageIndex}
        pageSize={pageSize}
        total={total}
        onChange={(page, size) => {
          if (page != pageIndex) {
            setPageIndex(page);
          }
          if (pageSize != size) {
            setPageSize(pageSize)
          }
        }}
      />}
      <Modal title="Delete new"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText={"Cancel"}
        okText={"Ok"}
      >
        <p>Do you want delete this new?</p>
      </Modal>
    </div>
  );
};
export default ListPost;