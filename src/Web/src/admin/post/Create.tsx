import { Button, Form, Input, Typography, Upload, Modal } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import CKEditorComponent from './CKEditor';
import { useState } from 'react';
import { axiosInstanceForm } from '@/service/axios-config';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const Create = () => {
  const navigate = useNavigate();
  const [body, setBody] = useState();
  const [fileList, setFileList] = useState<any>([]);
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [previewTitle, setPreviewTitle] = useState('');

  const handlePreview = async (file: any) => {
    setPreviewImage(file.thumbUrl || file.url);
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.thumbUrl.substring(file.url.lastIndexOf('/') + 1));
  };

  const beforeUpload = () => {
    return false;
  };

  const handleChange = ({ fileList: newFileList }: any) => setFileList(newFileList.slice(-1));

  const onFinish = async (values: any) => {
    try {
      const formData: any = new FormData();
      formData.append('title', values.title);
      formData.append('description', values.description);
      formData.append('content', body);
      formData.append('coverImage', fileList[0].originFileObj);
      formData.append('isUpdateCoverImage', true);
      if (body == null || body == '') {
        toast.error('Content is required!');
      } else {
        await axiosInstanceForm.post('/News/create-new', formData);
        toast.success('Add new success!');
        navigate('/admin/new');
      }
    } catch (error) {
      // console.log(error);
      toast.error('Something not ok...');
    }
  };
  return (
    <Form layout="vertical" name="basic" style={{ maxWidth: '100%' }} onFinish={onFinish}>
      <Typography.Title level={3}>Create New</Typography.Title>
      <Form.Item label="Title" name="title" rules={[{ required: true, message: 'Please input title' }]}>
        <Input type="text" />
      </Form.Item>

      <Form.Item label="Description" name="description">
        <Input type="text" max={500} />
      </Form.Item>

      <Form.Item
        name="fileImage"
        label="Title Image"
        valuePropName="fileList"
        getValueFromEvent={(e) => {
          if (Array.isArray(e)) {
            return e;
          }
          return e && e.fileList;
        }}
        rules={[{ required: true, message: 'Please upload an image' }]}
      >
        <Upload
          name="image"
          listType="picture-card"
          fileList={fileList}
          beforeUpload={beforeUpload}
          onPreview={handlePreview}
          onChange={handleChange}
        >
          {fileList.length >= 1 ? null : (
            <div>
              <UploadOutlined />
              <div style={{ marginTop: 8 }}>Upload</div>
            </div>
          )}
        </Upload>
      </Form.Item>

      <Form.Item>
        <Typography.Text>Content</Typography.Text>
        <CKEditorComponent body={body} setBody={setBody} />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
      <Modal open={previewVisible} title={previewTitle} footer={null} onCancel={() => setPreviewVisible(false)}>
        <img alt="example" style={{ width: '100%' }} src={previewImage} />
      </Modal>
    </Form>
  );
};

export default Create;
