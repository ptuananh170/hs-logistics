import { Button, Form, Input, Typography, Upload, Modal, Image } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { CloseOutlined } from '@ant-design/icons';
import CKEditorComponent from './CKEditor';
import { useEffect, useState } from 'react';
import { axiosInstance, axiosInstanceForm } from '@/service/axios-config';
import { toast } from 'react-toastify';
import { useNavigate, useParams } from 'react-router-dom';
const Edit = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  useEffect(() => {
    getDetail();
  }, []);
  const getDetail = async () => {
    try {
      const res: any = await axiosInstance.get('/News/get-detail/' + id);
      setEdit(res.data);
      setBody(res.data.content);
    } catch (error) {
      console.log(error);
    }
  };
  const [edit, setEdit] = useState<any>();
  const [body, setBody] = useState();
  const [fileList, setFileList] = useState<any>([]);
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [previewTitle, setPreviewTitle] = useState('');

  const handlePreview = async (file: any) => {
    setPreviewImage(file.thumbUrl || file.url);
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.thumbUrl.substring(file.url.lastIndexOf('/') + 1));
  };

  const beforeUpload = () => {
    return false;
  };

  const handleChange = ({ fileList: newFileList }: any) => setFileList(newFileList.slice(-1));

  const onFinish = async (values: any) => {
    try {
      const formData: any = new FormData();
      formData.append('id', id);
      formData.append('title', values.title);
      formData.append('content', body);
      formData.append('description', values.description);
      !edit.coverImage && formData.append('coverImage', fileList[0].originFileObj);
      !edit.coverImage ? formData.append('isUpdateCoverImage', true) : formData.append('isUpdateCoverImage', false);

      if (body == null || body == '') {
        toast.error('Content is required!');
      } else {
        await axiosInstanceForm.put('/News/update-new', formData);
        toast.success('Edit new success!');
        navigate('/admin/new');
      }
    } catch (error) {
      // console.log(error);
      toast.error('Something not ok...');
    }
  };
  return (
    edit && (
      <Form
        layout="vertical"
        name="basic"
        style={{ maxWidth: '100%' }}
        initialValues={{
          title: edit.title,
          description: edit.description
        }}
        onFinish={onFinish}
      >
        <Typography.Title level={3}>Edit New</Typography.Title>
        <Form.Item label="Title" name="title" rules={[{ required: true, message: 'Please input title' }]}>
          <Input type="text" defaultValue={edit.title} />
        </Form.Item>

        <Form.Item label="Description" name="description">
          <Input type="text" max={500} defaultValue={edit.description} />
        </Form.Item>

        {edit.coverImage ? (
          <Form.Item label="Upload Image">
            <Image
              width={100}
              height={100}
              preview={false}
              src={import.meta.env.VITE_URL_FILE + '/' + edit.coverImage}
              alt="Uploaded Image"
            />
            <Button
              icon={<CloseOutlined />}
              danger
              size="small"
              onClick={() => setEdit({ ...edit, coverImage: null })}
            ></Button>{' '}
          </Form.Item>
        ) : (
          <Form.Item
            name="fileImage"
            label="Title Image"
            valuePropName="fileList"
            getValueFromEvent={(e) => {
              if (Array.isArray(e)) {
                return e;
              }
              return e && e.fileList;
            }}
            rules={[{ required: true, message: 'Please upload an image' }]}
          >
            <Upload
              name="image"
              listType="picture-card"
              fileList={fileList}
              beforeUpload={beforeUpload}
              onPreview={handlePreview}
              onChange={handleChange}
            >
              {fileList.length >= 1 ? null : (
                <div>
                  <UploadOutlined />
                  <div style={{ marginTop: 8 }}>Upload</div>
                </div>
              )}
            </Upload>
          </Form.Item>
        )}

        <Form.Item>
          <Typography.Text>Content</Typography.Text>
          <CKEditorComponent body={body} setBody={setBody} />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
        <Modal open={previewVisible} title={previewTitle} footer={null} onCancel={() => setPreviewVisible(false)}>
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </Form>
    )
  );
};

export default Edit;
