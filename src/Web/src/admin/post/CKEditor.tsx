import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import "./ckeditor.scss";
const CKEditorComponent = (props: { setBody: any; body: any }) => {
  const { setBody, body } = props;

  function uploadAdapter(loader: any) {
    return {
      upload: () => {
        return new Promise((resolve, reject) => {
          loader.file
            .then((file: any) => {
              const reader = new FileReader();
              reader.onloadend = () => {
                const base64String = reader.result;
                resolve({ default: base64String });
              };
              reader.onerror = (error) => reject(error);
              reader.readAsDataURL(file);
            })
            .catch((error: any) => reject(error));
        });
      },
      abort: () => { },
    };
  }

  function uploadPlugin(editor: any) {
    editor.plugins.get("FileRepository").createUploadAdapter = (
      loader: any
    ) => {
      return uploadAdapter(loader);
    };
  }

  const handleEditorChange = (event: any, editor: any) => {
    const data = editor.getData();
    setBody(data);
  };

  return (
    <div style={{ marginTop: "8px" }}>
      <CKEditor
        config={{
          toolbar: {
            items: [
              "heading",
              "|",
              "bold",
              "italic",
              "link",
              "bulletedList",
              "numberedList",
              "|",
              "indent",
              "outdent",
              "|",
              "blockQuote",
              "insertTable",
              "mediaEmbed",
              "undo",
              "redo",
              "|",
              "imageUpload",
              "imageResize"
            ],
          },
          extraPlugins: [uploadPlugin],
        }}
        editor={ClassicEditor}
        data={body}
        onChange={handleEditorChange}
      />
    </div>
  );
};

export default CKEditorComponent;
