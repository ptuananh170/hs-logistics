import { Col, Row, Breadcrumb, Dropdown, Space } from "antd";
import { DownOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { cookie } from "@/common/helpers/cookie/cookie";
import { axiosInstance } from "@/service/axios-config";
const HeaderMain = () => {
  const items: MenuProps['items'] = [
    {
      label: <a href="/admin/signin" onClick={() => Logout()}>Logout</a>,
      key: '0',
    },
  ];

  const Logout = async () => {
    const res: any = await axiosInstance.get('/User/logout');
    cookie.clearCookie('accessToken');
    cookie.clearCookie('email');
  }

  return (
    <div>
      <Row>
        <Col span={21}></Col>
        <Col span={3}>
          <Dropdown menu={{ items }} trigger={["click"]}>
            <div style={{ cursor: "pointer" }} onClick={(e) => e.preventDefault()}>
              <Space>
                {cookie.getCookie('email')}
                <DownOutlined />
              </Space>
            </div>
          </Dropdown>
        </Col>
      </Row>
    </div>
  );
};

export default HeaderMain;
