import type { MenuProps } from "antd";
import { Menu } from "antd";
import { useEffect, useState } from "react";
import logo from '@/common/assets/svg/logocolor.png';
import { useNavigate } from "react-router-dom";
const SideBar = () => {
  const [current, setCurrent] = useState('new');
  const navigate = useNavigate();
  type MenuItem = Required<MenuProps>["items"][number];
  const items: MenuItem[] = [
    {
      key: "new",
      label: 'News',
    },
    // {
    //   key: "userAgent",
    //   label: 'User Agent',
    // },
    {
      key: "userContact",
      label: 'User Contact',
    },
    {
      key: "userCareer",
      label: 'User Career',
    },
    {
      key: "contactOther",
      label: 'Contact',
    },
  ];
  const handleClick: MenuProps['onClick'] = (e) => {
    if (e.key == 'new') {
      navigate("/admin/new");
    // } else if (e.key == 'userAgent') {
    //   navigate("/admin/useragent");
    } else if (e.key == 'userContact') {
      navigate("/admin/usercontact");
    } else if(e.key == 'userCareer') {
      navigate('/admin/usercareer');
    } else if(e.key == 'contactOther') {
      navigate('/admin/contact');
    }
    setCurrent(e.key);
  };
  useEffect(() => {
    // if (window.location.pathname == '/admin/useragent') {
    //   setCurrent('userAgent');
    // } else 
    if (window.location.pathname == '/admin/usercontact') {
      setCurrent('userContact');
    } else if (window.location.pathname == '/admin/usercareer') {
      setCurrent('userCareer');
    } else if (window.location.pathname == '/admin/contact') {
      setCurrent('contactOther');
    }
  }, [window.location.pathname])
  return (
    <div>
      <div style={{ height: "100px", color: "black", textAlign: "center", fontWeight: "bold", fontSize: "20px" }}>
          <img style={{ backgroundColor: 'transparent' }} width="90" height="80" src={logo} alt="Logistic HS" />
      </div>
      <Menu
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        items={items}
        onClick={handleClick}
        selectedKeys={[current]}
      />
    </div>
  );
};

export default SideBar;
