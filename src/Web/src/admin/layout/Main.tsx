import SideBar from "./SideBar";
import { Flex, Layout } from "antd";
import "./main.scss";
import { Outlet } from "react-router-dom";
import HeaderMain from "./HeaderMain";
import { useEffect, useState } from "react";
import { cookie } from "@/common/helpers/cookie/cookie";
const { Header, Sider, Content } = Layout;
const Main = () => {
  const [isLogin, setIsLogin] = useState<any>(null);
  useEffect(() => {
    if (cookie.getCookie('accessToken')) {
      setIsLogin(cookie.getCookie('accessToken'))
    } else {
      setIsLogin(null)
      location.href = '/admin/signin';
    }
  }, [cookie.getCookie('accessToken')])
  return (
    <>
      {isLogin && <Flex gap="middle" wrap>
          <Layout className="admin-main-container">
            <Sider width="15%" className="admin-main-sidebar">
              <SideBar />
            </Sider>
            <Layout>
              <Header className="admin-main-header">
                <HeaderMain />
              </Header>
              <Content className="admin-main-content">
                <Outlet />
              </Content>
            </Layout>
          </Layout>
        </Flex>}
    </>
  );
};

export default Main;
