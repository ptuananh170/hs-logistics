import { useEffect, useState } from 'react';
import { Table, Pagination, Typography, Space, Button } from 'antd';
import type { TableColumnsType } from 'antd';
import './index.scss';
import { axiosInstance } from '../../service/axios-config';
import axios from 'axios';
import { cookie } from '@/common/helpers/cookie/cookie';

const ListContact = () => {
  const columns: TableColumnsType<any> = [
    {
      title: 'Full Name',
      dataIndex: 'fullName',
    },
    {
      title: 'Email Address',
      dataIndex: 'emailAddress',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phoneNumber',
    },
    {
      title: 'Subject',
      dataIndex: 'subject',
    },
    {
      title: 'Pickup City',
      dataIndex: 'pickUpCity',
    },
    {
      title: 'Deliver City',
      dataIndex: 'deliverCity',
    },
    
    {
      title: 'Freight',
      key: 'freight',
      render: (_, record) => (
        <Space size="middle">
          {record.freight == 0 ? 'FCL' : 'LCL'}
        </Space>
      ),
    },
    
    {
      title: 'Incoterms',
      key: 'incoterms',
      render: (_, record) => (
        <Space size="middle">
          {record.incoterms == 0 ? 'EXW' : record.incoterms == 1 ? 'FOB' : 'CIF'}
        </Space>
      ),
    },
    {
      title: 'width',
      dataIndex: 'width',
    },
    {
      title: 'height',
      dataIndex: 'height',
    },
    {
      title: 'length',
      dataIndex: 'length',
    },
    {
      title: 'weight',
      dataIndex: 'weight',
    },
  ];
  const [datas, setDatas] = useState<any>();
  const [pageSize, setPageSize] = useState<any>(10);
  const [pageIndex, setPageIndex] = useState<any>(1);
  const [total, setTotal] = useState<any>(0)
  const getLists = async () => {
    const res: any = await axiosInstance.post('/UserContact/all-user-contacts', {
      pageIndex: pageIndex,
      pageSize: pageSize
    });
    setDatas(res.values);
    setTotal(res.totalCount);
  }

  const downloadExcel = async () => {
    const token: A = cookie.getCookie('accessToken') ?? '';
    let config: any = {
      responseType: 'blob',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${token}`
      }
    }
    try {
      const response: any = await axios.post(
        import.meta.env.VITE_API_URL + '/UserContact/download-excel-user-contacts', 
        { pageIndex: pageIndex, pageSize: pageSize }, config);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'file.xlsx');
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } catch (error) {
      console.error('Error downloading the file:', error);
    }
  };

  useEffect(() => {
    getLists();
  }, [pageIndex, pageSize])

  return (
    <div className='admin-list-post-container'>
      <Typography.Title level={3}>List User Contact</Typography.Title>
      <div className='admin-list-post-add'>
        <Button type="primary" onClick={() => downloadExcel()}>download Excel</Button>
      </div>
      <Table columns={columns} dataSource={datas} pagination={false} />
      {total > 0 && <Pagination
        style={{ marginTop: "10px" }}
        defaultCurrent={pageIndex}
        pageSize={pageSize}
        total={total}
        onChange={(page, size) => {
          if (page != pageIndex) {
            setPageIndex(page);
          }
          if (pageSize != size) {
            setPageSize(pageSize)
          }
        }}
      />}
    </div>
  );
};
export default ListContact;