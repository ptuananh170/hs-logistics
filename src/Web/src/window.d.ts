interface Window {
  axiosPromiseArray?: A[];
  dtQuery: IDtQuery;
  JSEncrypt: A;
}
interface IDtQuery {
  data: (obj: HTMLElement | A, name: string | A, value?: A) => A;
}
