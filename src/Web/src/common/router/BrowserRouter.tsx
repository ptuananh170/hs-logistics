import React, { useRef, useState, FC, useLayoutEffect } from 'react';
import { createBrowserHistory } from 'history';
import type { BrowserHistory } from 'history';
import { Router } from 'react-router';

export interface BrowserRouterProps {
  basename?: string;
  children?: React.ReactNode;
}

const browserHistory = createBrowserHistory({ window });

const BrowserRouter: FC<BrowserRouterProps> = (props) => {
  const { basename, children } = props;
  const historyRef = useRef<BrowserHistory>();
  if (historyRef.current == null) {
    historyRef.current = browserHistory;
  }

  const history = historyRef.current;
  const [state, setState] = useState({
    action: history.action,
    location: history.location
  });

  useLayoutEffect(() => history.listen(setState), [history]);

  return (
    <Router basename={basename} location={state.location} navigationType={state.action} navigator={history}>
      {children}
    </Router>
  );
};

export { browserHistory as history, BrowserRouter };
