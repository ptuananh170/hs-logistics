import './NotWork.scss';

export const NotWorkTemplate: React.FC = () => {
  return (
    <div style={{ paddingBottom : '5em' }}>
      <h1 className="fw-bolder nw">500</h1>
      <h2>Unexpected Error</h2>
      <div className="gears" style={{marginTop : '2em', scale : '70%'}}>
        <div className="gear one">
          <div className="bar"></div>
          <div className="bar"></div>
          <div className="bar"></div>
        </div>
        <div className="gear two">
          <div className="bar"></div>
          <div className="bar"></div>
          <div className="bar"></div>
        </div>
        <div className="gear three">
          <div className="bar"></div>
          <div className="bar"></div>
          <div className="bar"></div>
        </div>
      </div>
    </div>
  );
};
