export const useUtils = () => {
  const createUrl = (url: string, params?: Record<string, undefined | string | number>) => {
    let str = '';
    if (params) {
      Object.keys(params)
        .filter((item) => params[item])
        .forEach((item, index) => {
          str += `${index === 0 ? '?' : '&'}${item}=${params[item]}`;
        });
      return url + str;
    } else {
      return url;
    }
  };

  const debounce = (func: A, wait = 300) => {
    let timeout: ReturnType<typeof setTimeout> | null = null;
    return function (...args: A[]) {
      if (timeout) {
        clearTimeout(timeout);
      }

      timeout = setTimeout(() => {
        func(...args);
      }, wait);
    };
  };

  const isObject = (val: A) => {
    return typeof val === 'object' && val !== null;
  };

  const getLableByValue = (options: SelectOption<string | number>[], targetValue: string | number): string => {
    const label = options.filter((item) => item.value === targetValue)[0]?.label ?? '';
    return label;
  };

  const combineClassNames = (...arg: (string | boolean | undefined)[]): string => {
    /**
     * Use case 1: combineClassNames('class1', 'class2')   => "class1 class2"
     * Use case 2: combineClassNames('class1', some_conditions && 'class2')
     * use case 3: combineClassNames('class1, some_conditions ? 'class2' : 'class3')
     */
    const result = [];
    for (const item of arg) {
      if (typeof item === 'string' && item.trim()) result.push(item);
    }
    return result.join(' ');
  };

  const getQuery = (locationSearch: string) => {
    const strt = locationSearch.substring(locationSearch.indexOf('?') + 1);
    const arr = strt.split('&');
    const result: Record<string, string> = {};
    arr.forEach((item) => {
      const keyValue = item.split('=');
      result[keyValue[0]] = keyValue[1];
    });
    return result;
  };
  const createFormDataByObj = (obj: A): A => {
    const keyList: string[] = Object.keys(obj);
    const fd = new FormData();
    keyList.forEach((key) => {
      if (obj[key]?.constructor === Array) {
        obj[key].forEach((item: A) => {
          fd.append(key, item);
        });
      } else {
        fd.append(key, obj[key]);
      }
    });
    return fd;
  };
  const rsaEncrypt = (Text: string, PublicKey: string): string => {
    const encryptor = new window.JSEncrypt();
    encryptor.setPublicKey(PublicKey);
    return encryptor.encrypt(Text);
  };

  const mathRandom = (function (): (number: number) => number {
    const today = new Date();
    let seed = today.getTime();
    function rnd(): number {
      seed = (seed * 9301 + 49297) % 233280;
      return seed / 233280.0;
    }
    return (number: number): number => rnd() * number;
  })();

  const createUUId = () => {
    return `${Date.now()}${String(mathRandom(66)).replace(/\D/g, '').substring(0, 8)}`;
  };

  return {
    createUUId,
    rsaEncrypt,
    createFormDataByObj,
    debounce,
    getQuery,
    createUrl,
    isObject,
    getLableByValue,
    combineClassNames
  };
};
