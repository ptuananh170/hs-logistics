import { FC } from 'react';
import styles from './ShowMoreButton.module.scss';
import { Popover } from 'antd';
import { PopoverProps } from 'antd/lib';
import { useUtils } from '@/common/utils';

type ShowMoreButtonProps = PopoverProps & {
  number: number;
};

const ShowMoreButton: FC<ShowMoreButtonProps> = ({ number, trigger = 'click', placement = 'right', ...rest }) => {
  const { combineClassNames } = useUtils();
  return (
    <Popover rootClassName={styles.showMorePopOverRoot} trigger={trigger} placement={placement} {...rest}>
      <div className={combineClassNames(styles.showMore, rest.className)}>+{number}</div>
    </Popover>
  );
};

export default ShowMoreButton;
