import { DownOutlined, UpOutlined } from '@ant-design/icons';
import { useEffect, useRef, useState } from 'react';
import styles from './FoldCard.module.scss';

interface IProps {
  title?: string;
  tagName?: React.ReactNode;
  children?: React.ReactNode;
  operate?: React.ReactNode;
  className?: string;
  cardClass?: string;
  titleIcon?: React.ReactNode;
  defaultDisplay?: 'block' | 'none';
  noDisplayBorder?: boolean;
  open?: boolean;
  onChange?: (open?: boolean) => void;
}

const FoldCard = (props: IProps) => {
  const {
    title,
    children,
    operate,
    className,
    titleIcon,
    tagName,
    defaultDisplay = 'block',
    open = true,
    onChange,
    noDisplayBorder = false,
    cardClass
  } = props;
  const [carConDisplay, setCarConDisplay] = useState(defaultDisplay);
  const openCurrentRef = useRef(defaultDisplay);
  const setDisPlay = (value: 'block' | 'none') => {
    setCarConDisplay(value);
    openCurrentRef.current = value;
  };
  useEffect(() => {
    if (open) {
      setDisPlay('block');
    } else {
      setDisPlay('none');
    }
  }, [open]);

  const isShowCon = (e: A) => {
    e.stopPropagation();
    const str = carConDisplay === 'block' ? 'none' : 'block';
    setDisPlay(str);
    onChange?.(openCurrentRef.current === 'block');
  };

  const newClass = className ?? '';
  return (
    <div className={`${styles.dtCard} ${cardClass}`}>
      <div className={`dt-card-header ${noDisplayBorder ? 'dtNoBoderCard' : ''}`} onClick={isShowCon}>
        <div className="dt-card-left">
          <div className="cardTitle dt-two-rows">{title}</div>
          {titleIcon && <div style={{ marginLeft: '0.5rem' }}>{titleIcon}</div>}
        </div>
        <div className="dt-card-right dt-pr-8">
          {tagName && <div style={{ marginLeft: '0.5rem' }}>{tagName}</div>}
          {operate ?? ''}
          {carConDisplay === 'block' ? <UpOutlined className="upIcon" /> : <DownOutlined className="upIcon" />}
        </div>
      </div>
      <div className={`dt-card-con ${newClass}`} style={{ display: carConDisplay }}>
        {children}
      </div>
    </div>
  );
};

export default FoldCard;
