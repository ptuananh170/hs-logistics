export interface MethodType {
  clearSearch: VoidFunction;
}
export interface IProps {
  className?: string;
  placeholder: string;
  search: (val: string) => void;
}
