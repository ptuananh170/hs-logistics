import { Input, Tooltip } from 'antd';
import { forwardRef, useImperativeHandle, useState } from 'react';
import styles from './CommonSearch.module.scss';
import { IProps, MethodType } from './CommonSearch.model';
import { useUtils } from '@/common/utils';

const CommonSearch = forwardRef<MethodType, IProps>((props, ref) => {
  const { combineClassNames } = useUtils();
  const { search, placeholder = 'Tìm kiếm....', className } = props;
  const { Search } = Input;
  const [showTooltip, setShowTooltip] = useState(false);
  const [searchValue, setSearchValue] = useState<string>();
  const onOpenChange = (isOpen: boolean) => {
    if (searchValue && searchValue?.length > 0 && isOpen === true) {
      setShowTooltip(true);
    }
    if (!isOpen) {
      setShowTooltip(false);
    }
  };
  const onSearch = (value: A) => {
    search(value);
  };
  const clearSearch = () => {
    setSearchValue('');
  };
  useImperativeHandle(ref, () => ({ clearSearch }));
  return (
    <Tooltip
      title={<span style={{ color: 'black' }}>{placeholder}</span>}
      color="#fff"
      open={showTooltip}
      onOpenChange={onOpenChange}
      trigger="hover"
    >
      <div className=""></div>
      <Search
        className={combineClassNames(styles.searchInput, className)}
        value={searchValue}
        onChange={(e) => {
          setSearchValue(e.target.value);
        }}
        placeholder={placeholder}
        onSearch={onSearch}
        allowClear
        onMouseEnter={() => setShowTooltip(true)}
        onMouseLeave={() => setShowTooltip(false)}
      />
    </Tooltip>
  );
});

CommonSearch.displayName = 'CommonSearch';

export default CommonSearch;
