declare namespace Components {
  namespace RichText {
    interface RichTextRef {
      setValue: (value: string) => void;
      setEmpty: () => void;
      getValue: () => string;
      isEmpty: () => boolean;
    }
  }
  namespace FormStep {
    interface StepBarItem<T = string> {
      key: T;
      name: string;
      isShow?: boolean;
    }
    interface StepContentProps<T = string> {
      stepKey: T;
    }
    interface StepContentRef {
      getValidateRes: () => boolean | Promise<boolean>;
    }
  }
  namespace FileUpload {
    interface BaseRef {
      setRecordId: (id: string, needRefreshList?: boolean) => void;
      refreshList: () => void;
      manualUpload: () => Promise<boolean>;
      fileList: A[];
    }
    type UploadButtonRef = BaseRef & { setFileList: React.Dispatch<React.SetStateAction<A[]>> };
    type SignatureBoardRef = BaseRef & { resetSignatureBoard: () => void };
    type AvatarUploadRef = BaseRef;
    type UploadPhotoRef = BaseRef & {
      setRecordId: (id: string, needRefreshList?: boolean) => Promise<A[]>;
    };
    type FolderTabRef = BaseRef;
    interface AvatarImageRef {
      refreshList: () => Promise<void>;
    }
    interface DrawBoardRef {
      clear: () => void;
      hasPathPoint: () => boolean;
      getFile: () => File | undefined;
    }

    interface SignatureButtonRef {
      setPreviewFile: (file: File | undefined) => Promise<void>;
      setRecordId: (id?: string, needRefreshList?: boolean) => Promise<void>;
      manualUpload: () => Promise<boolean>;
      fileList: A[];
    }

    interface IRoleUserOptions {
      value: string;
      label: string;
    }
  }
  namespace ViewImage {
    interface ViewImageRef {
      setSelctItemId: (id: string) => void;
    }
  }

  namespace MoreImage {
    interface MoreImageRef {
      reload: () => Promise<void>;
      getList: () => Promise<Partial<FileItem>[]>;
    }
  }
  namespace NumberInput {
    interface INumInputProps {
      value?: number;
      numMax?: number;
      numMin?: number;
      onChange?: (value: number) => void;
    }
  }
}
