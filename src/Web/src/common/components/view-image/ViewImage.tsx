import { Modal, Button, Skeleton } from 'antd';
import { IViewImageProps } from './ViewImage.model';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import styles from './ViewImage.module.scss';
import { useState, forwardRef, useImperativeHandle } from 'react';

const ViewImage = forwardRef<Components.ViewImage.ViewImageRef, IViewImageProps>((props, ref) => {
  const { imageList = [], title, open } = props;
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [showImage, setShowImage] = useState<boolean>(false);

  useImperativeHandle(ref, () => {
    return {
      setSelctItemId: (id: string) => {
        const tempIndex = (imageList ?? []).findIndex((item: A) => item.id === id);
        setCurrentIndex(tempIndex);
      }
    };
  });
  const prevClick = () => {
    setShowImage(false);
    setCurrentIndex((index) => {
      return (index - 1 + imageList.length) % imageList.length;
    });
  };

  const nextClick = () => {
    setShowImage(false);
    setCurrentIndex((index) => {
      return (index + 1) % imageList.length;
    });
  };
  const imageOnLoad = () => {
    setShowImage(true);
  };

  return (
    <Modal
      title={title}
      open={open}
      className={`${styles.viewImage}`}
      centered
      footer={null}
      onCancel={() => {
        props?.onClose?.();
      }}
      width={'50%'}
    >
      <div className="view-image-content">
        <div className="view-image-main">
          {imageList.map(
            (item, idx) =>
              currentIndex === idx && (
                <div className="view-image-item" key={item.id}>
                  <img
                    src={item.url}
                    alt={`image-${item.id}`}
                    className={`${!showImage ? 'dt-none' : ''}`}
                    onLoad={imageOnLoad}
                  />
                  {!showImage && <Skeleton.Image rootClassName={`view-image-skeleton`} active />}
                </div>
              )
          )}
        </div>
        <div className="view-image-footer">
          <Button disabled={imageList.length < 2} onClick={prevClick} icon={<LeftOutlined />}></Button>
          <div>{`${Math.max(0, currentIndex + 1)}/${imageList.length}`}</div>
          <Button disabled={imageList.length < 2} onClick={nextClick} icon={<RightOutlined />}></Button>
        </div>
      </div>
    </Modal>
  );
});
ViewImage.displayName = 'ViewImage';
export default ViewImage;
