export interface IImageItem {
  url: string;
  id: string;
}

export interface IViewImageProps {
  imageList: IImageItem[];
  open?: boolean;
  showId?: string;
  title: string;
  onClose?: VoidFunction;
}
