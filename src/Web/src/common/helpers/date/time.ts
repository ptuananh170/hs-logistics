function padZero(value: any) {
  return value < 10 ? '0' + value : value;
}

export function longToDateTime(long: any) {
  let date = new Date(long);
  let hours = padZero(date.getHours());
  let minutes = padZero(date.getMinutes());
  let seconds = padZero(date.getSeconds());
  let day = padZero(date.getDate());
  let month = padZero(date.getMonth() + 1);
  let year = date.getFullYear();
  return `${hours}:${minutes}:${seconds} ${day}/${month}/${year}`;
}