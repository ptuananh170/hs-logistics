import { Outlet } from 'react-router-dom';
import UserLayout from '@/portal/components/layouts/UserLayout';
import { useLoading } from '@/common/utils';
import LazyLoading from '@/common/components/lazy-loading/LazyLoading';
import 'animate.css';
import '@/common/assets/scss/index.scss';
const Layout = () => {
  const { isLoading } = useLoading();
  return (
    <>
      <UserLayout>
        <Outlet></Outlet>
      </UserLayout>
      {isLoading && <LazyLoading />}
    </>
  );
};

export default Layout;
