import { lazy } from 'react';
import AboutUs from './routers/AboutUsRouter';
import HomeRouter from './routers/HomeRouter';
import LogictisMain from './routers/LogisticRouter';
import AdminRouter from './routers/AdminRouter';
import contactRouter from './routers/ContactRouter';
import insignROuter from './routers/InsignRouter';
import Main from '@/admin/layout/Main';
const NotFound = lazy(() => import('@/common/page/not-found/NotFound'));
const Layout = lazy(() => import('../AppLayout'));
const SingIn = lazy(() => import('../admin/login/Login'));

export const routers: IRouter.IRoute[] = [
  {
    path: '/',
    element: Layout,
    name: 'layout',
    meta: { pageTitle: 'layout' },
    children: [...AboutUs, ...LogictisMain, ...HomeRouter, ...contactRouter, ...insignROuter]
  },
  {
    path: '/admin',
    element: Main,
    name: 'admin',
    meta: { pageTitle: 'admin' },
    children: [...AdminRouter]
  },
  { path: '/404', name: '404page', element: NotFound, meta: { pageTitle: '404' } },
  { path: '/admin/signin', name: 'signin', element: SingIn, meta: { pageTitle: 'signin' } },
  { path: '*', name: '404', element: NotFound, meta: { pageTitle: '404' } }
];
