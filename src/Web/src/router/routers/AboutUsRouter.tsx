import { lazy } from 'react';

const AboutUsMain = lazy(() => import('@/portal/page/about-us/AboutUsMain'));
const Career = lazy(() => import('@/portal/page/about-us/carrer/Carrer'));
const Sustainability = lazy(() => import('@/portal/page/about-us/sustainability/Sustainability'));
const BeOurAgent = lazy(() => import('@/portal/page/about-us/be-our-agent/BeOurAgent'));

const routes: IRouter.IRoute<'/about-us'>[] = [
  {
    path: '/about-us',
    name: 'aboutus',
    exact: true,
    element: AboutUsMain,
    meta: { pageTitle: 'aboutus', leftKey: 'aboutus' }
  },
  {
    path: '/about-us/career',
    name: 'career',
    exact: true,
    element: Career,
    meta: { pageTitle: 'career', leftKey: 'career' }
  },
  {
    path: '/about-us/be-our-agent',
    name: 'be-our-agent',
    exact: true,
    element: BeOurAgent,
    meta: { pageTitle: 'be-our-agent', leftKey: 'be-our-agent' }
  },
  {
    path: '/about-us/sustainability',
    name: 'sustainability',
    exact: true,
    element: Sustainability,
    meta: { pageTitle: 'sustainability', leftKey: 'sustainability' }
  }
];

export default routes;
