import ListArgent from '@/admin/argent/ListArgent';
import ListCarreer from '@/admin/career/ListCarreer';
import ListContact from '@/admin/contact/ListContact';
import ListContactOther from '@/admin/contactOther/ListContactOther';
import Create from '@/admin/post/Create';
import Edit from '@/admin/post/Edit';
import ListPost from '@/admin/post/ListPost';
const routes: IRouter.IRoute<'/'>[] = [
  {
    path: '/admin/new',
    name: 'admin-new',
    exact: true,
    element: ListPost,
    meta: { pageTitle: 'admin-new', leftKey: 'admin-new' }
  },
  {
    path: '/admin',
    name: 'admin-new',
    exact: true,
    element: ListPost,
    meta: { pageTitle: 'admin-new', leftKey: 'admin-new' }
  },
  {
    path: '/admin/new/create',
    name: 'admin-create',
    exact: true,
    element: Create,
    meta: { pageTitle: 'admin-create', leftKey: 'admin-create' }
  },
  // {
  //   path: '/admin/useragent',
  //   name: 'admin-useragent',
  //   exact: true,
  //   element: ListArgent,
  //   meta: { pageTitle: 'admin-useragent', leftKey: 'admin-useragent' }
  // },
  {
    path: '/admin/usercareer',
    name: 'admin-usercareer',
    exact: true,
    element: ListCarreer,
    meta: { pageTitle: 'admin-usercareer', leftKey: 'admin-usercareer' }
  },
  {
    path: '/admin/contact',
    name: 'admin-contact',
    exact: true,
    element: ListContactOther,
    meta: { pageTitle: 'admin-contact', leftKey: 'admin-contact' }
  },
  {
    path: '/admin/usercontact',
    name: 'admin-usercontact',
    exact: true,
    element: ListContact,
    meta: { pageTitle: 'admin-usercontact', leftKey: 'admin-usercontact' }
  },
  {
    path: '/admin/new/edit/:id',
    name: 'admin-create',
    exact: true,
    element: Edit,
    meta: { pageTitle: 'admin-create', leftKey: 'admin-create' }
  }
];

export default routes;
