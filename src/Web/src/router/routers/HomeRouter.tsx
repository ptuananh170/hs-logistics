import { lazy } from 'react';

const Home = lazy(() => import('@/portal/page/home-page/home'));

const routes: IRouter.IRoute<'/'>[] = [
  {
    path: '/',
    name: 'aboutus',
    exact: true,
    element: Home,
    meta: { pageTitle: 'home', leftKey: 'home' }
  }
];

export default routes;
