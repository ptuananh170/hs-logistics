import { lazy } from 'react';

const LogictisMain = lazy(() => import('@/portal/page/Logistic-Solution/LogisticSolution'));
const warehouse = lazy(() => import('@/portal/page/Logistic-Solution/warehouse/Warehouse-Distribution'));
const custom = lazy(() => import('@/portal/page/Logistic-Solution/customs-brokerage/CustomsBrokerage'));
const supply = lazy(() => import('@/portal/page/Logistic-Solution/supply-chain-optimization/SupplyChainOptimization'));
const value = lazy(() => import('@/portal/page/Logistic-Solution/value-added/ValueAdded'));
const GreenTransport = lazy(() => import('@/portal/page/Logistic-Solution/green/GreenTransport'));
const CustomsService = lazy(() => import('@/portal/page/Logistic-Solution/customsservice/CustomService'));

const routes: IRouter.IRoute<'/logistic'>[] = [
  {
    path: '/logistics-solutions',
    name: 'logistics-solutions',
    exact: true,
    element: LogictisMain,
    meta: { pageTitle: 'Logistic solution', leftKey: 'logistics-solutions' }
  },
  {
    path: '/logistics-solutions/warehouse-distribution',
    name: 'logistic-warehouse',
    exact: true,
    element: warehouse,
    meta: { pageTitle: 'Warehouse distribution', leftKey: 'logistic-warehouse' }
  },
  {
    path: '/logistics-solutions/customs-brokerage',
    name: 'logistic-custom',
    exact: true,
    element: custom,
    meta: { pageTitle: 'Customs brokerage', leftKey: 'logistic-custom' }
  },
  {
    path: '/logistics-solutions/supply-chain-optimization',
    name: 'logistic-supply',
    exact: true,
    element: supply,
    meta: { pageTitle: 'Supply chain', leftKey: 'logistic-supply' }
  },
  {
    path: '/logistics-solutions/value-added',
    name: 'logistic-value',
    exact: true,
    element: value,
    meta: { pageTitle: 'Value added', leftKey: 'logistic-value' }
  },
  {
    path: '/logistics-solutions/green-transport',
    name: 'green-transport',
    exact: true,
    element: GreenTransport,
    meta: { pageTitle: 'Green transport', leftKey: 'green-transport' }
  },
  {
    path: '/logistics-solutions/customs-service',
    name: 'customs-service',
    exact: true,
    element: CustomsService,
    meta: { pageTitle: 'Customs service', leftKey: 'customs-service' }
  }
];

export default routes;
