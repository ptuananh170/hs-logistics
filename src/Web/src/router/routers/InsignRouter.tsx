import { lazy } from 'react';

const newsMain = lazy(() => import('@/portal/page/Insign/InsignMain'));
const detailNews = lazy(() => import('@/portal/page/Insign/InsignDetail/InsignDetail'));

const insignROuter: IRouter.IRoute<'/'>[] = [
  {
    path : '/insight-news',
    name: 'insight-news',
    exact: true,
    element: newsMain,
    meta: { pageTitle: 'insight-news', leftKey: 'insight-news' }
  },
  {
    path : '/insight-news/:id',
    name: 'insight-news',
    exact: true,
    element: detailNews,
    meta: { pageTitle: 'insight-news', leftKey: 'insight-news' }
  },
];

export default insignROuter;
