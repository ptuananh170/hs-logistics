import { lazy } from 'react';

const contact = lazy(() => import('@/portal/page/contact/Contact'));

const contactRouter: IRouter.IRoute<'/'>[] = [
  {
    path : '/contact',
    name: 'contact',
    exact: true,
    element: contact,
    meta: { pageTitle: 'contact', leftKey: 'contact' }
  },
];

export default contactRouter;
