import { cookie } from '@/common/helpers/cookie/cookie';
import axios, { AxiosInstance, AxiosResponse, InternalAxiosRequestConfig } from 'axios';
import { Modal } from 'antd';

export const axiosInstance: AxiosInstance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
});

export const axiosInstanceForm: AxiosInstance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  headers: {
    'Content-Type': 'multipart/form-data',
    'Access-Control-Allow-Origin': '*'
  }
});

// Interceptors before request
axiosInstance.interceptors.request.use(
  (config: InternalAxiosRequestConfig): InternalAxiosRequestConfig => {
    const token: A = cookie.getCookie('accessToken') ?? '';
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error: unknown): Promise<never> => {
    // Handle errors when sending requests
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response: AxiosResponse): AxiosResponse['data'] => {
    // Process returned data
    return response.data;
  },
  (error: any): Promise<never> => {
    if (error.response?.status === 401) {
      Modal.error({
        title: 'Unauthorized',
        content: 'Your session has expired. Please log in again.',
        onOk: () => {
          cookie.clearCookie('accessToken');
          location.href = '/admin/signin';
        }
      });
    } else if (error.response?.status !== 200 && error.response?.status !== 422 && error.response?.status !== 400) {
      Modal.error({
        title: error.response?.status,
        content: 'Something wrong!'
      });
    }    
    return Promise.reject(error);
  }
);

// Interceptors before request
axiosInstanceForm.interceptors.request.use(
  (config: InternalAxiosRequestConfig): InternalAxiosRequestConfig => {
    const token: A = cookie.getCookie('accessToken') ?? '';
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error: unknown): Promise<never> => {
    // Handle errors when sending requests
    return Promise.reject(error);
  }
);

axiosInstanceForm.interceptors.response.use(
  (response: AxiosResponse): AxiosResponse['data'] => {
    // Process returned data
    return response.data;
  },
  (error: any): Promise<never> => {
    if (error.response?.status === 401) {
      Modal.error({
        title: 'Unauthorized',
        content: 'Your session has expired. Please log in again.',
        onOk: () => {
          cookie.clearCookie('accessToken');
          location.href = '/admin/signin';
        }
      });
    } else if (error.response?.status !== 200 && error.response?.status !== 422 && error.response?.status !== 400) {
      Modal.error({
        title: error.response?.status,
        content: 'Something wrong!'
      });
    }    
    return Promise.reject(error);
  }
);
