import Router from '@/common/router/router';
import { App as AntdApp, ConfigProvider } from 'antd';
import { BreadcrumbProvider } from '@/common/components/bread-crumb/Breadcrum';
import { routers } from '@/router/RouterCongfig';
import '../i18n';
import ScrollToTop from './AutoScrollTop';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  return (
    <AntdApp style={{ height: '100%' }}>
      <ToastContainer />
      <BreadcrumbProvider>
        <Router routers={routers}>
          <ScrollToTop />
        </Router>
      </BreadcrumbProvider>
    </AntdApp>
  );
};

export default App;
