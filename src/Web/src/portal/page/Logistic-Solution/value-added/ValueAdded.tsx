import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import 'bootstrap/dist/css/bootstrap.css';
import './ValueAdded.scss';
import ConnectExpert from '../connect-expert/ConnectExpert';
import { useTranslation } from 'react-i18next';
import { value } from '../card-anim/CardImgImport';

export default function mainPage() {
  const { t } = useTranslation();

  const content1: any[] = t('logistic_value.content_1.list', { returnObjects: true });

  const Item: React.FC<{ content: any }> = ({ content }) => {
    return (
      <div className="divText">
        <div className="icon">
          <FontAwesomeIcon icon={faCheck} />
        </div>
        <div className="content">
          <div>{content}</div>
        </div>
      </div>
    );
  };

  const ct1: any[] = [];
  content1.forEach((element) => {
    ct1.push(<Item content={element} />);
  });

  return (
    <div style={{ marginTop: '5%', marginBottom: '15em' }}>
      <div style={{ width: '80%' }} className="container">
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <img src={value()} className="img-fluid" alt="" />
          </div>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 text-start">
            <h5 className="header">{t('logistic_value.content_1.title')}</h5>
            <div className="short-border"></div>
            <div className="mb-3 blue-text">{t('logistic_value.content_1.desc')}</div>
            {ct1}
            <div className="bd-dot my-4"></div>
            <div className="fst-italic fw-lighter fs-6">{t('logistic_value.content_1.desc_2')}</div>
          </div>
        </div>
      </div>

      <ConnectExpert></ConnectExpert>
    </div>
  );
}
