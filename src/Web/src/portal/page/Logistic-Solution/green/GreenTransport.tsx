import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import 'bootstrap/dist/css/bootstrap.css';
import './index.scss';
import ConnectExpert from '../connect-expert/ConnectExpert';
import { useTranslation } from 'react-i18next';
import img13 from '../../../../common/assets/img/Anh 13.jpg';

const Item: React.FC<{ content: any }> = ({ content }) => {
  return (
    <div className={content.title ? 'mb-3' : 'divText'}>
      <div className={content.title ? 'fs-5 fw-bold t-blue' : 'icon t-blue'}>
        <FontAwesomeIcon style={{ marginRight: content.title ? '0.5em' : '0em' }} icon={faCheck} />
        {content.title ? content.title : ''}
      </div>
      <div className="content">
        <div>{content.desc}</div>
      </div>
    </div>
  );
};

export default function GreenTransport() {
  const { t } = useTranslation();

  const content2: any[] = t('logistic_custom.content_2', { returnObjects: true });

  const ct2: any[] = [];
  content2.forEach((element) => {
    ct2.push(<Item content={element} />);
  });

  return (
    <div style={{ marginTop: '5%', marginBottom: '15em' }}>

      <div className="container-fluid bg-blue">
        <div className="container text-start" style={{ marginTop: '2em', width: '80%' }}>
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 order-md-2">
              <img className="rounded float-start img-fluid" src={img13} alt="" />
            </div>

            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 order-md-1 my-auto">
              <h3 className="header-sec">{t('logistic_custom.title_2')}</h3>
              <div className="short-border"></div>
              <div className="mb-3">{t('logistic_custom.desc_2')}</div>
              <div className="mb-3">{t('logistic_custom.desc_2.2')}</div>
              <div>
                <h5 className="header-sec mb-3">{t('logistic_custom.offer')}</h5>
              </div>
              {ct2}
              <div className="fst-italic fs-6 bd-dot">
                <div className="mt-3">{t('logistic_custom.desc_2.3')}</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ConnectExpert></ConnectExpert>
    </div>
  );
}
