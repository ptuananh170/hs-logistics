import 'bootstrap/dist/css/bootstrap.css';
import GetAFreeQuote from '../../components/get-a-free-quote/GetAFreeQuote';
import './LogisticSolution.scss';
import CardAnimation from './card-anim/CardAnim';
import { useTranslation } from 'react-i18next';
import anhImg from '../../../common/assets/img/Anh 4.jpg';

export default function MainPage() {
  const { t } = useTranslation();

  return (
    <div className="down-content">
      <CardAnimation></CardAnimation>

      <div className="mb-5">
        &nbsp; <br />
      </div>

      <div style={{ width: '80%' }} className="container">
        <div className="row container">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 order-md-2">
            <img style={{ width: '100%' }} src={anhImg} alt="" />
          </div>

          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 order-md-1">
            <div>
              <h2 className="header">{t('Logistic_Solution.desc_title')}</h2>
            </div>
            <div className="bd-short"></div>
            <div className="mt-3">{t('Logistic_Solution.desc_desc')}</div>
            <div className="d-flex flex-row mt-3">
              <div>
                <img
                  style={{ width: '80%' }}
                  src="https://3alogistics.vn/wp-content/uploads/2023/09/circle-with-icon-inside.png"
                  alt=""
                />
              </div>
              <div className="mt-3 ms-3">
                <h2 className="header-sec">{t('Logistic_Solution.desc_icon_1')}</h2>
              </div>
            </div>

            <div className="d-flex flex-row mt-3">
              <div>
                <img
                  style={{ width: '80%' }}
                  src="https://3alogistics.vn/wp-content/uploads/2023/09/circle-with-icon-inside.png"
                  alt=""
                />
              </div>
              <div className="mt-3 ms-3">
                <h2 className="header-sec">{t('Logistic_Solution.desc_icon_1')}</h2>
              </div>
            </div>

            <div className="d-flex flex-row mt-3">
              <div>
                <img
                  style={{ width: '80%' }}
                  src="https://3alogistics.vn/wp-content/uploads/2023/09/circle-with-icon-inside-1.png"
                  alt=""
                />
              </div>
              <div className="mt-3 ms-3">
                <h2 className="header-sec">{t('Logistic_Solution.desc_icon_2')}</h2>
              </div>
            </div>

            <div className="d-flex flex-row mt-3">
              <div>
                <img
                  style={{ width: '80%' }}
                  src="https://3alogistics.vn/wp-content/uploads/2023/09/circle-with-icon-inside-2.png"
                  alt=""
                />
              </div>
              <div className="mt-3 ms-3">
                <h2 className="header-sec">{t('Logistic_Solution.desc_icon_3')}</h2>
              </div>
            </div>

            <div className="d-flex flex-row mt-3">
              <div>
                <img
                  style={{ width: '80%' }}
                  src="https://3alogistics.vn/wp-content/uploads/2023/09/circle-with-icon-inside-3.png"
                  alt=""
                />
              </div>
              <div className="mt-3 ms-3">
                <h2 className="header-sec">{t('Logistic_Solution.desc_icon_4')}</h2>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div style={{ marginTop: '6em' }}>
        <GetAFreeQuote></GetAFreeQuote>
      </div>
    </div>
  );
}
