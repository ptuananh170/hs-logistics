import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Warehouse-Distribution.scss';
import 'bootstrap/dist/css/bootstrap.css';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import ConnectExpert from '../connect-expert/ConnectExpert';
import { useTranslation } from 'react-i18next';
import img6 from '../../../../common/assets/img/ANH 6.jpg';
import img9 from '../../../../common/assets/img/ANH 9.jpg';

const Item: React.FC<{ content: any }> = ({ content }) => {
  return (
    <div className="divText">
      <div className="icon">
        <FontAwesomeIcon icon={faCheck} />
      </div>
      <div className="content">
        <div>
          <h5 className="title-blue">{content.title}</h5>
        </div>
        <div>{content.desc}</div>
      </div>
    </div>
  );
};

export default function mainPage() {
  const { t } = useTranslation();

  const content1: any[] = t('logistic_warehouse.content_1', { returnObjects: true });

  const content2: any[] = t('logistic_warehouse.content_2', { returnObjects: true });

  const ct1: any[] = [];
  content1.forEach((element) => {
    ct1.push(<Item content={element} />);
  });

  const ct2: any[] = [];
  content2.forEach((element) => {
    ct2.push(<Item content={element} />);
  });

  return (
    <div style={{ marginBottom: '15em', marginTop: '5em' }}>
      <div className="container text-center" style={{ width: '80%' }}>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 mb-3">
            <img
              className="rounded float-end img-fluid"
              src={img6}
              alt=""
            />
          </div>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 text-start">
            <h3 className="header">{t('logistic_warehouse.title_1')}</h3>
            <div className="short-border"></div>
            {ct1}
          </div>
        </div>
      </div>

      <div className="container text-center" style={{ marginTop: '7em', width: '80%' }}>
        <div className="row">
          <div className="col-12 box mx-auto">{t('logistic_warehouse.warehouse')}</div>
        </div>
      </div>

      <div className="container-fluid bg-blue">
        <div className="container text-start" style={{ marginTop: '7em', width: '80%' }}>
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
              <h3 className="header">{t('logistic_warehouse.title_2')}</h3>
              <br />
              <div>{t('logistic_warehouse.desc_2')}</div>
              <br />
              <div>{t('logistic_warehouse.desc_2.2')}</div>
              <div className="short-border"></div>
              {ct2}
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
              <img
                className="rounded float-start img-fluid"
                src={img9}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
      <ConnectExpert></ConnectExpert>
    </div>
  );
}
