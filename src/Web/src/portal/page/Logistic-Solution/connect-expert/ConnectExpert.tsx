import 'bootstrap/dist/css/bootstrap.css';
import './ConnectExpert.scss';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { FemaleSpCustomer } from '../card-anim/CardImgImport';

const ConnectExpert : React.FC = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  return (
    <div className="container box-sd text-center max-w" style={{ marginTop: '7em' }}>
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5 ">
          <img className="float-end img-fluid" src={FemaleSpCustomer()} alt="" />
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 text-start">
          <h6 className="header">{t('logistic_common.title')}</h6>
          <div className="mb-5">
          {t('logistic_common.desc')}
          </div>
          <button onClick={() => {
            navigate('/contact');
          }} className="touchBtn">{t('logistic_common.btn')}</button>
        </div>
      </div>
    </div>
  );
}

export default ConnectExpert;
