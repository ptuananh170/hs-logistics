import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import '../LogisticSolution.scss';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import { BgImg1, BgImg2, BgImg3, BgImg4, ImgIc1, ImgIc2, ImgIc3, ImgIc4 } from './CardImgImport';

const Item: React.FC<{ product: any }> = ({ product }) => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [backgroundImage, setBackgroundImage] = useState('');

  const handleMouseEnter = (img: any) => {
    // Update background image based on the provided image
    setBackgroundImage(
      `linear-gradient(90deg, hsla(21, 63%, 47%, 0.85) 0%, hsla(229, 76%, 23%, 0.85) 100%),url('${img}')`
    );
  };

  return (
    <div className="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
      <div
        className={`card c-anim pointer`}
        onMouseEnter={() => handleMouseEnter(product.bgImg)}
        onMouseLeave={() => setBackgroundImage('')}
        style={{ backgroundImage: backgroundImage, }}
      >
        <div id='c-content' className="card-body c-content">
          <h4 className="card-title text-center bd-bottom" style={{ fontWeight: 'bold' }}>
            <div className="icon-sq mx-auto mt-4">
              <img className="mx-auto my-auto d-block center-img" decoding="async" src={product.iconImg} alt="" />
            </div>

            <p className="mt-4 align-bottom c-name" style={{ height: "100px" }}>
              {product.name}
            </p>
          </h4>
          <p title={product.desc} className="card-text mt-5 c-desc truncate-multiline" >
            {product.desc}
          </p>

          <div className="anim-right" style={{ cursor: 'pointer' }} onClick={() => navigate(product.path)}>
            <span className="btn btn-danger rounded-circle">
              <FontAwesomeIcon icon={faChevronRight} />
            </span>
            <span style={{ fontWeight: 'bold', fontSize: '1rem' }} className="ms-4">
              {t('Common_Readmore')}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

const CardAnimation: React.FC = () => {
  const { t } = useTranslation();

  const products: any[] = [
    {
      name: t('Common_Warehouse_Distribution'),
      iconImg: ImgIc1(),
      bgImg: BgImg1(),
      desc: t('Logistic_Solution.card_1.desc'),
      path: '/logistics-solutions/warehouse-distribution'
    },
    {
      name: t('Common_Customs_brokerage'),
      iconImg: ImgIc2(),
      bgImg: BgImg2(),
      desc: t('Logistic_Solution.card_2.desc'),
      path: '/logistics-solutions/customs-brokerage'
    },
    {
      name: t('Common_SUPPLY_CHAIN_MANAGEMENT'),
      iconImg: ImgIc3(),
      bgImg: BgImg3(),
      desc: t('Logistic_Solution.card_3.desc'),
      path: '/logistics-solutions/supply-chain-optimization'
    },
    {
      name: t('Common_Value_added'),
      iconImg: ImgIc4(),
      bgImg: BgImg4(),
      desc: t('Logistic_Solution.card_4.desc'),
      path: '/logistics-solutions/value-added'
    },
    {
      name: t('logistic_custom.title_2'),
      iconImg: ImgIc4(),
      bgImg: BgImg4(),
      desc: t('Logistic_Solution.card_4.desc'),
      path: '/logistics-solutions/green-transport'
    },
    {
      name: t('logistic_custom.title_3'),
      iconImg: ImgIc4(),
      bgImg: BgImg4(),
      desc: t('Logistic_Solution.card_4.desc'),
      path: '/logistics-solutions/customs-service'
    }
  ];

  const itemList: any[] = [];
  for (let i = 0; i < products.length; i++) {
    itemList.push(<React.Fragment key={i}><Item product={products[i]} /></React.Fragment>);
  }

  return (
    <div style={{ width: '80%' }} className="mt-5 mb-5 container">
      <div className="row container">{itemList}</div>
    </div>
  );
};

export default CardAnimation;
