import imgIc1 from '@/common/assets/img/card/ic1.png';
import imgIc2 from '@/common/assets/img/card/ic2.png';
import imgIc3 from '@/common/assets/img/card/ic3.png';
import imgIc4 from '@/common/assets/img/card/ic4.png';
import imgBg1 from '@/common/assets/img/card/bg1.png';
import imgBg2 from '@/common/assets/img/card/bg2.png';
import imgBg3 from '@/common/assets/img/card/bg3.png';
import imgBg4 from '@/common/assets/img/card/bg4.png';
import truckRed from '@/common/assets/img/truck-red.jpg';
import femImg from '@/common/assets/img/fem-emp.webp';
import femImg2 from '@/common/assets/img/fem-emp-2.png';
import connect from '@/common/assets/img/connect.jpg';
import valueImg from '@/common/assets/img/value.png';
import containerImg from '@/common/assets/img/container.png';

export function ImgIc1() {
  return imgIc1;
}
export function ImgIc2() {
  return imgIc2;
}
export function ImgIc3() {
  return imgIc3;
}
export function ImgIc4() {
  return imgIc4;
}
export function BgImg1() {
  return imgBg1;
}
export function BgImg2() {
  return imgBg2;
}
export function BgImg3() {
  return imgBg3;
}
export function BgImg4() {
  return imgBg4;
}
export function TruckRed(){
    return truckRed;
}
export function FemaleSpCustomer(){
    return femImg;
}
export function FemaleSp2(){
    return femImg2;
}
export function Connect(){
    return connect;
}
export function value(){
    return valueImg;
}
export function containerImgBG(){
    return containerImg;
}

export const validateEmail = (email: string): boolean => {
  const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return re.test(String(email).toLowerCase());
};

export const validatePhoneNumber = (phoneNumber: string): boolean => {
  const re = /^0\d{9}$/;
  return re.test(phoneNumber);
};