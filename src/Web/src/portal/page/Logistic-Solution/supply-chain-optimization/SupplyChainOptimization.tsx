import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './SupplyChainOptimization.scss';
import 'bootstrap/dist/css/bootstrap.css';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import ConnectExpert from '../connect-expert/ConnectExpert';
import { useTranslation } from 'react-i18next';
import { Connect, FemaleSp2 } from '../card-anim/CardImgImport';

const Item: React.FC<{ content: any }> = ({ content }) => {
  return (
    <div className='mb-2'>
      <div className='fs-5 fw-bold'>
        <FontAwesomeIcon icon={faCheck} style={{ marginRight: '0.5em', color : '#1c2d5a;' }} />
        <span style={{color : '#1c2d5a;' }} >{content.title}</span>
      </div>
      <div>
        <div className='fs-6'>{content.desc}</div>
      </div>
    </div>
  );
};

export default function mainPage() {
  const { t } = useTranslation();

  const content1: any[] = t('logistic_supply.content_1.list', { returnObjects: true });

  // const content2: any[] = t('logistic_supply.content_2.list', { returnObjects: true });
  const content2: any[] = [];

  const ct1: any[] = [];
  content1.forEach((element) => {
    ct1.push(<Item content={element} />);
  });

  const ct2: any[] = [];
  content2.forEach((element) => {
    ct2.push(<Item content={element} />);
  });

  return (
    <div style={{ marginBottom: '15em', marginTop: '5em' }}>
      <div className="container text-center" style={{ width: '80%' }}>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <img
              className="rounded float-end"
              src={FemaleSp2()}
              alt=""
            />
          </div>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 text-start">
            <h3 className="header">{t('logistic_supply.content_1.title')}</h3>
            <div className="short-border"></div>
            <div className="mb-3 blue-text">{t('logistic_supply.content_1.desc')}</div>
            {ct1}
          </div>
        </div>
      </div>

      <div className="container-fluid bg-blue">
        <div className="container text-start" style={{ marginTop: '2em', width: '80%' }}>
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 my-auto">
              <h3 className="header-sec">{t('logistic_supply.content_2.title')}</h3>
              <div className="short-border"></div>
              <div className="mb-3 blue-text">{t('logistic_supply.content_2.desc')}</div>
              {ct2}
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
              <img
                className="rounded float-start img-fluid"
                src={Connect()}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
      <ConnectExpert></ConnectExpert>
    </div>
  );
}
