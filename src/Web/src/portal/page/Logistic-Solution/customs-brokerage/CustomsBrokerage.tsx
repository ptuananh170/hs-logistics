import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './CustomsBrokerage.scss';
import 'bootstrap/dist/css/bootstrap.css';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import ConnectExpert from '../connect-expert/ConnectExpert';
import { useTranslation } from 'react-i18next';
import img3 from '../../../../common/assets/img/Anh 3.jpg';
import img2 from '../../../../common/assets/img/Anh 2.jpg';
import img13 from '../../../../common/assets/img/Anh 13.jpg';

const Item: React.FC<{ content: any }> = ({ content }) => {
  return (
    <div className={content.title ? 'mb-3' : 'divText'}>
      <div className={content.title ? 'fs-5 fw-bold t-blue' : 'icon t-blue'}>
        <FontAwesomeIcon style={{ marginRight: content.title ? '0.5em' : '0em' }} icon={faCheck} />
        {content.title ? content.title : ''}
      </div>
      <div className="content">
        <div>{content.desc}</div>
      </div>
    </div>
  );
};

export default function mainPage() {
  const { t } = useTranslation();

  const content1: any[] = t('logistic_custom.content_1', { returnObjects: true });

  const content2: any[] = t('logistic_custom.content_2', { returnObjects: true });

  const content3: any[] = t('logistic_custom.content_3', { returnObjects: true });

  const ct1: any[] = [];
  content1.forEach((element) => {
    ct1.push(<Item content={element} />);
  });

  const ct2: any[] = [];
  content2.forEach((element) => {
    ct2.push(<Item content={element} />);
  });

  const ct3: any[] = [];
  content3.forEach((element) => {
    ct3.push(<Item content={element} />);
  });

  return (
    <div style={{ marginBottom: '15em', marginTop: '5em' }}>
      <div className="container text-center" style={{ width: '80%' }}>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <img className="rounded float-end img-fluid" src={img3} alt="" />
          </div>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 text-start">
            <h3 className="header">{t('logistic_custom.title_1')}</h3>
            <div className="short-border"></div>
            <div>{t('logistic_custom.desc_1')}</div>
            <div className="my-3">{t('logistic_custom.desc_1.2')}</div>
            <div>
              <h5 className="header-sec mb-3">{t('logistic_custom.offer')}</h5>
            </div>
            {ct1}
          </div>
        </div>
      </div>

      <div className="container-fluid">
        <div className="container text-start" style={{ marginTop: '2em', width: '80%' }}>
          {/* <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 order-md-2">
              <img className="rounded float-start img-fluid" src={img13} alt="" />
            </div>

            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 order-md-1 my-auto">
              <h3 className="header-sec">{t('logistic_custom.title_2')}</h3>
              <div className="short-border"></div>
              <div className="mb-3">{t('logistic_custom.desc_2')}</div>
              <div className="mb-3">{t('logistic_custom.desc_2.2')}</div>
              <div>
                <h5 className="header-sec mb-3">{t('logistic_custom.offer')}</h5>
              </div>
              {ct2}
              <div className="fst-italic fs-6 bd-dot">
                <div className="mt-3">{t('logistic_custom.desc_2.3')}</div>
              </div>
            </div>
          </div> */}
        </div>
      </div>

      {/* <div className="container text-center" style={{ width: '80%' }}>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <img className="rounded float-end img-fluid" src={img2} alt="" />
          </div>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 text-start">
            <h3 className="header">{t('logistic_custom.title_3')}</h3>
            <div className="short-border"></div>
            <div>{t('logistic_custom.desc_3')}</div>
            <div>{t('logistic_custom.desc_3.1')}</div>
            <div className="header-sec my-3">{t('logistic_custom.offer')}</div>
            {ct3}
          </div>
        </div>
      </div> */}
      <ConnectExpert></ConnectExpert>
    </div>
  );
}
