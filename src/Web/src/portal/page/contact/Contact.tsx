import 'bootstrap/dist/css/bootstrap.css';
import './Contact.scss';
import '../Logistic-Solution/customs-brokerage/CustomsBrokerage.scss';
import ConnectExpert from '../Logistic-Solution/connect-expert/ConnectExpert';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLocationDot, faPhone } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import imgPath from '../../../common/assets/img/Anh 2.jpg';
import { useTranslation } from 'react-i18next';
import { Form, Input } from 'antd';
import { toast } from 'react-toastify';
import { axiosInstance } from '@/service/axios-config';

export default function mainPage() {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  return (
    <div style={{ marginBottom: '15em' }}>
      <div className="mx-auto" style={{ width: '80%' }}>
        <div className="container">
          <div className="row text-center">
            <div className="col-sm-12 col-xl-4">
              <div className="bg-blue">
                <div className="bg-circle ">
                  <FontAwesomeIcon icon={faLocationDot} className="icon-white" />
                </div>
                <div style={{ marginTop: '50px' }}>
                  <div className="container">Tầng 3 Đình Vũ Plaza, phường Đông Hải, quận Hải An, Hải Phòng</div>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-xl-4">
              <div className="bg-blue">
                <div className="bg-circle ">
                  <FontAwesomeIcon icon={faPhone} className="icon-white" />
                </div>
                <div style={{ marginTop: '50px' }}>
                  <div className="container">
                    <div className="fs-4 fw-bold">{t('contact.phone')}</div>
                    <div>0969115091</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-xl-4">
              <div className="bg-blue">
                <div className="bg-circle ">
                  <FontAwesomeIcon icon={faEnvelope} className="icon-white" />
                </div>
                <div style={{ marginTop: '50px' }}>
                  <div className="container">
                    <div className="fs-4 fw-bold">Email</div>
                    <div>thuylinh@hslogistics.vn</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="container-fluid"
        style={{
          backgroundImage: `url(${imgPath})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          height: '50%',
          marginTop: '4em'
        }}
      >
        <div className="container-fluid mx-auto max-w">
          <div style={{ paddingTop: '10em' }}>&nbsp;</div>
          <div className="" style={{ backgroundColor: 'white', borderRadius: '15px' }}>
            <Form
              form={form}
              className="p-5 row"
              onFinish={async (values: any) => {
                if (!values.fname) {
                  toast.error(t('toast.name'));
                  return;
                }
                if (!values.lname) {
                  toast.error(t('toast.name'));
                  return;
                }
                if (!values.com) {
                  toast.error(t('toast.com'));
                  return;
                }
                if (!values.level) {
                  toast.error(t('toast.level'));
                  return;
                }
                if (!values.phone) {
                  toast.error(t('toast.phone'));
                  return;
                }
                if (!values.email) {
                  toast.error(t('toast.email'));
                  return;
                }
                if (!values.subject) {
                  toast.error(t('toast.subject'));
                  return;
                }

                const body = {
                  firstName: values.fname,
                  lastName: values.lname,
                  company: values.com,
                  level: values.level,
                  phoneNumber: values.phone,
                  emailAddress: values.email,
                  subject: values.subject,
                  notes: values.note ?? null
                };

                const res : any = await axiosInstance.post('/Contact/create-contact', body);
                if (res.statusCode == 200) {
                  toast.success(t('toast.ok'));
                  form.resetFields();
                } else {
                  toast.error(t('toast.err'));
                }
              }}
            >
              <Form.Item
                rules={[{ required: true, message: t('toast.name') }]}
                name={'fname'}
                className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6"
              >
                <Input size="large" placeholder={t('contact.form.fname')} />
              </Form.Item>

              <Form.Item
                rules={[{ required: true, message: t('toast.name') }]}
                name={'lname'}
                className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6"
              >
                <Input size="large" placeholder={t('contact.form.lname')} />
              </Form.Item>

              <Form.Item
                rules={[{ required: true, message: t('toast.com') }]}
                name={'com'}
                className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6"
              >
                <Input size="large" placeholder={t('contact.form.com')} />
              </Form.Item>

              <Form.Item
                rules={[{ required: true, message: t('toast.level') }]}
                name={'level'}
                className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6"
              >
                <Input size="large" placeholder={t('contact.form.level')} />
              </Form.Item>

              <Form.Item
                rules={[{ required: true, message: t('toast.phone') }]}
                name={'phone'}
                className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6"
              >
                <Input size="large" placeholder={t('contact.form.phone')} />
              </Form.Item>

              <Form.Item
                rules={[{ required: true, message: t('toast.email') }]}
                name={'email'}
                className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6"
              >
                <Input size="large" placeholder={t('contact.form.email')} />
              </Form.Item>

              <Form.Item
                rules={[{ required: true, message: t('toast.subject') }]}
                name={'subject'}
                className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12"
              >
                <Input size="large" placeholder={t('contact.form.subject')} />
              </Form.Item>

              <Form.Item name={'note'} className="mb-3 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                <Input.TextArea placeholder={t('contact.form.note')} rows={10} maxLength={3000} />
              </Form.Item>

              <div className="col-12">
                <button className="btnSend container mt-3">{t('contact.form.send')}</button>
              </div>
            </Form>
          </div>
          <div style={{ paddingBlockStart: '10em' }}>&nbsp;</div>
        </div>
      </div>

      <ConnectExpert></ConnectExpert>
    </div>
  );
}
