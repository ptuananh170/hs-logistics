import { axiosInstance } from '@/service/axios-config';
import { faCalendarDays } from '@fortawesome/free-regular-svg-icons';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Card, Select, Space, Tooltip } from 'antd';
import 'bootstrap/dist/css/bootstrap.css';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import '../home.scss';
import './NewsComponent.scss';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';

const MainNewsPage: React.FC = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const [datas, setDatas] = useState<any>();
  const [isOk, setStatePage] = useState<any>(true);
  const getLists = async () => {
    try {
      const res: any = await axiosInstance.post('/News/all-news', {
        pageIndex: 1,
        pageSize: 4
      });
      setDatas(res.values);
      setStatePage(true);
    } catch {
      setStatePage(false);
    }
  };

  useEffect(() => {
    getLists();
  }, []);

  const select: any[] = t('home_news.select', { returnObjects: true });

  if(!isOk){
    return <></>
  }

  return (
    <div className="conatainer-fluid">
      <div className="text-center mx-auto" style={{ width: '80%' }}>
        <h2 className="text-uppercase fw-bold" dangerouslySetInnerHTML={{ __html: t('home_news.title') }} />
        <div className="short-bd mx-auto"></div>
      </div>
      {/* <div style={{ width: '78%' }} className="mx-auto">
        <Space wrap>
          <Select defaultValue="" style={{ width: '15em', height: '3em' }} options={select} />
        </Space>
      </div> */}

      <div style={{ width: '80%', marginTop: '2em' }} className="mx-auto row">
        {/* loop here */}
        {datas?.map((item: any, index: any) => {
          return <ItemNews news={item} key={index} />;
        })}
        {/* end loop */}
      </div>

      <div className="text-center" style={{ marginTop: '3em' }}>
        <button
          className="btn-blue"
          onClick={() => {
            navigate('/insight-news');
          }}
        >
          {t('home_news.all_news')}
        </button>
      </div>
    </div>
  );
};

export default MainNewsPage;

const ItemNews: React.FC<{ news: any }> = ({ news }) => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  return (
    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-4 col-xxl-3 mb-4">
      <Space direction="vertical" style={{ width: '100%' }}>
        <Card
          hoverable
          cover={
            <img
              alt="example"
              src={import.meta.env.VITE_URL_FILE + '/' + news.coverImage}
              style={{ height: '20em' }}
              onClick={() => {
                navigate('/insight-news/' + news?.id);
              }}
            />
          }
          style={{ backgroundColor: '#f6f6f6' }}
        >
          <div className="mx-start text-center" style={{ width: '30%' }}>
            <div className="cd-new">{t('home_news.news')}</div>
          </div>

          <div
            className="header-sec-2 pointer"
            onClick={() => {
              navigate('/insight-news/' + news?.id);
            }}
          >
            <Tooltip placement="top" title={news.title}>
              {news.title}
            </Tooltip>
          </div>

          <div className="my-3">
            <FontAwesomeIcon icon={faCalendarDays} /> &nbsp;
            {news?.publishedTime}
          </div>

          <div>
            <button
              className="btn-red-2"
              onClick={() => {
                navigate('/insight-news/' + news?.id);
              }}
            >
              {t('Common_Readmore')}
              <FontAwesomeIcon style={{ marginLeft: '0.5em' }} icon={faArrowRight} />
            </button>
          </div>
        </Card>
      </Space>
    </div>
  );
};

const ConvertDateTime: React.FC<any> = ({ input }) => {
  const formattedDate = dayjs(input).format('DD/MM/YYYY HH:mm');
  return <>{formattedDate}</>;
};
