import { faCircleRight } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Statistic, Tabs } from 'antd';
import 'bootstrap/dist/css/bootstrap.css';
import './home.scss';
import vid from '@/common/assets/img/video.mp4';
import { useTranslation } from 'react-i18next';
import GetAFreeQuote from '../../components/get-a-free-quote/GetAFreeQuote';
import CardAnimation from '../Logistic-Solution/card-anim/CardAnim';
import styles from '../about-us/AboutUs.module.scss';
import MainNewsPage from './news/NewsComponent';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import imgMan from '@/common/assets/img/business-man.jpg'
import { useNavigate } from 'react-router';
import { TruckRed } from '../Logistic-Solution/card-anim/CardImgImport';

const Item: React.FC<{ product: any }> = ({ product }) => {
  return (
    <div className="col-3 mb-4 elementor-widget-container align-text-bottom">
      <div className="ch-item ch-img-1 hover01" style={{ backgroundImage: `url(${product.img})` }}>
        <div className="txt">{product.text}</div>
      </div>
    </div>
  );
};


const HomeMainPage: React.FC = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const product: any[] = t('home.list_img', { returnObjects: true });

  const items: any[] = t('home.tabs', { returnObjects: true });

  const InfoTabs: React.FC = () => (
    <Tabs type="card" size="large" style={{ fontSize: '1.2em' }} defaultActiveKey="1" items={items} />
  );

  const prdImg: any[] = [];
  product.forEach((element) => {
    prdImg.push(<Item product={element} />);
  });

  return (
    <div style={{ marginBottom: '13em' }}>
      <div className="container-fluid" style={{ width: '80%' }}>
        <div className="mx-auto text-start">
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
              <div className="text-uppercase" style={{ fontSize: '3em', fontWeight: 'bolder' }}>
                {t('home.title_1')}
              </div>
              <div style={{ fontSize: '2.8em', fontWeight: 'bolder' }} className="text-danger text-uppercase">
                {t('home.title_2')}
              </div>
              <div style={{ fontSize: '2em', fontWeight: 'bolder' }}>{t('home.title_3')}</div>
              <div className="short-bd"></div>

              <div className="fs-6">
                <p>
                  <FontAwesomeIcon icon={faCheck} style={{ marginRight: '0.5em', color: 'red' }} /> {t('home.desc_1')}
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheck} style={{ marginRight: '0.5em', color: 'red' }} /> {t('home.desc_2')}
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheck} style={{ marginRight: '0.5em', color: 'red' }} /> {t('home.desc_3')}
                </p>
              </div>
            </div>

            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
              <img
                src={TruckRed()}
                alt="Truck-kun"
                className="img-fluid rounded"
              />
            </div>
          </div>
        </div>

        {/* <div className="row my-5">{prdImg}</div> */}

        <div className="row" style={{ marginTop: '3em' }}>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 text-start order-md-2 mb-3">
            <h3 className="fs-2 fw-bold text-uppercase">
              {t('home.we')} <span className="text-danger">{t('Common_Brand')}</span>
            </h3>
            <InfoTabs></InfoTabs>

            <div className="mt-3">
              <button
                className="btn-red"
                onClick={() => navigate('/about-us')}
              >
                {t('Common_Readmore')}
                <FontAwesomeIcon style={{ marginLeft: '0.5em' }} icon={faCircleRight} />
              </button>
            </div>
          </div>

          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5 order-md-1">
            <img
              className="img-fluid rounded"
              src={imgMan}
              alt=""
            />
          </div>
        </div>
      </div>
      <div style={{ marginTop: '4em', marginBottom: '4em' }}>
        <Statistical />
      </div>
      <div className="container-fluid">
        <div className="mx-auto text-center">
          <div style={{ fontSize: '3em', fontWeight: 'bolder' }}>
            {t('home.service')} <span className="text-danger">{t('Common_Brand')}</span>
          </div>
          <div className="short-bd mx-auto"></div>
        </div>
        <CardAnimation></CardAnimation>
      </div>
      <div style={{ marginTop: '6em' }}>
        <GetAFreeQuote></GetAFreeQuote>
      </div>
      <div style={{ marginTop: '5em' }}>
        <MainNewsPage></MainNewsPage>
      </div>
    </div>
  );
};

export default HomeMainPage;

function Statistical() {
  const { t } = useTranslation();
  return (
    <div className={styles.statisticalWrap}>
      <div className="video-container">
        <video controls className="video-element">
          <source src={vid} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </div>
      <div className="statistical">
        <div className="content_statis">
          <span>
            <Statistic value={50} />
          </span>
          <div className={styles.title_static}>{t('home.tons')}</div>
        </div>
        <div className="content_statis">
          <span>
            <Statistic value={50} />
          </span>
          <div className={styles.title_static}>{t('home.tosea')}</div>
        </div>
        <div className="content_statis">
          <span>
            <Statistic value={20} />
          </span>
          <div className={styles.title_static}>{t('home.TruckMove')}</div>
        </div>
        <div className="content_statis">
          <span>
            <Statistic value={9000} />+
          </span>
          <div className={styles.title_static}>{t('home.GlobalPartners')}</div>
        </div>
        <div className="content_statis">
          <span>
            SLP
          </span>
          <div style={{ cursor: "pointer" }} onClick={() => window.open("https://www.slpprop.com/vi/")} className={styles.title_static}>{t('home.WarehousePartner')}</div>
        </div>
      </div>
    </div>
  );
}
