import OurMission from './components/OurMission';
import OurVision from './components/OurVision';
import OurValues from './components/OurValues';
import TeamMembers from './components/TeamMembers';
import GetAFreeQuote from '../../components/get-a-free-quote/GetAFreeQuote';
import Statistical from './components/Statistical';

function AboutUsMain() {
  return (
    <>
      <OurVision />
      <OurMission />
      <OurValues />
      <Statistical />
      {/* <TeamMembers /> */}
      <GetAFreeQuote />
    </>
  );
}

export default AboutUsMain;
