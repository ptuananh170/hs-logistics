import { Col, Row } from 'antd';
import styles from '../AboutUs.module.scss';
import Paragraph from 'antd/es/typography/Paragraph';
import { useTranslation } from 'react-i18next';
import img from '@/common/assets/img/csm_Contargo_Straubing_Spatenstich_556f6924ba.jpg'

function OurMission() {
  const { t } = useTranslation();
  return (
    <Row className={styles.ourMissionWrapper}>
      <Row className={styles.ourMission}>
        <Col className={styles.ourMissionContent}>
          <h3>{t('AboutUs_Our_Mision')}</h3>
          <Paragraph>{t('AboutUs_Our_Mision_Paragraph')}</Paragraph>
        </Col>
        <Col className={styles.ourMissionImg}>
          <img
            style={{ width: '100%', objectFit: 'cover' }}
            src={img}
          />
        </Col>
      </Row>
    </Row>
  );
}

export default OurMission;
