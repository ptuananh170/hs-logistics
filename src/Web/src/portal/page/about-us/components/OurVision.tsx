import { Col, Row } from 'antd';
import styles from '../AboutUs.module.scss';
import Paragraph from 'antd/es/typography/Paragraph';
import { useTranslation } from 'react-i18next';
import img from '@/common/assets/img/csm_eActros_2_58690ed26c.jpg'

function OurVision() {
  const { t } = useTranslation();
  return (
    <Row className={styles.ourVisionWrapper}>
      <Row className={styles.ourVision}>
        <Col className={styles.ourVisionImg}>
          <img
            style={{ width: '100%', objectFit: 'cover' }}
            src={img}
          />
        </Col>
        <Col className={styles.ourVisionContent}>
          <h3>{t('AboutUs_Our_Vision')}</h3>
          <Paragraph>{t('AboutUs_Our_Vision_Paragraph')}</Paragraph>
        </Col>
      </Row>
    </Row>
  );
}

export default OurVision;
