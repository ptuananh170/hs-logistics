import { Statistic } from 'antd';
import styles from '../AboutUs.module.scss';
import { useTranslation } from 'react-i18next';

function Statistical() {
  const { t } = useTranslation();
  return (
    <div className={styles.statisticalWrap}>
      <div className="statistical">
        <div>
          <span>
            <Statistic value={50} />
          </span>
          <div className="title">{t('home.tons')}</div>
        </div>
        <div>
          <span>
            <Statistic value={50} />
          </span>
          <div className="title">{t('home.tosea')}</div>
        </div>
        <div>
          <span>
            <Statistic value={20} />
          </span>
          <div className="title">{t('home.TruckMove')}</div>
        </div>
        <div>
          <span>
            <Statistic value={9000} />+
          </span>
          <div className="title">{t('home.GlobalPartners')}</div>
        </div>
        <div>
          <span>
            SLP
          </span>
          <div style={{ cursor: "pointer" }} onClick={() => window.open("https://www.slpprop.com/vi/")} className="title">{t('home.WarehousePartner')}</div>
        </div>
      </div>
    </div>
  );
}

export default Statistical;
