import { Row, Timeline } from 'antd';
import styles from '../AboutUs.module.scss';
import Paragraph from 'antd/es/typography/Paragraph';
import { useTranslation } from 'react-i18next';
import { useDevice } from '@/common/utils';

function OurValues() {
  const { t } = useTranslation();
  const device = useDevice();
  return (
    <Row className={styles.ourValueWrapper}>
      <Row className={styles.ourValue}>
        {/* <h3>{t('AboutUs_Our_Values')}</h3> */}
        <h3>{t('about_us.history')}</h3>
        <br />
        <Paragraph>{t('AboutUs_Our_Values_Paragraph')}</Paragraph>
        <Paragraph></Paragraph>
        <br />
        <div className="timeline-wrapper">
          <Timeline
            mode={device.type == 'mobile' ? 'left' : 'alternate'}
            items={[
              {
                children: genContent('1989', t('about_us.1989'))
              },
              {
                children: genContent('2000', t('about_us.2000'))
              },
              {
                children: genContent('2010', t('about_us.2010'))
              },
              {
                children: genContent('2022', t('about_us.2022'))
              },
              {
                children: genContent('2023', t('about_us.2023'))
              }
            ]}
          />
        </div>
      </Row>
    </Row>
  );
}

export default OurValues;

function genContent(title: any, desc: any) {
  return (
    <div className={styles.box}>
      <h4 className={styles.title}>{title}</h4>
      <p>{desc}</p>
    </div>
  );
}
