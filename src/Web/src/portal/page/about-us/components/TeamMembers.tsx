import { useTranslation } from 'react-i18next';
import styles from '../AboutUs.module.scss';

function TeamMembers() {
  const { t } = useTranslation();
  return (
    <div className={styles.teamMembersWrapper}>
      <div className="team-members">{t('AboutUs_Team_Members')}</div>
      <div className="des">
        The <span style={{ color: '#ED1B2F' }}> Best </span> & <span style={{ color: '#ED1B2F' }}> Skilled </span>
        People Together
      </div>
      <div className="divider"></div>
    </div>
  );
}

export default TeamMembers;
