import { Col, Row } from 'antd';
import Paragraph from 'antd/es/typography/Paragraph';
import styles from '../BeOurAgent.module.scss';
import { useTranslation } from 'react-i18next';

function BeOurAgentNetwork() {
  const { t } = useTranslation();
  return (
    <Row className={styles.BeOurAgentNetworkWrapper}>
      <Row className={styles.BeOurAgentNetwork}>
        <Col>
          <img
            style={{ width: '100%', objectFit: 'cover' }}
            src="https://3alogistics.vn/wp-content/uploads/2023/09/img.png"
          />
        </Col>
        <Col>
          <h2>{t('network.title')}</h2>
          <div className="divider"></div>
          <Paragraph>{t('network.desc_1')}</Paragraph>
          <Paragraph>{t('network.desc_2')}</Paragraph>
        </Col>
      </Row>
    </Row>
  );
}

export default BeOurAgentNetwork;
