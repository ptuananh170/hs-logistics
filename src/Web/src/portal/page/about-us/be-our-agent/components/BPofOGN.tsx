import { Button, Col, Form, Input, Row } from 'antd';
import Paragraph from 'antd/es/typography/Paragraph';
import styles from '../BeOurAgent.module.scss';
import { useTranslation } from 'react-i18next';
import { useState } from 'react';
import { axiosInstance } from '@/service/axios-config';
import { toast } from 'react-toastify';

function BecomePartOfOurGlobalNetwork() {
  const [form] = Form.useForm();
  const { t } = useTranslation();

  const handleSubmit = async (values: any) => {
    const res: any = await axiosInstance.post('/UserAgent/create-agent', values);
    if (res.statusCode == 200) {
      toast.success(t('toast.ok'))
      form.resetFields();
    } else {
      toast.error(t('toast.err'))
    }
    form.resetFields();
  }
  return (
    <div className={styles.applyCareer}>
      <div className="wrapper-form">
        <Row className="form-wrap">
          <Col>
            <h2>{t('agent.our')}</h2>
            <div className="divider"></div>
            <Paragraph style={{ marginBottom: 45.6 }}>
              {t('agent.desc')}
            </Paragraph>
            <Form form={form} name="basic"
              onFinish={handleSubmit}>
              <Row>
                <Col>
                  <Form.Item name="firstName" rules={[{ required: true, message: t('toast.name') }]}>
                    <Input size="large" placeholder={t('contact.form.fname')} />
                  </Form.Item>
                </Col>
                <Col>
                  <Form.Item name="lastName" rules={[{ required: true, message: t('toast.name') }]}>
                    <Input size="large" placeholder={t('contact.form.lname')} />
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item name="emailAddress" rules={[{ required: true, message: t('toast.email') }]}>
                <Input size="large" type='email' placeholder={t('Common_Email_Address_PlaceHolder')} /> 
              </Form.Item>
              <Form.Item name="phoneNumber" rules={[{ required: true, message: t('toast.phone') }]}>
                <Input size="large" placeholder={t('contact.form.phone')} />
              </Form.Item>
              <Form.Item name="message">
                <Input.TextArea
                  rows={10}
                  style={{ resize: 'none' }}
                  size="large"
                  placeholder={t('Common_Your_Message_PlaceHolder')}
                />
              </Form.Item>
              <Form.Item>
                <Button className='text-uppercase' style={{ width: '100%', }} type="primary" htmlType="submit">
                  {t('Common_Send_Message_Button')}
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col className="img-div">
            <img
              style={{ width: '100%', objectFit: 'cover' }}
              src="https://mso.vn/wp-content/uploads/2023/09/gia-tang-thu-nhap-1024x683.webp"
            />
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default BecomePartOfOurGlobalNetwork;
