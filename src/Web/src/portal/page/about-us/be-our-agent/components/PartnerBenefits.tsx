import { Col, Row } from 'antd';
import styles from '../BeOurAgent.module.scss';
import Paragraph from 'antd/es/typography/Paragraph';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHandshake } from '@fortawesome/free-regular-svg-icons';
import { faAward, faSitemap, faUser } from '@fortawesome/free-solid-svg-icons';
import { useTranslation } from 'react-i18next';

function PartnerBenefits() {
  const { t } = useTranslation();
  return (
    <Row className={styles.partnerBenefitsWrapper}>
      <Row className={styles.partnerBenefits}>
        <h2>{t('agent.exp')}</h2>
        <div className="divider"></div>
        <Paragraph style={{ marginBottom: 50 }}>{t('agent.exp_1')}</Paragraph>
        <Row className="content">
          <Row className="content">
            <Col>
              <FontAwesomeIcon style={{ marginBottom: 15, color: '#1c2d5a' }} size={'3x'} icon={faSitemap} />
              <h3>72.000 m2</h3>
              <Paragraph>{t('agent.area')}</Paragraph>
            </Col>
            <Col>
              <FontAwesomeIcon style={{ marginBottom: 15, color: '#1c2d5a' }} size={'3x'} icon={faHandshake} />
              <h3>{t('AboutUs_BeOurAgent_SharedGoals')}</h3>
              <Paragraph>{t('agent.ser')}</Paragraph>
            </Col>
          </Row>
          <Row className="content">
            <Col>
              <FontAwesomeIcon style={{ marginBottom: 15, color: '#1c2d5a' }} size={'3x'} icon={faUser} />
              <h3>200+</h3>
              <Paragraph>{t('agent.truck')}</Paragraph>
            </Col>
            <Col>
              <FontAwesomeIcon style={{ marginBottom: 15, color: '#1c2d5a' }} size={'3x'} icon={faAward} />
              <h3>4 Barges</h3>
              <Paragraph>120 TEUs</Paragraph>
            </Col>
            <Col>
              <FontAwesomeIcon style={{ marginBottom: 15, color: '#1c2d5a' }} size={'3x'} icon={faAward} />
              <h3>25.000 m2</h3>
              <Paragraph>CY</Paragraph>
            </Col>
          </Row>
        </Row>
      </Row>
    </Row>
  );
}

export default PartnerBenefits;
