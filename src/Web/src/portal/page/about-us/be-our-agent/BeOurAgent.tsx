import BecomePartOfOurGlobalNetwork from './components/BPofOGN';
import BeOurAgentNetwork from './components/Network';
import PartnerBenefits from './components/PartnerBenefits';

function BeOurAgent() {
  return (
    <>
      <BeOurAgentNetwork />
      <PartnerBenefits />
      <BecomePartOfOurGlobalNetwork />
    </>
  );
}

export default BeOurAgent;
