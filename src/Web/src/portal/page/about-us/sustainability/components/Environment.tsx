import { Col, Row } from 'antd';
import Paragraph from 'antd/es/typography/Paragraph';
import styles from '../Sustainability.module.scss';
import { useTranslation } from 'react-i18next';

function Environment() {
  const { t } = useTranslation();
  return (
    <Row className={styles.environmentWrapper}>
      <Row className={styles.environment}>
        <Row className={styles.subenvironment}>
          <Col>
            <img
              style={{ width: '100%', objectFit: 'cover' }}
              src="https://3alogistics.vn/wp-content/uploads/2023/09/image-25-2-1.jpg"
            />
          </Col>
          <Col>
            <h2>{t('AboutUs_Environment_Entry')}</h2>
            <div className="divider"></div>
            <Paragraph>{t('AboutUs_Sustainability_Environment_Paragraph_1a')}</Paragraph>
            <Paragraph>{t('AboutUs_Sustainability_Environment_Paragraph_1b')}</Paragraph>
            <Paragraph>{t('AboutUs_Sustainability_Environment_Paragraph_1c')}</Paragraph>
          </Col>
        </Row>
        <Row className={styles.subenvironment}>
          <Col>
            <Paragraph>{t('AboutUs_Sustainability_Environment_Paragraph_2a')}</Paragraph>
            <Paragraph>{t('AboutUs_Sustainability_Environment_Paragraph_2b')}</Paragraph>
          </Col>
          <Col>
            <img
              style={{ width: '100%', objectFit: 'cover' }}
              src="https://3alogistics.vn/wp-content/uploads/2023/09/Rectangle-11-1.png"
            />
          </Col>
        </Row>
      </Row>
    </Row>
  );
}

export default Environment;
