import { Col, Row } from 'antd';
import Paragraph from 'antd/es/typography/Paragraph';
import styles from '../Sustainability.module.scss';
import { useTranslation } from 'react-i18next';

function Governance() {
  const { t } = useTranslation();
  return (
    <Row className={styles.environmentWrapper}>
      <Row className={styles.environment}>
        <Row className={styles.subenvironment}>
          <Col>
            <img
              style={{ width: '100%', objectFit: 'cover' }}
              src="https://3alogistics.vn/wp-content/uploads/2023/09/kinsey-cB8YiJt_0Y0-unsplash-fix-5-1.jpg"
            />
          </Col>
          <Col>
            <h2>{t('AboutUs_Governance_Entry')}</h2>
            <div className="divider"></div>
            <Paragraph>{t('AboutUs_Sustainability_Governance_Paragraph')}</Paragraph>
          </Col>
        </Row>
      </Row>
    </Row>
  );
}

export default Governance;
