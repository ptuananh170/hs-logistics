import { Col, Row } from 'antd';
import Paragraph from 'antd/es/typography/Paragraph';
import styles from '../Sustainability.module.scss';
import { useTranslation } from 'react-i18next';

function SocialGovernance() {
  const { t } = useTranslation();
  return (
    <Row className={styles.socialGovernanceWrapper}>
      <Row className={styles.socialGovernance}>
        <Row className={styles.subSocialGovernance}>
          <Col>
            <img
              style={{ width: '100%', objectFit: 'cover' }}
              src="https://3alogistics.vn/wp-content/uploads/2023/12/sustainability-1024x683.jpg"
            />
          </Col>
          <Col>
            <h2>Quản trị Xã hội</h2>
            <div className="divider"></div>
            <Paragraph>{t('AboutUs_Sustainability_SocialGovernance_Paragraph_1a')}</Paragraph>
          </Col>
        </Row>
        <Row className={styles.subSocialGovernance}>
          <Col>
            <Paragraph>{t('AboutUs_Sustainability_SocialGovernance_Paragraph_2a')}</Paragraph>
            <Paragraph>{t('AboutUs_Sustainability_SocialGovernance_Paragraph_2b')}</Paragraph>
          </Col>
          <Col>
            <img
              style={{ width: '100%', objectFit: 'cover' }}
              src="https://3alogistics.vn/wp-content/uploads/2023/09/Rectangle-11-1-1.png"
            />
          </Col>
        </Row>
      </Row>
    </Row>
  );
}

export default SocialGovernance;
