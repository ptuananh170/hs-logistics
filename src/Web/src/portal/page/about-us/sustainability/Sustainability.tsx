import { Divider } from 'antd';
import Environment from './components/Environment';
import Governance from './components/Governance';
import SocialGovernance from './components/SocialGovernance';
import styles from './Sustainability.module.scss';
import GetAFreeQuote from '../../../components/get-a-free-quote/GetAFreeQuote';
import { useTranslation } from 'react-i18next';

function Sustainability() {
  const { t } = useTranslation();
  return (
    <div className={styles.sustainability}>
      <div className="header">
        <h3>{t('AboutUs_Environment_Entry')}</h3>
        <h3>{t('AboutUs_Social_Governance_Entry')}</h3>
        <h3>{t('AboutUs_Governance_Entry')}</h3>
      </div>
      <Divider />
      <Environment />
      <div className="guessing-wrapper">
        <div className="guessing">
          <h3>{t('AboutUs_Sustainability_3A')}</h3>
          <Divider />
        </div>
      </div>
      <SocialGovernance />
      <Governance />
      <GetAFreeQuote />
    </div>
  );
}

export default Sustainability;
