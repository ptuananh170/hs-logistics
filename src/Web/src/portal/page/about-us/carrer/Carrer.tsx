import ApplyCareer from './components/ApplyCareer';
import LogisticsCareer from './components/LogisticsCareer';
import SeekingCareers from './components/SeekingCareers';

function Career() {
  return (
    <>
      <LogisticsCareer />
      <SeekingCareers />
      <ApplyCareer />
    </>
  );
}

export default Career;
