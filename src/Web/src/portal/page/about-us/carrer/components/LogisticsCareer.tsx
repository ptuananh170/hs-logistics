import { containerImgBG } from '@/portal/page/Logistic-Solution/card-anim/CardImgImport';
import { RightCircleFilled } from '@ant-design/icons';
import { Col, Row } from 'antd';
import Paragraph from 'antd/es/typography/Paragraph';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import styles from '../Career.module.scss';

function LogisticsCareer() {
  const { t } = useTranslation();
  return (
    <Row className={styles.logisticWrapper}>
      <Row className={styles.logisticCareer}>
        <Col>
          <img
            style={{ width: '100%', objectFit: 'cover' }}
            src={containerImgBG()}
          />
        </Col>
        <Col>
          <h2>{t('career.join')}</h2>
          <div className="divider"></div>
          <Paragraph>{t('career.join_desc')}</Paragraph>
          <Row>
            <Link to="/about-us" className="link">
              <RightCircleFilled style={{ marginRight: 15, fontWeight: 700 }} />
              {t('career.detail')}
            </Link>
          </Row>
        </Col>
      </Row>
    </Row>
  );
}

export default LogisticsCareer;
