import Paragraph from 'antd/es/typography/Paragraph';
import styles from '../Career.module.scss';
import { Button, Col, Form, Input, Row, Upload } from 'antd';
import { useTranslation } from 'react-i18next';
import imgMan from '@/common/assets/img/business-man.jpg';
import { useState } from 'react';
import { axiosInstanceForm } from '@/service/axios-config';
import { toast } from 'react-toastify';
import { validateEmail, validatePhoneNumber } from '@/portal/page/Logistic-Solution/card-anim/CardImgImport';

function ApplyCareer() {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const [file, setFile] = useState<any>([]);
  const onFinish = async (values: any) => {
    try {
      if (!validateEmail(values.emailAddress)) {
        toast.error(t('toast.email'));
        return;
      }
      if (!validatePhoneNumber(values.phoneNumber)) {
        toast.error(t('toast.phone'));
        return;
      }
      const formData: any = new FormData();
      formData.append('firstName', values.firstName);
      formData.append('lastName', values.lastName);
      formData.append('emailAddress', values.emailAddress);
      formData.append('phoneNumber', values.phoneNumber);
      formData.append('file', file);
      formData.append('coverLetter', values.coverLetter);
      await axiosInstanceForm.post('/UserCareer/create-user-career', formData);
      toast.success(t('toast.ok'));
      setFile([]);
      form.resetFields();
    } catch (error) {
      console.log(error);
      toast.error(t('toast.err'));
    }
  };

  return (
    <div className={styles.applyCareer}>
      <div className="wrapper-form">
        <Row className="form-wrap">
          <Col>
            <h2>{t('career.title')}</h2>
            <div className="divider"></div>
            <Paragraph style={{ marginBottom: 45.6 }}>{t('career.contact')}</Paragraph>
            <Form form={form} name="basic" onFinish={onFinish}>
              <Row>
                <Col>
                  <Form.Item name="firstName" rules={[{ required: true, message: t('toast.name') }]}>
                    <Input size="large" placeholder={t('contact.form.fname')} />
                  </Form.Item>
                </Col>
                <Col>
                  <Form.Item name="lastName" rules={[{ required: true, message: t('toast.name') }]}>
                    <Input size="large" placeholder={t('contact.form.lname')} />
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item name="emailAddress" rules={[{ required: true, message: t('toast.email') }]}>
                <Input size="large" placeholder="Email" />
              </Form.Item>
              <Form.Item name="phoneNumber" rules={[{ required: true, message: t('toast.phone') }]}>
                <Input size="large" placeholder={t('contact.form.phone')} />
              </Form.Item>
              <Form.Item name="file" rules={[{ required: true, message: 'Please input upload file' }]}>
                <Input type="file" onChange={(e: any) => setFile(e?.target.files[0])} />
              </Form.Item>
              <Form.Item name="coverLetter" rules={[{ required: true, message: 'Please input letter' }]}>
                <Input.TextArea
                  rows={5}
                  style={{ resize: 'none' }}
                  size="large"
                  placeholder={t('Common_Cover_Letter_PlaceHolder')}
                />
              </Form.Item>
              <Button type="primary" htmlType="submit">
                {t('Common_Submit_Button')}
              </Button>
            </Form>
          </Col>
          <Col className="img-div">
            <img style={{ width: '100%', objectFit: 'cover' }} src={imgMan} />
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default ApplyCareer;
