import { Row } from 'antd';
import styles from '../Career.module.scss';
import { useTranslation } from 'react-i18next';

function SeekingCareers() {
  const { t } = useTranslation();
  return (
    <Row className={styles.seekingWrapper}>
      <div className={styles.seekingCareers}>
        <h4 className="first-row text-uppercase">{t('career.join_2')}</h4>
        <h2 className='text-uppercase' style={{display : 'inline',}} dangerouslySetInnerHTML={{ __html: t('career.desc_2') }} />
        <div className="divider"></div>
        <p className="content" style={{ padding: '0 224px' }}>
          {t('career.desc_2_1')}
        </p>
      </div>
    </Row>
  );
}

export default SeekingCareers;
