import { faCalendar } from '@fortawesome/free-regular-svg-icons';
import { faArrowRight, faChevronRight, faMagnifyingGlass, faUserTie } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import 'bootstrap/dist/css/bootstrap.css';
import './InsignMain.scss';
import { Pagination, Tooltip } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { axiosInstance } from '@/service/axios-config';
import { useNavigate } from 'react-router-dom';
import { NotWorkTemplate } from '@/common/page/not-work/NotWork';

export default function mainPage() {
  const { t } = useTranslation();

  const [datas, setDatas] = useState<any>();
  const [pageSize, setPageSize] = useState<any>(10);
  const [pageIndex, setPageIndex] = useState<any>(1);
  const [total, setTotal] = useState<any>(0);
  const [isOk, setStatePage] = useState<any>(true);

  const [inputValue, setInputValue] = useState('');

  const handleInputChange = (event: any) => {
    setInputValue(event.target.value);
  };

  const getLists = async (inputValue: any) => {
    try{
      const res: any = await axiosInstance.post('/News/all-news', {
        pageIndex: pageIndex,
        pageSize: pageSize,
        searchContent: inputValue ? inputValue : ''
      });
      
      setDatas(res.values);
      // console.log(res);
      setPageSize(res?.pageSize);
      setPageIndex(res?.currentPage);
      setTotal(res?.totalCount);
      setStatePage(true);
    }
    catch{
      setStatePage(false);
    }
    
  };

  useEffect(() => {
    getLists(null);
  }, [pageSize, pageIndex]);

  if(!isOk){
    return(
      <div className="container" style={{ marginBottom: '19em'  }}>
        <NotWorkTemplate></NotWorkTemplate>
      </div>
    );
  }

  return (
    <div className="container-fluid" style={{ marginBottom: '19em' }}>
      <div className="mx-auto" style={{ width: '90%' }}>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3 order-md-2 mb-3">
            <div className="bg-blue-2">
              <div className="p-3">
                <FontAwesomeIcon icon={faChevronRight} style={{ marginRight: '1em', fontSize: '2em', color: 'red' }} />
                <span style={{ fontSize: '2em', fontWeight: 'bold' }}>{t('Common_search')}</span>

                <div className="input-group">
                  <input
                    type="text"
                    className="form-control"
                    value={inputValue}
                    onChange={handleInputChange}
                    aria-label="Text input with segmented dropdown button"
                  />
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={() => {
                      // console.log('demo test ' + inputValue);
                      getLists(inputValue);
                    }}
                  >
                    <FontAwesomeIcon icon={faMagnifyingGlass} />
                  </button>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-9 col-xl-9 col-xxl-9 order-md-1">
            {/* loop here */}
            {datas?.map((item: any, index: any) => {
              return <LoadingNews news={item} key={index} />;
            })}
            {/* end loop */}
            <div className="mx-end">
              {total > 0 && (
                <Pagination
                  style={{ marginTop: '10px' }}
                  defaultCurrent={pageIndex}
                  pageSize={pageSize}
                  total={total}
                  onChange={(page, size) => {
                    if (page != pageIndex) {
                      setPageIndex(page);
                    }
                    if (pageSize != size) {
                      setPageSize(pageSize);
                    }
                  }}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const LoadingNews: React.FC<{ news: any }> = ({ news }) => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  // console.log(news);
  return (
    <>
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
          <img
            onClick={() => {
              navigate('/insight-news/' + news?.id);
            }}
            src={import.meta.env.VITE_URL_FILE + '/' + news.coverImage}
            alt=""
            className="img-fluid rounded pointer"
          />
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
          <div className="box-news">News</div>
          <div
            className="title text-truncate pointer"
            onClick={() => {
              navigate('/insight-news/' + news?.id);
            }}
          >
            <Tooltip placement="bottomLeft" title={news.title}>
              {news.title}
            </Tooltip>
          </div>
          <div>
            <FontAwesomeIcon icon={faUserTie} style={{ marginRight: '1em' }} />
            Admin
            <FontAwesomeIcon icon={faCalendar} style={{ marginRight: '1em', marginLeft: '2em' }} />
            {news.publishedTime}
          </div>
          <div className="demo-content-news">description {news.description}</div>
        </div>
      </div>
      <button
        className="btn-readmore"
        onClick={() => {
          navigate('/insight-news/' + news?.id);
        }}
      >
        {t('Common_Readmore')} <FontAwesomeIcon icon={faArrowRight} style={{ marginLeft: '0.5em' }} />
      </button>
      <hr />
    </>
  );
};
