import { NotWorkTemplate } from '@/common/page/not-work/NotWork';
import { axiosInstance } from '@/service/axios-config';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import { faUserTie } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import 'bootstrap/dist/css/bootstrap.css';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import './InsignDetail.scss';

interface News {
  id: any;
  title: any;
  content: any;
  coverImage: any;
  publishedTime: any;
}

export default function mainPage() {

  const { id } = useParams<{ id: string }>();

  const [datas, setDatas] = useState<any>();

  let [isOk, setInputValue] = useState<boolean>();
  let [isLoading, setLoading] = useState<boolean>(true);

  const [news, setNews] = useState<News | any>(null);

  const getLists = async (inputValue: any) => {
    try {
      const res: any = await axiosInstance.get('/News/get-detail/' + id);
      if (res?.statusCode != 200) {
        // alert('not ok');
      }
      setDatas(res.data);
      setNews(res.data);
      setInputValue(true);
    } catch {
      setInputValue(false);
    }
    setLoading(false);
  };

  useEffect(() => {
    getLists(null);
  }, []);

  if (!isOk && !isLoading)
    return (
        <NotWorkTemplate></NotWorkTemplate>
    );

  return (
    <div style={{ marginBottom: '19em' }}>
      <div className="container-fluid">
        <div className="mx-auto" style={{ width: '70%' }}>
          <div className="row">
            {/* <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3 order-md-2 mb-3">
              <div className="bg-blue-2">
                <div className="p-3">
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    style={{ marginRight: '1em', fontSize: '2em', color: 'red' }}
                  />
                  <span style={{ fontSize: '2em', fontWeight: 'bold' }}>{t('Common_search')}</span>

                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control"
                      value={inputValue}
                      onChange={handleInputChange}
                      aria-label="Text input with segmented dropdown button"
                    />
                    <button
                      type="button"
                      className="btn btn-secondary"
                      onClick={() => {
                        // console.log('demo test ' + inputValue);
                        // getLists(inputValue);
                      }}
                    >
                      <FontAwesomeIcon icon={faMagnifyingGlass} />
                    </button>
                  </div>
                </div>
              </div>
            </div> */}

            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 order-md-1">
              <h1 className="fw-bold fs-1 text-uppercase mt-5">{news?.title}</h1>
              <div>
                <FontAwesomeIcon icon={faUserTie} style={{ marginRight: '1em' }} />
                HS Logistic
                <FontAwesomeIcon icon={faClock} style={{ margin: '0em 1em 0em 3em' }} />
                {news?.publishedTime}
              </div>
              <div className="mt-5 fs-5" dangerouslySetInnerHTML={{ __html: news?.content }}></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
