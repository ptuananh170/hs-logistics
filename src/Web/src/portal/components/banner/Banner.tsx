import img10 from '@/common/assets/img/ANH 10.jpg';
import img14 from '@/common/assets/img/Anh 14.jpg';
import img15 from '@/common/assets/img/Anh 15.jpg';
import img2 from '@/common/assets/img/Anh 2.jpg';
import img3 from '@/common/assets/img/Anh 3.jpg';
import img4 from '@/common/assets/img/Anh 4.jpg';
import { Carousel } from 'antd';
import { Link } from 'react-router-dom';
import styles from './Banner.module.scss';
import { useTranslation } from 'react-i18next';

function Banner() {
  const { t } = useTranslation();
  return (
    <div className={styles.banner}>
      <Carousel autoplay dots dotPosition="bottom" effect="scrollx">
        <div className={styles.carousel_item}>
          <img src={img2} className={styles.carousel_image}></img>
          <div className={styles.overlay_text}>
            <div className={styles.text_block}>{t('Banner_WeAre')}</div>
            <div>{t('Banner_ProvideService')}</div>
            <div className={styles.text_block}>{t('Banner_WeProundOf')}</div>
            <Link to="/logistics-solutions/warehouse-distribution" className={styles.button}>
              {t('Banner_ViewMore')}
            </Link>
          </div>
        </div>
        <div className={styles.carousel_item}>
          <img src={img3} className={styles.carousel_image}></img>
        </div>
        <div className={styles.carousel_item}>
          <img src={img4} className={styles.carousel_image}></img>
        </div>
        <div className={styles.carousel_item}>
          <img src={img10} className={styles.carousel_image}></img>
        </div>
        <div className={styles.carousel_item}>
          <img src={img15} alt="truck-kun-not-local" className={styles.carousel_image}></img>
        </div>
        <div className={styles.carousel_item}>
          <img src={img14} alt="truck-kun-not-local" className={styles.carousel_image}></img>
        </div>
      </Carousel>
    </div>
  );
}

export default Banner;
