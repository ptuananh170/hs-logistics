import { Button, Col, Form, Input, Row, Select } from 'antd';
import styles from './GetAFreeQuote.module.scss';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { axiosInstance, axiosInstanceForm } from '@/service/axios-config';
import { validateEmail, validatePhoneNumber } from '@/portal/page/Logistic-Solution/card-anim/CardImgImport';

function GetAFreeQuote() {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  return (
    <div className={styles.applyCareer}>
      <div className="overlay"></div>
      <div className="wrapper-form">
        <Row className="form-wrap">
          <Col>
            <h5>{t('Common_Know_The_Price_Entry')}</h5>
            <h2>{t('Common_Get_A_FreeQuote_Entry')}</h2>
          </Col>
          <Form
            layout="vertical"
            form={form}
            onFinish={async (values: any) => {
              const fName = values.fullName;
              if (!fName) {
                toast.error(t('toast.name'));
                return;
              }

              const email = values.email;
              if (!validateEmail(email)) {
                toast.error(t('toast.email'));
                return;
              }

              if (!validatePhoneNumber(values.phone)) {
                toast.error(t('toast.phone'));
                return;
              }

              if (!values.subject) {
                toast.error(t('toast.subject'));
                return;
              }

              if (!values.cityPick) {
                toast.error(t('toast.pickCity'));
                return;
              }

              if (!values.cityDeli) {
                toast.error(t('toast.deliCity'));
                return;
              }

              if (!values.type) {
                toast.error(t('toast.freight'));
                return;
              }

              if (!values.inco) {
                toast.error(t('toast.inco'));
                return;
              }

              if (values.width == null || values.width < 0) {
                toast.error(t('toast.width'));
                return;
              }

              if (values.height == null || values.height < 0) {
                toast.error(t('toast.height'));
                return;
              }

              if (values.length == null || values.length < 0) {
                toast.error(t('toast.length'));
                return;
              }

              if (values.weight == null || values.weight < 0) {
                toast.error(t('toast.weight'));
                return;
              }

              let body = {
                fullName: fName,
                emailAddress: email,
                phoneNumber: values.phone,
                subject: values.subject,
                pickUpCity: values.cityPick,
                deliverCity: values.cityDeli,
                freight: Number(values.type),
                incoterms: Number(values.inco),
                width: Number(values.width),
                height: Number(values.height),
                length: Number(values.length),
                weight: Number(values.weight)
              };

              let rs: any = await axiosInstance.post('/UserContact/create/user-contact', body);
              if (rs.statusCode == 200) {
                toast.success(t('toast.ok'));
                form.resetFields();
              } else toast.error(t('toast.err'));
            }}
          >
            <Row>
              <Col>
                <Form.Item name={'fullName'} rules={[{ required: true, message: t('toast.name') }]}>
                  <Input size="large" placeholder={t('Common_Your_Name_PlaceHolder')} />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name={'email'} rules={[{ required: true, message: t('toast.email') }]}>
                  <Input size="large" placeholder={t('Common_Email_Address_PlaceHolder')} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Item name={'phone'} rules={[{ required: true, message: t('toast.phone') }]}>
                  <Input size="large" placeholder={t('Common_Phone_Number_PlaceHolder')} />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name={'subject'} rules={[{ required: true, message: t('toast.subject') }]}>
                  <Input size="large" placeholder={t('Common_Subject_PlaceHolder')} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Item name={'cityPick'} rules={[{ required: true, message: t('toast.pickCity') }]}>
                  <Input size="large" placeholder={t('Common_Pick_Up_City_PlaceHolder')} />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name={'cityDeli'} rules={[{ required: true, message: t('toast.deliCity') }]}>
                  <Input size="large" placeholder={t('Common_Deliver_City_PlaceHolder')} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Item
                  label={t('Common_Freight_Type_PlaceHolder')}
                  name={'type'}
                  rules={[{ required: true, message: t('toast.freight') }]}
                >
                  <Select size="large" placeholder={t('Common_Please_Choose_An_Option_PlaceHolder')}>
                    <Select.Option value="0">FCL</Select.Option>
                    <Select.Option value="1">LCL</Select.Option>
                  </Select>
                </Form.Item>
                <Row className="sub-form-item">
                  <Col className="sub-form-item-col">
                    <Form.Item name={'width'} rules={[{ required: true, message: t('toast.width') }]}>
                      <Input size="large" placeholder={t('Common_Width_PlaceHolder')} />
                    </Form.Item>
                  </Col>
                  <Col className="sub-form-item-col">
                    <Form.Item name={'height'} rules={[{ required: true, message: t('toast.height') }]}>
                      <Input size="large" placeholder={t('Common_Height_PlaceHolder')} />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col>
                <Form.Item
                  label={t('Common_Incoterms_PlaceHolder')}
                  name={'inco'}
                  rules={[{ required: true, message: t('toast.inco') }]}
                >
                  <Select size="large" placeholder={t('Common_Please_Choose_An_Option_PlaceHolder')}>
                    <Select.Option value="0">EXW</Select.Option>
                    <Select.Option value="1">FOB</Select.Option>
                    <Select.Option value="2">CIF</Select.Option>
                  </Select>
                </Form.Item>
                <Row className="sub-form-item">
                  <Col className="sub-form-item-col">
                    <Form.Item name={'length'} rules={[{ required: true, message: t('toast.length') }]}>
                      <Input size="large" placeholder={t('Common_Length_PlaceHolder')} />
                    </Form.Item>
                  </Col>
                  <Col className="sub-form-item-col">
                    <Form.Item name={'weight'} rules={[{ required: true, message: t('toast.weight') }]}>
                      <Input size="large" placeholder={t('Common_Weight_PlaceHolder')} />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Button style={{ width: '100%' }} type="primary" htmlType="submit">
              {t('Common_Send_Message_Button')}
            </Button>
          </Form>
        </Row>
      </div>
    </div>
  );
}

export default GetAFreeQuote;
