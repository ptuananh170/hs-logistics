import { Affix, Button, Layout } from 'antd';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import FooterNav from './components/footer/FooterNav';
import MenuNav from './components/menu/MenuNav';
import styles from './LayoutUser.module.scss';
import BackToTop from '@/common/components/back-to-top/BackToTop';
import logo from '@/common/assets/svg/logo.png';
import { useTranslation } from 'react-i18next';
import Banner from '../banner/Banner';
import { MenuUnfoldOutlined } from '@ant-design/icons';
import MenuMobile from './components/menu-mobile/MenuMobile';
import i18next from 'i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEarthAsia } from '@fortawesome/free-solid-svg-icons';

export interface IProps {
  children?: React.ReactNode;
}
const UserLayout: React.FC<IProps> = (props) => {
  const { Content } = Layout;
  const { t } = useTranslation();
  const [openMobilePanel, setOpenMobilePanel] = useState<boolean>(true);

  const changeLang = () => {
    i18next.language == 'vn' ? i18next.changeLanguage('en') : i18next.changeLanguage('vn');
  };

  return (
    <>
      <Layout className={styles.masterLayout}>
        <Affix className={styles.siteLayoutAffix} offsetTop={0}>
          <div>
            <div className={styles.headerWrapper}>
              <div className={styles.header}>
                <Link to="/">
                  <img style={{ backgroundColor: 'transparent' }} width="90" height="80" src={logo} alt="Logistic HS" />
                </Link>
                <div className={styles.subheader}>
                  <MenuNav />
                  <div>|</div>
                  <button style={{ padding: 0 }} onClick={changeLang}>
                    <FontAwesomeIcon icon={faEarthAsia} />
                  </button>
                  <Button
                    type="text"
                    className="button-mobile"
                    icon={<MenuUnfoldOutlined style={{ fontSize: 24 }} />}
                    onClick={() => setOpenMobilePanel(!openMobilePanel)}
                  ></Button>
                </div>
              </div>
            </div>
            <MenuMobile openMobilePanel={openMobilePanel} />
          </div>
        </Affix>
        <div style={{ marginBottom: '3em' }}>
          <Banner />
        </div>
        <Layout className={styles.siteLayout}>
          <Content className={styles.content}>
            <div className={styles.contentCard}>{props.children}</div>
          </Content>
        </Layout>
        <FooterNav />
        <BackToTop />
      </Layout>
    </>
  );
};

export default React.memo(UserLayout);
