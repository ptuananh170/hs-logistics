import React from 'react';
import styles from './FooterNav.module.scss';
import { Button, Col, Divider, Input, Row } from 'antd';
import { MailOutlined, PhoneOutlined, FacebookFilled, LinkedinFilled } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

const FooterNav: React.FC<A> = () => {
  const { t } = useTranslation();
  return (
    <div className={styles.footerNav}>
      <div className={styles.sitefooter}>
        <Row className={styles.footerContainer}>
          <Row className={styles.subscribe}>
            <div className={styles.content}>
              <Divider plain>
                <div className="uael-divider-content">
                  <h3>{t('Common_Daily_Update_Entry')}</h3>
                </div>
              </Divider>
              <h1>{t('Common_Subscribe_Now_Entry')}</h1>
              <p>{t('Common_Subscribe_Description_Entry')}</p>
            </div>
            <div className={styles.email}>
              <Input placeholder={t('Common_Enter_Your_Email_Placeholder')} />
              <Button type="primary">{t('Common_Submit_Button')}</Button>
            </div>
          </Row>
          <Row className={styles.info}>
            <Col className={styles.logoContact}>
              {/* <img width="148" height="112" src={logo} alt="" /> */}
              <span style={{ display: 'flex' }}>
                <MailOutlined />
                <a
                  style={{ textDecoration: 'none', color: '#fff' }}
                  href="https://mail.google.com/mail/?view=cm&fs=1&to=thuylinh@hslogistics.vn"
                  target="_blank"
                >
                  thuylinh@hslogistics.vn
                </a>
              </span>
              <span style={{ display: 'flex' }}>
                <PhoneOutlined />
                <a style={{ textDecoration: 'none', color: '#fff' }} href="tel:0969115091" target="_blank">
                  <span>+0969115091</span>
                </a>
              </span>
              <span data-tooltip="true" title="Linkedin" style={{ cursor: "pointer" }} onClick={() => window.open('/')}>
                <LinkedinFilled />
              </span>
            </Col>
            <Col className={styles.office}>
              <Col className={styles.subOffice}>
                <div className={styles.title}>{t('Common_Head_Office')}</div>
                <p>Tầng 3 Đình Vũ Plaza, phường Đông Hải, quận Hải An, Hải Phòng</p>
              </Col>
            </Col>

            <Col className={styles.fanpage}>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14914.213761733232!2d106.73182625056828!3d20.849731592079788!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314a65368981f1b7%3A0x167c894d0ffeedea!2zQ8O0bmcgVHkgQ3AgVuG6rW4gVOG6o2kgVGjGsMahbmcgTeG6oWkgUXXhuqNuZyDEkMO0bmc!5e0!3m2!1svi!2s!4v1717773226535!5m2!1svi!2s"
                style={{ border: '0' }}
                width="260"
                height="150"
                allowFullScreen={false}
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
              ></iframe>
            </Col>
          </Row>
        </Row>
      </div>
    </div>
  );
};

export default React.memo(FooterNav);
