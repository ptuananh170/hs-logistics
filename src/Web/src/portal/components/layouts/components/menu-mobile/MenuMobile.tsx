import { Collapse, List, Typography } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import styles from './MenuMobile.module.scss';
import { useTranslation } from 'react-i18next';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { CustomMenuItem } from '../menu/MenuNav';

function renderMenuItem(item: CustomMenuItem, navigate: Function, t: Function) {
  const { label, children, path } = item as { label: string; children?: CustomMenuItem[]; path?: string };

  const handleItemClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const isExpandIconClicked = (event.target as HTMLElement).closest('.ant-collapse-expand-icon') !== null;
    if (!isExpandIconClicked && path) {
      event.stopPropagation();
      navigate(path);
    }
  };

  return (
    <div key={item.key} onClick={handleItemClick}>
      <List.Item>
        <Collapse accordion collapsible="icon" expandIconPosition="end">
          <Collapse.Panel
            header={
              <div
                className={
                  `/` + location.pathname.split('/')[1] === item.path || location.pathname === item.path
                    ? `selected`
                    : ''
                }
              >
                {label}
              </div>
            }
            key={item.key ? item.key.toString() : new Date().getTime().toString()}
            showArrow={children != null}
          >
            {children && (
              <List
                className={styles.subList}
                dataSource={children}
                renderItem={(child: CustomMenuItem) => renderMenuItem(child, navigate, t)}
              />
            )}
          </Collapse.Panel>
        </Collapse>
      </List.Item>
    </div>
  );
}

interface IMenuMobile {
  openMobilePanel?: boolean;
}

function MenuMobile(props: IMenuMobile) {
  const { openMobilePanel = false } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();
  const location = useLocation();
  const items: CustomMenuItem[] = [
    {
      label: t('Common_Home_Entry'),
      key: 'homepage',
      path: '/'
    },
    {
      label: t('Common_Logistics_Solution_Entry'),
      key: 'logistics-solutions',
      path: '/logistics-solutions',
      children: [
        {
          label: 'Warehouse & Distribution',
          key: 'warehouse-distribution',
          path: '/logistics-solutions/warehouse-distribution'
        },
        {
          label: 'Customs brokerage',
          key: 'customs-brokerage',
          path: '/logistics-solutions/customs-brokerage'
        },
        {
          label: 'Supply chain optimization',
          key: 'supply-chain-optimization',
          path: '/logistics-solutions/supply-chain-optimization'
        },
        {
          label: 'Value-added',
          key: 'value-added',
          path: '/logistics-solutions/value-added'
        }
      ]
    },
    {
      label: t('Common_AboutUs_Entry'),
      key: 'aboutus',
      path: '/about-us',
      children: [
        // {
        //   label: t('AboutUs_Sustainability_Entry'),
        //   key: 'sustainability',
        //   path: '/about-us/sustainability',
        // },
        {
          label: t('AboutUs_BeOurAgent_Entry'),
          key: 'be-our-agent',
          path: '/about-us/be-our-agent'
        },
        {
          label: t('AboutUs_Career_Entry'),
          key: 'career',
          path: '/about-us/career'
        }
      ]
    },
    {
      label: t('Common_Insight_News_Entry'),
      key: 'insight-news',
      path: '/insight-news'
    },
    {
      label: t('Common_Contact_Entry'),
      key: 'contact',
      path: '/contact'
    }
  ];

  return (
    <Collapse accordion className={`${openMobilePanel ? styles.list : styles.listopen}`}>
      {items.map((item) => renderMenuItem(item, navigate, t))}
    </Collapse>
  );
}

export default MenuMobile;
