import { routers } from '@/router/RouterCongfig';
import { Menu, MenuProps } from 'antd';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';
import { matchRoutes } from 'react-router-dom';
import styles from './MenuNav.module.scss';
import { useTranslation } from 'react-i18next';

interface IMenutNavProps {
  onMenuClick?: (key: string) => void;
  mode?: A;
}
type MenuItem = Required<MenuProps>['items'][number];
export type CustomMenuItem = MenuItem & {
  path?: string;
  children?: CustomMenuItem[];
};
interface IMenuClickEvent {
  key: string;
  keyPath: string[];
}
const MenuNav: React.FC<IMenutNavProps> = (props) => {
  const { t } = useTranslation();
  const items: CustomMenuItem[] = [
    {
      label: t('Common_Home_Entry'),
      key: 'homepage',
      path: '/'
    },
    {
      label: <span onClick={() => navigate('/logistics-solutions')}>{t('Common_Logistics_Solution_Entry')}</span>,
      key: 'logistics-solutions',
      path: '/logistics-solutions',
      children: [
        {
          label: t('Common_Customs_brokerage'),
          key: 'customs-brokerage',
          path: '/logistics-solutions/customs-brokerage'
        },
        {
          label: t('logistic_custom.title_2'),
          key: 'green-transport',
          path: '/logistics-solutions/green-transport'
        },
        {
          label: t('logistic_custom.title_3'),
          key: 'customs-service',
          path: '/logistics-solutions/customs-service'
        },
        {
          label: t('Common_Warehouse_Distribution'),
          key: 'warehouse-distribution',
          path: '/logistics-solutions/warehouse-distribution'
        },
        {
          label: t('Common_SUPPLY_CHAIN_MANAGEMENT'),
          key: 'supply-chain-optimization',
          path: '/logistics-solutions/supply-chain-optimization'
        },
        {
          label: t('Common_Value_added'),
          key: 'value-added',
          path: '/logistics-solutions/value-added'
        }
      ]
    },
    {
      label: <span onClick={() => navigate('/about-us')}>{t('Common_AboutUs_Entry')}</span>,
      key: 'aboutus',
      path: '/about-us',
      children: [
        // {
        //   label: t('AboutUs_Sustainability_Entry'),
        //   key: 'sustainability',
        //   path: '/about-us/sustainability',
        // },
        // {
        //   label: t('AboutUs_BeOurAgent_Entry'),
        //   key: 'be-our-agent',
        //   path: '/about-us/be-our-agent'
        // },
        {
          label: t('AboutUs_Career_Entry'),
          key: 'career',
          path: '/about-us/career'
        }
      ]
    },
    {
      label: t('Common_Insight_News_Entry'),
      key: 'insight-news',
      path: '/insight-news'
    },
    {
      label: t('Common_Contact_Entry'),
      key: 'contact',
      path: '/contact'
    }
  ];
  const [selectedKey, setSelectedKey] = useState<string[]>(['/']);
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    const matchRoutesList = matchRoutes(routers as A, location);
    if (matchRoutesList?.length && matchRoutesList.length > 0) {
      const theRouteDetails = matchRoutesList[matchRoutesList.length - 1];
      const theRoute = theRouteDetails?.route as A;
      const currentKey = theRoute?.meta?.leftKey;
      if (currentKey) {
        const activeKey = findItemNodeByKeyOrPath('key', currentKey);
        if (activeKey) {
          setSelectedKey([activeKey.key as string]);
        }
      } else {
        setSelectedKey(['']);
        getIncludeKeyByPath(theRoute.path);
      }
    } else {
      getIncludeKeyByPath(location.pathname);
    }
  }, [location.pathname]);

  const getIncludeKeyByPath = (pathName?: string) => {
    const defaultPathList = pathName?.split('/');
    const defaultPath = `/${defaultPathList?.[1] ?? ''}`;
    const currentInclude = items.find((item) => item.path?.includes(defaultPath));
    if (currentInclude) {
      setSelectedKey([currentInclude.key as string]);
    }
  };

  const menuClick = (info: IMenuClickEvent) => {
    if (info.key) {
      setSelectedKey([info.key]);
      const path = findItemNodeByKeyOrPath('key', info.key)?.path;
      path && navigate(path);
      const header = document.querySelector('.layout-header');
      header && header.scrollIntoView();
      props?.onMenuClick?.(info.key);
    }
  };

  const findItemNodeByKeyOrPath = (key: 'key' | 'path', keyValue: string): CustomMenuItem | null => {
    if (!items || !keyValue) return null;
    const treeList = [...items];
    while (treeList.length > 0) {
      const treeItem = treeList.shift();
      if (!treeItem) return null;
      if (treeItem[key] === keyValue) {
        return treeItem;
      }
      treeItem.children?.forEach((child) => {
        treeList.push(child as CustomMenuItem);
      });
    }
    return null;
  };

  return (
    <Menu
      onClick={menuClick}
      selectedKeys={selectedKey}
      className={styles.menuNav}
      mode={props.mode ?? 'horizontal'}
      items={items}
      // overflowedIndicator
    />
  );
};

export default React.memo(MenuNav);
