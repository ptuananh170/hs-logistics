import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { resolve } from 'path';

export default defineConfig({
  plugins: [react()],
  base: '/',
  optimizeDeps: {
    esbuildOptions: {
      target: 'esnext'
    }
  },
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html')
      }
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
        @import "./src/common/assets/scss/variables/_variables.scss";
        @import "./src/common/assets/scss/variables/_mixin.scss";`
      }
    }
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
      'admin': resolve(__dirname, './src/admin'),
      'portal': resolve(__dirname, './src/portal'),
    }
  },
  assetsInclude: ['**/*.svgx']
});
