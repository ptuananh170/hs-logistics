using HSLogistics.API;

Console.Title = "HSLogistics.API";
var builder = WebApplication.CreateBuilder(new WebApplicationOptions()
{
    WebRootPath = ""
});

// Add configuration files based on the environment
var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production";
builder.Configuration
       .SetBasePath(Directory.GetCurrentDirectory())
       .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
       .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
       .AddEnvironmentVariables();

var startup = new Startup(builder.Configuration);

startup.ConfigureServices(builder.Services, builder.Environment);

var app = builder.Build();

startup.Configure(app, app.Environment);
app.Run();