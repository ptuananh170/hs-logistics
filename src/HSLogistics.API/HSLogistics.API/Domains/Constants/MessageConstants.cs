namespace HSLogistics.API.Domains.Constants;

public static class UserMessageConstants
{
    public static string LoginFailedPassword = "Your password or email is incorrect. Please try again!";
    public static string LockUserMessage = "Your account has been locked. Please try again after 30 minutes!";
    public static string LoginSuccessfully = "Login successfully!";
    public static string LoginInactiveUser = "Your account has been no longer to access the system!";
}