using System.Collections.Immutable;
using System.Security.Claims;

namespace HSLogistics.API.Domains.Constants;

public static class ConfigurationKeys
{
    public const string JWTSettingSecretKey = "JWTSetting:SecretKey";
    public const string JWTSetting = "JWTSetting";
    public const string CoreDbConnectionString = "CoreDbConnectStr";
}

public static class CustomClaimTypes
{
    public const string Role = ClaimTypes.Role;
    public const string Email = ClaimTypes.Email;
    public const string UserId = "UserId";
}

public static class UserRoles
{
    public const string NormalUser = "NormalUser";
    public const string Admin = "Admin";

    public static ImmutableList<string> Roles =
        ImmutableList.Create<string>(NormalUser, Admin);
}

public static class UserAdminDefault
{
    public const string SuperAdminId = "4CED47E2-F549-40EC-AA42-1E2DF140ADA8";
    public const string SuperAdminPassword = "Superadmin123@";
    public const string SuperAdminFullName = "Admin Default";
    public const string SuperAdminEmail = "superAdmin@gmail.com";
    public const string SuperAdminUserName = "superAdmin";
}

public static class CommonConstants 
{
    public const string ImageFolder = "Images";
    public const string TimeLimitPeriodAPI = "5s";
    public const int TimeLimitAPI = 3;
}

public static class APINeedToLimitConstants
{
    public const string CreateUserAgent = "POST:/api/UserAgent/create-agent";
    public const string CreateUserContact = "POST:/api/UserContact/create/user-contact";
    public const string CreateUserCareer = "POST:/api/UserCareer/create-user-career";
    public const string CreateContact = "POST:/api/Contact/create-contact";
}