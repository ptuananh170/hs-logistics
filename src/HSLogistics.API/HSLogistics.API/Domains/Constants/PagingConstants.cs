namespace HSLogistics.API.Domains.Constants;

public static class FilterKeyConstants
{
    public const string Freight = "Freight";
    public const string Incoterms = "Incoterms";
}

public static class OrderByKeyConstants
{
    public const string FullName = "FullName";
    public const string CreatedDate = "CreatedDate";
}