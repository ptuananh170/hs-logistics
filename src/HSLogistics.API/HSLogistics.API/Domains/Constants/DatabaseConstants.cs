namespace HSLogistics.API.Domains.Constants;

public static class TableNameConstants
{
    public const string UserContact = "CUserContacts";
    public const string New = "CNews";
    public const string UserCareer = "CUserCareer";
    public const string UserAgent = "CUserAgent";
    public const string Contact = "CContact";
}

public static class PrimaryKeyConstants
{
    public const string PK_UserContact = "PK_CUserContacts";
    public const string PK_New = "PK_CNews";
    public const string PK_UserCareer = "PK_CUserCareer";
    public const string UserAgent = "PK_CUserAgent";
    public const string PK_Contact = "PK_CContact";
}