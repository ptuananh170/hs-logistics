namespace HSLogistics.API.Domains.Enums;

public enum UserGender : byte
{
    Male = 0,
    Female = 1,
    None = 2
}

public enum UserStatus : byte
{
    Active = 0,
    Inactive = 1,
}

public enum FreightType : byte
{
    FCL = 0,
    LCL = 1
}

public enum Incoterms : byte
{
    EXW = 0,
    FOB = 1,
    CIF = 2
}

public enum UserCareerStatus : byte
{
    New = 0,
    Seen = 1,
    Passed = 2,
    Failed = 3,
}

public enum UserAgentStatus : byte
{
    New = 0,
    Pending = 1,
    Approved = 2,
    Denied = 3,
}