using AutoMapper;
using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;

namespace HSLogistics.API.Domains.AutoMapper;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<UserContactDto, UserContact>();
        CreateMap<UserContact, UserContactDto>();
        CreateMap<UserCareerModel, UserCareer>()
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => UserCareerStatus.New));
        CreateMap<UserCareer, UserCareerModel>();
        CreateMap<UserCareer, UserCareerListModel>()
            .ForMember(dest => dest.File, opt => opt.MapFrom(src => src.CVFile));
        CreateMap<UserCareerListModel, UserCareer>()
            .ForMember(dest => dest.CVFile, opt => opt.MapFrom(src => src.File));
        CreateMap<New, NewListModel>()
            .ForMember(dest => dest.PublishedTime, opt => opt.MapFrom(src => (new DateTime(src.PublishedTime)).ToString("dd/MM/yyyy")));
        CreateMap<NewListModel, New>();
        CreateMap<UserAgent, UserAgentModel>();
        CreateMap<UserAgentModel, UserAgent>();
        CreateMap<Contact, ContactModel>().ReverseMap();
    }
}
