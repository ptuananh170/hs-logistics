namespace HSLogistics.API.Domains.Models;

public class APIResponseModel
{
    public int StatusCode { get; set; }
    public string? Message { get; set; }
    public object? Data { get; set; }
    public bool IsSuccess { get; set; }
}