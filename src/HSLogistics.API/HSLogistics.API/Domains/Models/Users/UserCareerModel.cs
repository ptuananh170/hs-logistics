﻿using System.ComponentModel.DataAnnotations;

namespace HSLogistics.API.Domains.Models
{
    public class UserCareerModel
    {
        [Required]
        public string FirstName { set; get; }
        [Required]
        public string LastName { set; get; }
        [Required]
        [EmailAddress]
        public string EmailAddress { set; get; }
        [Required]
        [Phone]
        public string PhoneNumber { set; get; }
        [Required]
        public IFormFile File { set; get; }
        public string? CoverLetter { set; get; }
    }

    public class UserCareerListModel
    {
        public Guid Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmailAddress { set; get; }
        public string PhoneNumber { set; get; }
        public string File { set; get; }
        public string? CoverLetter { set; get; }
    }

}
