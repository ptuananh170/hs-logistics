﻿using System.ComponentModel.DataAnnotations;

namespace HSLogistics.API.Domains.Models
{
    public class UserAgentModel
    {
        public Guid? Id { get; set; }
        [Required]
        public string FirstName { set; get; }
        [Required]
        public string LastName { set; get; }
        [Required]
        [EmailAddress]
        public string EmailAddress { set; get; }
        [Required]
        [Phone]
        public string PhoneNumber { set; get; }
        public string? Message { set; get; }
    }

}
