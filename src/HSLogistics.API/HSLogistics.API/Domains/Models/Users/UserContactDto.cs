using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using HSLogistics.API.Domains.Enums;

namespace HSLogistics.API.Domains.Models;

public class UserContactDto
{
    public Guid? Id { get; set; }
    [Required]
    public string FullName { get; set; }
    [Required]
    [EmailAddress]
    public string EmailAddress { get; set; }
    [Required]
    [Phone]
    public string PhoneNumber { get; set; }
    [Required]
    public string Subject { get; set; }
    public string? PickUpCity { get; set; }
    public string? DeliverCity { get; set; }
    [DefaultValue(FreightType.FCL)]
    public FreightType Freight { get; set; }
    [DefaultValue(Incoterms.EXW)]
    public Incoterms Incoterms { get; set; }
    public double? Width { get; set; }
    public double? Height { get; set; }
    public double? Length { get; set; }
    public double? Weight { get; set; }
}