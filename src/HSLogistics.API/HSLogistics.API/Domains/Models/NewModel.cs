using System.ComponentModel.DataAnnotations;

namespace HSLogistics.API.Domains.Models;

public class NewModel
{
    public Guid? Id { get; set; }
    [Required]
    [MaxLength(200)]
    public string Title { get; set; }
    [Required]
    public string Content { get; set; }
    public IFormFile? CoverImage { get; set; }
    public bool? IsUpdateCoverImage { get; set; }
    [MaxLength(500)]
    public string? Description { get; set; }
}

public class NewListModel
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public string CoverImage { get; set; }
    public string PublishedTime { get; set; }
    public string? Description { get; set; }
}