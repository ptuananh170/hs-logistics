namespace HSLogistics.API.Domains.Models;

public class JWTSettingModel
{
    public required string SecretKey { get; set; }
}