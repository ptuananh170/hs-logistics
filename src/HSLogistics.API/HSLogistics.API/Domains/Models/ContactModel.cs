﻿using System.ComponentModel.DataAnnotations;

namespace HSLogistics.API.Domains.Models
{
    public class ContactModel
    {
        public Guid? Id { get; set; }
        [Required]
        public string FirstName { set; get; }
        [Required]
        public string LastName { set; get; }
        [Required]
        [EmailAddress]
        public string EmailAddress { set; get; }
        [Required]
        [Phone]
        public string PhoneNumber { set; get; }
        public string? Notes { set; get; }
        [Required]
        public string Level { set; get; }
        [Required]
        public string Subject { set; get; }
        public string? Company { get; set; }
    }
}
