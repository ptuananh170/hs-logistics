namespace HSLogistics.API.Domains.Models;

public class TableRequestParameter
{
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public string? SearchContent { get; set; }
    public PageRequestOrderBy? OrderBy { get; set; }
    public List<PageRequestFilter>? Filters { get; set; }
}

public class PageRequestOrderBy
{
    public bool OrderByDesc { get; set; }
    public string OrderByColumn { get; set; }
}

public class PageRequestFilter
{
    public string FilterKey { get; set; }
    public List<string> Values { get; set; }
}

public class PagedList<T>
{
    public int CurrentPage { get; set; }
    public int TotalPages { get; set; }
    public int PageSize { get; set; }
    public int TotalCount { get; set; }
    public List<T> Values { get; set; } = new List<T>();
    public bool HasPrevious => CurrentPage > 1;
    public bool HasNext => CurrentPage < TotalPages;

    public PagedList(List<T> items, int count, int pageNumber, int pageSize)
    {
        TotalCount = count;
        PageSize = pageSize;
        CurrentPage = pageNumber;
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        Values.AddRange(items);
    }

    public static PagedList<T> ToPagedList(IQueryable<T> source, int pageNumber, int pageSize, int countItem)
    {
        var count = countItem;
        var items = new List<T>();
        if (source.Count() <= pageSize)
        {
            items.AddRange(source.ToList());
        }
        else
        {
            items.AddRange(source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList());
        }

        return new PagedList<T>(items, count, pageNumber, pageSize);
    }
}