using System.Security.Claims;
using HSLogistics.API.Domains.Constants;

namespace HSLogistics.API.Domains.Extensions;

public static class HttpContextExtension
{
    public static string GetCurrentUserId(this HttpContext httpContext)
    {
        if (httpContext.User.Identity is ClaimsIdentity claimsIdentity)
        {
            var result = claimsIdentity.FindFirst(CustomClaimTypes.UserId);
            return result?.Value ?? "";
        }
        return "";
    }

    public static bool IsAdmin(this HttpContext httpContext)
    {
        if (httpContext.User.Identity is ClaimsIdentity claimsIdentity)
        {
            var result = claimsIdentity.HasClaim(CustomClaimTypes.Role, UserRoles.Admin);
            return result;
        }
        return false;
    }
}