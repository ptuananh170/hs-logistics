using Microsoft.AspNetCore.Mvc;

namespace HSLogistics.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class APIBaseController(IHttpContextAccessor contextAccessor) : ControllerBase
{
    private readonly IHttpContextAccessor _contextAccessor = contextAccessor;
}