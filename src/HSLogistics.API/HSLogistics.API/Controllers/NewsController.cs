using AutoMapper;
using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HSLogistics.API.Controllers;

public class NewsController(IHttpContextAccessor contextAccessor,
    IWebHostEnvironment webHostEnvironment,
    INewService newService,
    IMapper mapper) : APIBaseController(contextAccessor)
{
    private readonly IWebHostEnvironment _environment = webHostEnvironment;
    private readonly INewService _newService = newService;
    private readonly IMapper _mapper = mapper;

    [HttpPost("create-new")]
    [Authorize(Roles = UserRoles.Admin)]
    public async Task<APIResponseModel> CreateNew([FromForm] NewModel model)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
                return new APIResponseModel()
                {
                    StatusCode = 400,
                    Data = errors,
                    IsSuccess = false,
                    Message = string.Join(";", errors)
                };
            }

            var result = await _newService.CreateNewAsync(model);
            return result;
        }
        catch (Exception ex)
        {
            return new APIResponseModel()
            {
                Message = ex.Message,
                StatusCode = StatusCodes.Status500InternalServerError,
                IsSuccess = false
            };
        }


    }

    [HttpDelete("delete-new/{newId}")]
    [Authorize(Roles = UserRoles.Admin)]
    public async Task<APIResponseModel> DeleteNew([FromRoute] Guid newId)
    {
        try
        {
            var result = await _newService.DeleteNewAsync(newId);
            return result;
        }
        catch (Exception ex)
        {
            return new APIResponseModel()
            {
                Message = ex.Message,
                StatusCode = StatusCodes.Status500InternalServerError,
                IsSuccess = false
            };
        }
    }

    [HttpPut("update-new")]
    [Authorize(Roles = UserRoles.Admin)]
    public async Task<APIResponseModel> UpdateNew([FromForm] NewModel model)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
                return new APIResponseModel()
                {
                    StatusCode = 400,
                    Data = errors,
                    IsSuccess = false,
                    Message = string.Join(";", errors)
                };
            }

            var result = await _newService.UpdateNewAsync(model);
            return result;
        }
        catch (Exception ex)
        {
            return new APIResponseModel()
            {
                Message = ex.Message,
                StatusCode = StatusCodes.Status500InternalServerError,
                IsSuccess = false
            };
        }
    }

    [HttpGet("get-detail/{newId}")]
    public async Task<APIResponseModel> GetNewById([FromRoute] Guid newId)
    {
        try
        {
            var result = new APIResponseModel()
            {
                StatusCode = StatusCodes.Status200OK,
                IsSuccess = true,
            };
            var entity = await _newService.GetNewById(newId);
            var data = _mapper.Map<NewListModel>(entity);
            result.Data = data;
            return result;
        }
        catch (Exception ex)
        {
            return new APIResponseModel()
            {
                Message = ex.Message,
                StatusCode = StatusCodes.Status500InternalServerError,
                IsSuccess = false
            };
        }
    }

    [HttpPost("all-news")]
    public async Task<PagedList<NewListModel>> GetAllNewsAdmin([FromBody] TableRequestParameter parameter)
    {
        try
        {
            var allNews = await _newService.GetAllNewsWithPaging(parameter);
            var listAllNewModel = _mapper.Map<List<NewListModel>>(allNews.Values);
            var result = new PagedList<NewListModel>(listAllNewModel, allNews.TotalCount,
                allNews.CurrentPage, allNews.PageSize);
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            var errors = new PagedList<NewListModel>(new List<NewListModel>(), 0, 0, 0);
            return errors;
        }
    }

}