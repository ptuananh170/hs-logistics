﻿using AutoMapper;
using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HSLogistics.API.Controllers
{
    public class ContactController : APIBaseController
    {
        private readonly IContactService _contactService;
        private readonly IMapper _mapper;
        public ContactController(IHttpContextAccessor contextAccessor,
            IContactService contactService, IMapper mapper) : base(contextAccessor)
        {
            _contactService = contactService;
            _mapper = mapper;
        }

        [HttpPost("create-contact")]
        public async Task<APIResponseModel> CreateContact([FromBody] ContactModel contactModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Values.SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage)
                        .ToList();
                    return new APIResponseModel()
                    {
                        StatusCode = 400,
                        Data = errors,
                        IsSuccess = false,
                        Message = string.Join(";", errors)
                    };
                }
                var entity = _mapper.Map<Contact>(contactModel);
                var resultAdd = await _contactService.CreateAsync(entity);
                return new APIResponseModel()
                {
                    Data = contactModel,
                    IsSuccess = resultAdd != null,
                    StatusCode = StatusCodes.Status200OK
                };
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    Message = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    IsSuccess = false
                };
            }
        }

        [HttpDelete("delete-contact/{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<APIResponseModel> Delete([FromRoute] Guid id)
        {
            try
            {
                var resultDel = await _contactService.DeleteAsync(id);
                return new APIResponseModel()
                {
                    Data = id,
                    IsSuccess = resultDel != null,
                    StatusCode = StatusCodes.Status200OK
                };
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    Message = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    IsSuccess = false
                };
            }
        }

        [HttpPost("all-contacts")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<PagedList<ContactModel>> GetAll([FromBody] TableRequestParameter tableRequestParameter)
        {
            try
            {
                var allContacts = await _contactService.GetAllWithPagingAsync(tableRequestParameter);
                var listContactsModel = _mapper.Map<List<ContactModel>>(allContacts.Values);
                var result = new PagedList<ContactModel>(listContactsModel, allContacts.TotalCount,
                    allContacts.CurrentPage, allContacts.PageSize);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                var errors = new PagedList<ContactModel>(new List<ContactModel>(), 0, 0, 0);
                return errors;
            }
        }

    }
}
