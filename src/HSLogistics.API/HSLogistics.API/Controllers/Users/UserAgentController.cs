﻿using AutoMapper;
using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HSLogistics.API.Controllers.Users
{
    public class UserAgentController(IHttpContextAccessor contextAccessor,
    IMapper mapper,
    IUserAgentService userAgentService) : APIBaseController(contextAccessor)
    {
        private readonly IMapper _mapper = mapper;
        private readonly IUserAgentService _userAgentService = userAgentService;

        [HttpPost("create-agent")]
        public async Task<APIResponseModel> CreateUserAgent([FromBody] UserAgentModel agentModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Values.SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage)
                        .ToList();
                    return new APIResponseModel()
                    {
                        StatusCode = 400,
                        Data = errors,
                        IsSuccess = false,
                        Message = string.Join(";", errors)
                    };
                }

                var result = await _userAgentService.CreateUserAgentAsync(
                    _mapper.Map<UserAgent>(agentModel)
                    );
                return result;
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    Message = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    IsSuccess = false
                };
            }
        }

        [HttpPost("all-agents")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<PagedList<UserAgentModel>> GetAll([FromBody] TableRequestParameter tableRequestParameter)
        {
            try
            {
                var allUserAgents = await _userAgentService.GetAllWithPagingAsync(tableRequestParameter);
                var listAllUserAgentsModel = _mapper.Map<List<UserAgentModel>>(allUserAgents.Values);

                var result = new PagedList<UserAgentModel>(listAllUserAgentsModel, allUserAgents.TotalCount,
                    allUserAgents.CurrentPage, allUserAgents.PageSize);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                var errors = new PagedList<UserAgentModel>(new List<UserAgentModel>(), 0, 0, 0);
                return errors;
            }
        }

        [HttpGet("detail-agent/{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<UserAgentModel> GetUserAgentById([FromRoute] Guid id)
        {
            try
            {
                var entity = await _userAgentService.GetByIdAsync(id);
                var result = _mapper.Map<UserAgentModel>(entity);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        [HttpDelete("delete-agent/{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<APIResponseModel> DeleteUserAgent([FromRoute] Guid id)
        {
            try
            {
                var result = await _userAgentService.DeleteUserAgent(id);
                return result;
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    Message = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    IsSuccess = false
                };
            }
        }

        [HttpGet("update-status/{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<APIResponseModel> UpdateStatus([FromRoute] Guid id, UserAgentStatus status)
        {
            try
            {
                var result = await _userAgentService.UpdateStatus(status, id);
                return result;
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    Message = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    IsSuccess = false
                };
            }
        }


    }
}
