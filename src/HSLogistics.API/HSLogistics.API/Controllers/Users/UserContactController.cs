﻿using AutoMapper;
using ClosedXML.Excel;
using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace HSLogistics.API.Controllers.Users
{
    public class UserContactController : APIBaseController
    {
        private readonly IUserContactService _userContactService;
        private readonly IMapper _mapper;

        public UserContactController(IHttpContextAccessor contextAccessor,
            IUserContactService userContactService,
            IMapper mapper) : base(contextAccessor)
        {
            _userContactService = userContactService;
            _mapper = mapper;
        }

        [HttpPost("create/user-contact")]
        public async Task<APIResponseModel> CreateUserContact([FromBody] UserContactDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Values.SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage)
                        .ToList();
                    return new APIResponseModel()
                    {
                        StatusCode = 400,
                        Data = errors,
                        IsSuccess = false,
                        Message = string.Join(";", errors)
                    };
                }

                var entity = _mapper.Map<UserContact>(model);
                var result = await _userContactService.CreateUserContact(entity);
                return result;
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    StatusCode = StatusCodes.Status409Conflict,
                    Data = model,
                    Message = ex.Message,
                    IsSuccess = false
                };
            }
        }

        [HttpPost("all-user-contacts")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<PagedList<UserContactDto>> GetAllUserContacts([FromBody] TableRequestParameter parameter)
        {
            try
            {
                var userContacts = await _userContactService.GetAllWithPagingAsync(parameter);
                var listUerContactsDto = _mapper.Map<List<UserContactDto>>(userContacts.Values);
                var result = new PagedList<UserContactDto>(listUerContactsDto, userContacts.TotalCount,
                    userContacts.CurrentPage, userContacts.PageSize);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                var errors = new PagedList<UserContactDto>(new List<UserContactDto>(), 0, 0, 0);
                return errors;
            }
        }

        [HttpPost("download-excel-user-contacts")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> DownloadUserContactsToExcel([FromBody] TableRequestParameter requestParameter)
        {
            try
            {
                var userContacts = await GetAllUserContacts(requestParameter);
                var dt = GetDataTableForUserContacts(userContacts.Values);
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.ColumnWidth = 30;
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"Export_UserContact_{DateTime.UtcNow.Ticks}.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        [HttpDelete("delete-user-contact/{id}")]
        [Authorize(Roles = UserRoles.Admin)]

        public async Task<APIResponseModel> Delete([FromRoute] Guid id)
        {
            try
            {
                var result = await _userContactService.DeleteAsync(id);
                return new APIResponseModel()
                {
                    Data = id,
                    IsSuccess = result != null,
                    StatusCode = StatusCodes.Status200OK
                };
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    StatusCode = StatusCodes.Status409Conflict,
                    Message = ex.Message,
                    IsSuccess = false
                };
            }
        }


        #region private method
        private DataTable GetDataTableForUserContacts(List<UserContactDto> values)
        {
            DataTable dt = new DataTable("UserContacts");
            dt.Columns.AddRange(new DataColumn[12]
            {
            new DataColumn("No"),
            new DataColumn("FullName"),
            new DataColumn("EmailAddress"),
            new DataColumn("Subject"),
            new DataColumn("PickUpCity"),
            new DataColumn("DeliverCity"),
            new DataColumn("Freight"),
            new DataColumn("Incoterms"),
            new DataColumn("Width"),
            new DataColumn("Height"),
            new DataColumn("Length"),
            new DataColumn("Weight")
            });
            int i = 1;
            foreach (var user in values)
            {
                var freight = user.Freight.ToString();
                var incoterms = user.Incoterms.ToString();
                dt.Rows.Add(
                    i,
                    user.FullName,
                    user.EmailAddress,
                    user.Subject,
                    user.PickUpCity,
                    user.DeliverCity,
                    freight, incoterms,
                    user.Width,
                    user.Height,
                    user.Length,
                    user.Weight
                );
                i++;
            }

            return dt;
        }
        #endregion

    }
}
