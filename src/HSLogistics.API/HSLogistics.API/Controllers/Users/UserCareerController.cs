﻿using AutoMapper;
using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HSLogistics.API.Controllers.Users
{
    public class UserCareerController : APIBaseController
    {

        private readonly IMapper _mapper;
        private readonly IUserCareerService _userCareerService;

        public UserCareerController(IHttpContextAccessor contextAccessor,
            IMapper mapper,
            IUserCareerService userCareerService) : base(contextAccessor)
        {
            _mapper = mapper;
            _userCareerService = userCareerService;
        }

        [HttpPost("create-user-career")]
        public async Task<APIResponseModel> CreateUserCareer([FromForm] UserCareerModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Values.SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage)
                        .ToList();
                    return new APIResponseModel()
                    {
                        StatusCode = 400,
                        Data = errors,
                        IsSuccess = false,
                        Message = string.Join(";", errors)
                    };
                }
                var result = await _userCareerService.CreateUserCareer(model);
                return result;
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    Message = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    IsSuccess = false
                };
            }
        }


        [HttpGet("detail-career/{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<UserCareerListModel> GetById([FromRoute] Guid id)
        {
            try
            {
                var entity = await _userCareerService.GetByIdAsync(id);
                var result = _mapper.Map<UserCareerListModel>(entity);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpDelete("delete-career/{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<APIResponseModel> Delete([FromRoute] Guid id)
        {
            try
            {
                var result = await _userCareerService.DeleteUserCareer(id);
                return result;
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    Message = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    IsSuccess = false
                };
            }
        }

        [HttpPost("all-careers")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<PagedList<UserCareerListModel>> GetAll([FromBody] TableRequestParameter tableRequestParameter)
        {
            try
            {
                var allUserAgents = await _userCareerService.GetAllWithPagingAsync(tableRequestParameter);
                var listAllUserAgentsModel = _mapper.Map<List<UserCareerListModel>>(allUserAgents.Values);

                var result = new PagedList<UserCareerListModel>(listAllUserAgentsModel, allUserAgents.TotalCount,
                    allUserAgents.CurrentPage, allUserAgents.PageSize);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                var errors = new PagedList<UserCareerListModel>(new List<UserCareerListModel>(), 0, 0, 0);
                return errors;
            }
        }


        [HttpGet("download-file/{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> DownloadCVFile([FromRoute] Guid id)
        {
            try
            {
                var career = await _userCareerService.GetByIdAsync(id);
                if (career == null)
                {
                    return NotFound("Cannot find career to download");
                }

                if (string.IsNullOrEmpty(career.CVFile))
                {
                    return NotFound("Cannot find cv file of career to download");
                }

                var folderPath = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                var filePath = Path.Combine(folderPath, career.CVFile);

                var memoryStream = new MemoryStream();
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    await stream.CopyToAsync(memoryStream);
                }

                memoryStream.Position = 0;

                return File(memoryStream, "application/pdf", $"CV_{career.FirstName} + {career.LastName}.pdf");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
