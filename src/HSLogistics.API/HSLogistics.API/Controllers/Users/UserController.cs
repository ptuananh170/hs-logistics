using AutoMapper;
using HSLogistics.API.Domains.Extensions;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.IdentityModel.Tokens.Jwt;

namespace HSLogistics.API.Controllers.Users;

public class UserController(
    IHttpContextAccessor contextAccessor,
    IUserService userService,
    IMapper mapper,
    IMemoryCache memoryCache) : APIBaseController(contextAccessor)
{
    private readonly IUserService _userService = userService;
    private readonly IMapper _mapper = mapper;
    private readonly IMemoryCache _memoryCache = memoryCache;

    [HttpPost("login")]
    public async Task<APIResponseModel> Login([FromBody] UserLoginModel loginModel)
    {
        try
        {
            var result = await _userService.Login(loginModel);
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new APIResponseModel()
            {
                Message = ex.Message,
                IsSuccess = false,
                StatusCode = StatusCodes.Status409Conflict
            };
        }
    }

    //[HttpGet("test")]
    //[Authorize]
    //public IActionResult TestHttpContext()
    //{
    //    var currentUserId = contextAccessor.HttpContext.GetCurrentUserId();
    //    var isAdmin = contextAccessor.HttpContext.IsAdmin();
    //    return Ok($"currentUserId: {currentUserId} and isAdmin: {isAdmin}");
    //}

    [HttpGet("logout")]
    public IActionResult Logout()
    {
        try
        {
            var token = Request.Headers["Authorization"].ToString().Replace("Bearer ", "");

            Response.Cookies.Delete("accessToken");

            var expiration = GetTokenExpiration(token);

            if (expiration.HasValue)
            {
                MemoryCacheEntryOptions cacheOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = expiration.Value
                };
                _memoryCache.Set(token, true, cacheOptions);
            }

            return Ok("Logout success");
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    private DateTime? GetTokenExpiration(string token)
    {
        var jwtTokenHandler = new JwtSecurityTokenHandler();
        var jwtToken = jwtTokenHandler.ReadToken(token) as JwtSecurityToken;
        return jwtToken?.ValidTo;
    }

}