using System.Text;
using AspNetCoreRateLimit;
using HSLogistics.API.Common;
using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Middleware;
using HSLogistics.API.Repository;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using HSLogistics.API.Repository.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace HSLogistics.API;

public class Startup(IConfiguration configuration)
{
    private IConfiguration Configuration { get; } = configuration;

    public void ConfigureServices(IServiceCollection services, IWebHostEnvironment env)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddCors(
            p => p.AddDefaultPolicy(build => { build.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); }));
        services.AddHealthChecks();
        services.AddHttpContextAccessor();

        AddRateLimtAPI(services);

        #region add dbcontext

        services.AddIdentity<UserIdentity, IdentityRole>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
                options.User.RequireUniqueEmail = true;
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.AllowedForNewUsers = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
            }).AddEntityFrameworkStores<CoreDbContext>()
            .AddDefaultTokenProviders();
        services.AddIdentityCore<UserIdentity>();
        services.AddDbContext<CoreDbContext>(options =>
        {
            options.UseNpgsql(Configuration.GetConnectionString(ConfigurationKeys.CoreDbConnectionString));
        });

        #endregion

        #region add JWT

        services.AddSwaggerGen(c =>
        {
            var securityScheme = new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                Scheme = "bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Description = "Bearer token for JWT Authorization",
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = JwtBearerDefaults.AuthenticationScheme
                }
            };
            c.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, securityScheme);

            var securityRequirement = new OpenApiSecurityRequirement
            {
                {
                    securityScheme,
                    new string[] { }
                }
            };
            c.AddSecurityRequirement(securityRequirement);
        });
        
        services.AddAuthentication();
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        InjectDependencyServices(services, env);
        
        services.Configure<JWTSettingModel>(Configuration.GetSection(ConfigurationKeys.JWTSetting));
        var secertKey = Configuration[ConfigurationKeys.JWTSettingSecretKey];
        var secretKeyBytes = Encoding.UTF8.GetBytes(secertKey
                                                    ?? throw new ArgumentNullException(
                                                        nameof(ConfigurationKeys.JWTSettingSecretKey)));
        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(secretKeyBytes),
                ClockSkew = TimeSpan.Zero,
            };
        });

        #endregion
        
    }


    private void AddRateLimtAPI(IServiceCollection services)
    {
        services.AddMemoryCache();
        services.Configure<IpRateLimitOptions>(options =>
        {
            options.EnableEndpointRateLimiting = true;
            options.StackBlockedRequests = false;
            options.HttpStatusCode = 429;
            options.RealIpHeader = "X-Real-IP";
            options.ClientIdHeader = "X-ClientId";
            options.GeneralRules = CommonUtil.GetAllRateLimitRules();
        });
        services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
        services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
        services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
        services.AddSingleton<IProcessingStrategy, AsyncKeyLockProcessingStrategy>();
        services.AddInMemoryRateLimiting();
    }

    private void InjectDependencyServices(IServiceCollection services, IWebHostEnvironment env)
    {
        services.AddSingleton<IHostEnvironment>(env);
        services.AddScoped<IDataBuilderService, DataBuilderService>();
        services.AddTransient<IUserService, UserService>();
        services.AddTransient<INewService, NewService>();
        services.AddTransient<IFileStorageService, FileStorageService>();
        services.AddTransient<IUserCareerService, UserCareerService>();
        services.AddTransient<IUserAgentService, UserAgentService>();
        services.AddTransient<IUserContactService, UserContactService>();
        services.AddTransient<IContactService, ContactService>();
    }

    public void Configure(WebApplication app, IWebHostEnvironment env)
    {
        if (!env.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            app.UseHsts();
        }
        else
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "HSLogistics.API"); });
        }

        app.UseCors(build => { build.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseIpRateLimiting();
        app.MapControllers();
        app.UseRouting();
        app.UseMiddleware<BlacklistMiddleware>();

        app.UseAuthentication();
        app.UseAuthorization();
        app.MapHealthChecks("/health", new HealthCheckOptions
        {
            ResultStatusCodes =
            {
                [HealthStatus.Healthy] = StatusCodes.Status200OK,
                [HealthStatus.Degraded] = StatusCodes.Status200OK,
                [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable,
            },
            AllowCachingResponses = true,
        });

        #region run data builder

        var scope = app.Services.CreateScope();
        var dataBuilderService = scope.ServiceProvider.GetService<IDataBuilderService>();
        if (dataBuilderService != null)
        {
            dataBuilderService.RunAsync().GetAwaiter().GetResult();
        }

        #endregion
    }
}