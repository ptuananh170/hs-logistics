﻿using AspNetCoreRateLimit;
using HSLogistics.API.Domains.Constants;
using System.Text.RegularExpressions;

namespace HSLogistics.API.Common
{
    public static class CommonUtil
    {
        public static byte[] ConvertFileToURL(IFormFile file)
        {
            string urlBase = "";
            if (file.ContentType.Equals("image/jpeg"))
            {
                urlBase = "data:image/jpeg;base64,";
            }
            if (file.ContentType.Equals("image/png"))
            {
                urlBase = "data:image/png;base64,";
            }
            if (string.IsNullOrEmpty(urlBase))
            {
                throw new Exception("We only support jpeg and png for upload image!");
            }
            if (file != null && file.Length > 0)
            {
                byte[] imageData = null;
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    return imageData = ms.ToArray();
                }
            }
            return null;
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // This regex pattern matches most valid email formats
                string pattern = @"^[^@\s]+@[^@\s]+\.[^@\s]+$";
                Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                return regex.IsMatch(email);
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
                return false;

            try
            {
                // This regex pattern matches most common phone number formats
                string pattern = @"^\+?[1-9]\d{0,2}[-. ]?\(?\d{1,4}\)?[-. ]?\d{1,4}[-. ]?\d{1,4}[-. ]?\d{1,9}$";
                Regex regex = new Regex(pattern);
                return regex.IsMatch(phoneNumber);
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }


        public static List<RateLimitRule> GetAllRateLimitRules()
        {
            var result = new List<RateLimitRule>();

            result.Add(new RateLimitRule()
            {
                Endpoint = APINeedToLimitConstants.CreateUserAgent,
                Period = CommonConstants.TimeLimitPeriodAPI,
                Limit = CommonConstants.TimeLimitAPI,
            });

            result.Add(new RateLimitRule()
            {
                Endpoint = APINeedToLimitConstants.CreateUserContact,
                Period = CommonConstants.TimeLimitPeriodAPI,
                Limit = CommonConstants.TimeLimitAPI,
            });

            result.Add(new RateLimitRule()
            {
                Endpoint = APINeedToLimitConstants.CreateUserCareer,
                Period = CommonConstants.TimeLimitPeriodAPI,
                Limit = CommonConstants.TimeLimitAPI,
            });

            result.Add(new RateLimitRule()
            {
                Endpoint = APINeedToLimitConstants.CreateContact,
                Period = CommonConstants.TimeLimitPeriodAPI,
                Limit = CommonConstants.TimeLimitAPI,
            });

            return result;
        }

    }
}
