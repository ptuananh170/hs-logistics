﻿using Microsoft.Extensions.Caching.Memory;

namespace HSLogistics.API.Middleware
{
    public class BlacklistMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMemoryCache _cache;

        public BlacklistMiddleware(RequestDelegate next, IMemoryCache cache)
        {
            _next = next;
            _cache = cache;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");

            if (_cache.TryGetValue(token, out _))
            {
                context.Response.StatusCode = 401;
                return;
            }

            await _next(context);
        }
    }

}
