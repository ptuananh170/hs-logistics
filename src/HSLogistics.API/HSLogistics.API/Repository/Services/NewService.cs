﻿using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HSLogistics.API.Repository.Services
{
    public class NewService : INewService
    {
        private readonly CoreDbContext _context;
        private readonly IFileStorageService _fileStorageService;
        public NewService(CoreDbContext context, IFileStorageService fileStorageService)
        {
            _context = context;
            _fileStorageService = fileStorageService;
        }

        public async Task<APIResponseModel> CreateNewAsync(NewModel model)
        {
            var entity = new New()
            {
                PublishedTime = DateTime.UtcNow.Ticks,
                CreatedDate = DateTime.UtcNow.Ticks,
                UpdatedDate = DateTime.UtcNow.Ticks,
                Title = model.Title,
                Content = model.Content,
                Description = model.Description
            };
            if (model.CoverImage == null)
            {
                return new APIResponseModel()
                {
                    Message = "Cover image cannot be null",
                    Data = model,
                    IsSuccess = false,
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
            entity.CoverImage = await _fileStorageService.UploadFileAsync(model.CoverImage);

            await _context.AddAsync<New>(entity);
            var resultAdd = await _context.SaveChangesAsync();
            return new APIResponseModel()
            {
                IsSuccess = resultAdd > 0,
                Data = entity,
                StatusCode = StatusCodes.Status200OK
            };
        }

        public async Task<APIResponseModel> DeleteNewAsync(Guid newId)
        {
            var result = new APIResponseModel()
            {
                IsSuccess = false,
                Data = null,
            };
            var newEntity = await GetNewById(newId);
            if (newEntity == null)
            {
                result.Message = "Cannot find new to delete";
                result.Data = newId;
                result.StatusCode = StatusCodes.Status404NotFound;
                return result;
            }

            _context.News.Remove(newEntity);
            var resultDelete = await _context.SaveChangesAsync();
            result.IsSuccess = resultDelete > 0;
            result.StatusCode = StatusCodes.Status200OK;
            result.Data = newEntity;

            _fileStorageService.DeleteFile(newEntity.CoverImage);

            return result;
        }

        public async Task<APIResponseModel> UpdateNewAsync(NewModel model)
        {
            
            if (model.Id == null || model.Id == Guid.Empty)
            {
                return new APIResponseModel()
                {
                    Message = "Id cannot be null",
                    Data = model,
                    IsSuccess = false,
                    StatusCode = StatusCodes.Status404NotFound
                };
            }

            var oldEntity = await GetNewById(model.Id.Value);

            if (oldEntity == null)
            {
                return new APIResponseModel()
                {
                    Message = "cannot find new entity to update",
                    Data = model,
                    IsSuccess = false,
                    StatusCode = StatusCodes.Status404NotFound
                };
            }

            var newEntity = new New()
            {
                Id = model.Id.Value,
                CreatedDate = oldEntity.CreatedDate,
                UpdatedDate = DateTime.UtcNow.Ticks,
                PublishedTime = DateTime.UtcNow.Ticks,
                Title = model.Title,
                Content = model.Content,
                CoverImage = oldEntity.CoverImage,
                Description = model.Description
            };

            if (model.IsUpdateCoverImage.Value)
            {
                if (model.CoverImage == null)
                {
                    return new APIResponseModel()
                    {
                        Message = "Cover image cannot be null",
                        Data = model,
                        IsSuccess = false,
                        StatusCode = StatusCodes.Status400BadRequest
                    };
                }
                _fileStorageService.DeleteFile(oldEntity.CoverImage);
                var newCoverImage = await _fileStorageService.UploadFileAsync(model.CoverImage);
                newEntity.CoverImage = newCoverImage;
            }

            _context.Update<New>(newEntity);

            var resultUpdate = await _context.SaveChangesAsync();

            return new APIResponseModel()
            {
                Data = newEntity,
                IsSuccess = resultUpdate > 0,
                StatusCode = StatusCodes.Status200OK,
            };
        }

        public async Task<New> GetNewById(Guid newId)
        {
            var result = await _context.News.AsNoTracking().FirstOrDefaultAsync(x => x.Id == newId);
            if (result == null)
                throw new ArgumentNullException("News not exist");
            return result;
        }

        public async Task<PagedList<New>> GetAllNewsWithPaging(TableRequestParameter parameter)
        {
            var newQuery = _context.News.AsQueryable();

            //search by title
            if (!string.IsNullOrEmpty(parameter.SearchContent))
            {
                parameter.SearchContent = parameter.SearchContent.ToLower();
                newQuery = newQuery.Where(u => u.Title.ToLower().Contains(parameter.SearchContent));
            }

            newQuery = newQuery.OrderByDescending(n => n.PublishedTime);

            var totalCount = await newQuery.CountAsync();

            var result =
                PagedList<New>.ToPagedList(newQuery, parameter.PageIndex, parameter.PageSize, totalCount);

            return await Task.FromResult(result);
        }
    }
}
