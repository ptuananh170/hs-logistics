﻿using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HSLogistics.API.Repository.Services
{
    public class UserAgentService : IUserAgentService
    {
        private readonly CoreDbContext _context;
        private readonly IFileStorageService _fileStorageService;

        public UserAgentService(CoreDbContext context, IFileStorageService fileStorageService)
        {
            _context = context;
            _fileStorageService = fileStorageService;
        }

        public Task<UserAgent> CreateAsync(UserAgent entity)
        {
            throw new NotImplementedException();
        }

        public async Task<APIResponseModel> CreateUserAgentAsync(UserAgent userAgentModel)
        {
            await _context.AddAsync<UserAgent>(userAgentModel);
            var rsAdd = await _context.SaveChangesAsync();
            return new APIResponseModel()
            {
                IsSuccess = rsAdd > 0,
                Data = userAgentModel,
                StatusCode = StatusCodes.Status200OK
            };
        }

        public Task<UserAgent> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<APIResponseModel> UpdateStatus(UserAgentStatus status, Guid id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
            {
                return new APIResponseModel()
                {
                    Data = id,
                    IsSuccess = false,
                    Message = "cannot find user career",
                    StatusCode = StatusCodes.Status404NotFound
                };
            }
            entity.Status = status;
            entity.UpdatedDate = DateTime.UtcNow.Ticks;
            
            _context.Update<UserAgent>(entity);
            var resultUpdate = await _context.SaveChangesAsync();
            return new APIResponseModel()
            {
                Data = entity,
                IsSuccess = resultUpdate > 0,
                StatusCode = StatusCodes.Status200OK
            };
        }

        public async Task<APIResponseModel> DeleteUserAgent(Guid id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
            {
                return new APIResponseModel()
                {
                    Data = id,
                    IsSuccess = false,
                    Message = "cannot find user career",
                    StatusCode = StatusCodes.Status404NotFound
                };
            }
            _context.Remove<UserAgent>(entity);
            var resultDel = await _context.SaveChangesAsync();
            return new APIResponseModel()
            {
                Data = entity,
                IsSuccess = resultDel > 0,
                StatusCode = StatusCodes.Status200OK
            };
        }

        public async Task<PagedList<UserAgent>> GetAllWithPagingAsync(TableRequestParameter tableRequestParameter)
        {
            var userAgentQuery = _context.UserAgents.AsQueryable();

            userAgentQuery = userAgentQuery.OrderByDescending(n => n.CreatedDate);

            var totalCount = await userAgentQuery.CountAsync();

            var result =
                PagedList<UserAgent>.ToPagedList(userAgentQuery, tableRequestParameter.PageIndex, tableRequestParameter.PageSize, totalCount);

            return await Task.FromResult(result);
        }

        public async Task<UserAgent> GetByIdAsync(Guid id)
        {
            var result = await _context.UserAgents.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
            {
                throw new ArgumentNullException("Cannot find user agent!");
            }
            return result;
        }

        public Task<UserAgent> UpdateAsync(UserAgent entity)
        {
            throw new NotImplementedException();
        }
    }
}
