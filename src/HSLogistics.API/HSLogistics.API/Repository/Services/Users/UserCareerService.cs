﻿using AutoMapper;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HSLogistics.API.Repository.Services
{
    public class UserCareerService : IUserCareerService
    {
        private readonly CoreDbContext _coreDbContext;
        private readonly IMapper _map;
        private readonly IFileStorageService _fileStorageService;

        public UserCareerService(CoreDbContext coreDbContext,
            IMapper map,
            IFileStorageService fileStorageService)
        {
            _coreDbContext = coreDbContext;
            _map = map;
            _fileStorageService = fileStorageService;
        }

        public Task<UserCareer> CreateAsync(UserCareer entity)
        {
            throw new NotImplementedException();
        }

        public async Task<APIResponseModel> CreateUserCareer(UserCareerModel model)
        {

            var entity = _map.Map<UserCareer>(model);

            var cvFile = await _fileStorageService.UploadFileAsync(model.File);
            entity.CVFile = cvFile;

            await _coreDbContext.AddAsync<UserCareer>(entity);
            var resultAdd = await _coreDbContext.SaveChangesAsync();

            return new APIResponseModel()
            {
                Data = entity,
                IsSuccess = resultAdd > 0,
                StatusCode = StatusCodes.Status200OK
            };

        }

        public async Task<APIResponseModel> DeleteUserCareer(Guid id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
            {
                return new APIResponseModel()
                {
                    Message = "cannot find user career to update",
                    Data = id,
                    IsSuccess = false,
                    StatusCode = StatusCodes.Status404NotFound
                };
            }
            _coreDbContext.Remove(entity);
            _fileStorageService.DeleteFile(entity.CVFile);
            var resultDel = await _coreDbContext.SaveChangesAsync();

            return new APIResponseModel()
            {
                Data = entity,
                IsSuccess = resultDel > 0,
                StatusCode = StatusCodes.Status200OK
            };
        }

        public Task<UserCareer> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<PagedList<UserCareer>> GetAllWithPagingAsync(TableRequestParameter tableRequestParameter)
        {
            var userCareerQuery = _coreDbContext.UserCareers.AsQueryable();

            userCareerQuery = userCareerQuery.OrderByDescending(n => n.CreatedDate);

            var totalCount = await userCareerQuery.CountAsync();

            var result =
                PagedList<UserCareer>.ToPagedList(userCareerQuery, tableRequestParameter.PageIndex, tableRequestParameter.PageSize, totalCount);

            return await Task.FromResult(result);
        }

        public async Task<UserCareer> GetByIdAsync(Guid id)
        {
            var result = await _coreDbContext.UserCareers.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
            {
                throw new ArgumentNullException("cannout find user career");
            }
            return result;
        }

        public Task<UserCareer> UpdateAsync(UserCareer entity)
        {
            throw new NotImplementedException();
        }
    }
}
