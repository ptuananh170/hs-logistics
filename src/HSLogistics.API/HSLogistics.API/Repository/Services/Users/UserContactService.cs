﻿using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HSLogistics.API.Repository.Services
{
    public class UserContactService : IUserContactService
    {

        private readonly CoreDbContext _coreDbContext;
        public UserContactService(CoreDbContext coreDbContext)
        {
            _coreDbContext = coreDbContext;
        }

        public Task<UserContact> CreateAsync(UserContact entity)
        {
            throw new NotImplementedException();
        }

        public async Task<UserContact> DeleteAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);
            if (entity != null)
            {
                _coreDbContext.Remove<UserContact>(entity);
                var resultDel = await _coreDbContext.SaveChangesAsync();
                return resultDel > 0 ? entity : null;
            }
            return null;
        }

        public async Task<UserContact> GetByIdAsync(Guid id)
        {
            var result = await _coreDbContext.UserContacts.FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
            {
                throw new ArgumentNullException("cannot find user contact");
            }
            return result;
        }

        public Task<UserContact> UpdateAsync(UserContact entity)
        {
            throw new NotImplementedException();
        }

        public async Task<APIResponseModel> CreateUserContact(UserContact entity)
        {
            var result = new APIResponseModel()
            {
                StatusCode = StatusCodes.Status200OK,
                Message = "Create user contact successfully",
                Data = entity
            };
            await _coreDbContext.UserContacts.AddAsync(entity);
            var resultAdd = await _coreDbContext.SaveChangesAsync();
            result.IsSuccess = resultAdd > 0;
            return result;
        }

        public async Task<PagedList<UserContact>> GetAllWithPagingAsync(TableRequestParameter parameter)
        {
            var userContactsQuery = _coreDbContext.UserContacts.AsQueryable();

            //search by fullname or email address
            if (!string.IsNullOrEmpty(parameter.SearchContent))
            {
                userContactsQuery = userContactsQuery.Where(u => u.FullName.Contains(parameter.SearchContent) || u.EmailAddress.Contains(parameter.SearchContent));
            }

            var totalCount = await userContactsQuery.CountAsync();

            var result =
                PagedList<UserContact>.ToPagedList(userContactsQuery, parameter.PageIndex, parameter.PageSize, totalCount);

            return await Task.FromResult(result);
        }
    }
}
