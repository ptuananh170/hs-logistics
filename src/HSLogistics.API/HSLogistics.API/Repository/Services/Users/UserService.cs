using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace HSLogistics.API.Repository.Services;

public class UserService(
    IOptionsMonitor<JWTSettingModel> optionsMonitor,
    SignInManager<UserIdentity> signInManager,
    UserManager<UserIdentity> userManager,
    CoreDbContext coreDbContext)
    : IUserService
{
    private readonly JWTSettingModel _jWtSetting = optionsMonitor.CurrentValue;
    private readonly UserManager<UserIdentity> _userManager = userManager;
    private readonly SignInManager<UserIdentity> _signInManager = signInManager;
    private readonly CoreDbContext _coreDbContext = coreDbContext;

    public async Task<APIResponseModel> Login(UserLoginModel loginModel)
    {
        APIResponseModel result = new APIResponseModel()
            {
                Message = UserMessageConstants.LoginSuccessfully,
                Data = loginModel,
                IsSuccess = true,
                StatusCode = (int)HttpStatusCode.OK
            };
            var user = await _userManager.FindByEmailAsync(loginModel.Email);
            if (user != null && user.Status != UserStatus.Active)
            {
                return new APIResponseModel()
                {
                    Message = UserMessageConstants.LoginInactiveUser,
                    Data = loginModel,
                    IsSuccess = false,
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            if (user == null)
            {
                return new APIResponseModel()
                {
                    Message = UserMessageConstants.LoginFailedPassword,
                    Data = loginModel,
                    IsSuccess = false,
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            var loginResult = await _signInManager.CheckPasswordSignInAsync(user, loginModel.Password, lockoutOnFailure: true);
            if (loginResult.Succeeded)
            {
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Message = UserMessageConstants.LoginSuccessfully;
                result.IsSuccess = true;
                result.Data = await GenerateToken(user);
            }
            else if (loginResult.IsLockedOut)
            {
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                result.Message = UserMessageConstants.LockUserMessage;
                result.IsSuccess = false;
                result.Data = loginModel;
            }
            else
            {
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                result.Message = UserMessageConstants.LoginFailedPassword;
                result.IsSuccess = false;
                result.Data = loginModel;
            }
            return result;
    }

    public async Task<string> GenerateToken(UserIdentity user)
    {
        var jwtTokenHandler = new JwtSecurityTokenHandler();
        var secertKeyBytes = Encoding.UTF8.GetBytes(_jWtSetting.SecretKey);
        var roles = await _userManager.GetRolesAsync(user);
        string currentRole = roles[0];
        var tokenDescription = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(CustomClaimTypes.Role, currentRole),
                new Claim(CustomClaimTypes.Email, user.Email ?? ""),
                new Claim(CustomClaimTypes.UserId, user.Id)
            }),
            Expires = DateTime.UtcNow.AddHours(3),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secertKeyBytes), SecurityAlgorithms.HmacSha512Signature)
        };
        var token = jwtTokenHandler.CreateToken(tokenDescription);
        var accessToken = jwtTokenHandler.WriteToken(token);
        return accessToken;
    }

    public async Task SignOutAsync()
    {
        await _signInManager.SignOutAsync();
    }

}