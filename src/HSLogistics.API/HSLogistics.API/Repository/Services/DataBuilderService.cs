using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualBasic;

namespace HSLogistics.API.Repository.Services;

public class DataBuilderService(RoleManager<IdentityRole> roleManager,
    UserManager<UserIdentity> userManager) : IDataBuilderService
{
    private readonly RoleManager<IdentityRole> _roleManager = roleManager;
    private readonly UserManager<UserIdentity> _userManager = userManager;
    
    public async Task RunAsync()
    {
        await CreateRolesDefaultAsync();
        await AddSuperAdminDefaultAsync();
    }
    private async Task CreateRolesDefaultAsync()
    {
        try
        {
            foreach (var role in UserRoles.Roles)
            {
                var existRole = await _roleManager.RoleExistsAsync(role);
                if (!existRole)
                {
                    await _roleManager.CreateAsync(new IdentityRole(role));
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        await Task.CompletedTask;
    }
    private async Task AddSuperAdminDefaultAsync()
    {
        try
        {
            var userEntity = await _userManager.FindByIdAsync(UserAdminDefault.SuperAdminId);
            if (userEntity == null)
            {
                userEntity = new UserIdentity()
                {
                    Id = UserAdminDefault.SuperAdminId,
                    FullName = UserAdminDefault.SuperAdminFullName,
                    Email = UserAdminDefault.SuperAdminEmail,
                    CreatedDate = DateTime.Now.Ticks,
                    Gender = UserGender.None,
                    PhoneNumber = "0000000000",
                    DateOfBirth = DateTime.Now.Ticks,
                    Status = UserStatus.Active,
                    UpdatedDate = DateTime.Now.Ticks,
                    UserName = UserAdminDefault.SuperAdminUserName
                };
                var resultAdd = await _userManager.CreateAsync(userEntity, UserAdminDefault.SuperAdminPassword);
                if (resultAdd.Succeeded)
                {
                    await _userManager.AddToRoleAsync(userEntity, UserRoles.Admin);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        await Task.CompletedTask;
    }
}