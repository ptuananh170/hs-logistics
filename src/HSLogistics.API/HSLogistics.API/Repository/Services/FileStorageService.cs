using AutoMapper;
using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Repository.Interfaces;

namespace HSLogistics.API.Repository.Services;

public class FileStorageService(
    IWebHostEnvironment env,
    CoreDbContext coreDbContext,
    IMapper mapper,
    IConfiguration configuration) : IFileStorageService
{
    private readonly IWebHostEnvironment _env = env;
    private readonly CoreDbContext _coreDbContext = coreDbContext;
    private readonly IMapper _mapper = mapper;
    private readonly IConfiguration _configuration = configuration;

    public async Task<string> UploadFileAsync(IFormFile file)
    {
        var fileId = Guid.NewGuid();
        string realFileName = $"{fileId.ToString().Replace("-", "")}{file.FileName}";

        string sharedImageFolder = "";

        var folderPathConfiguration = _configuration.GetSection("FolderPath").Value;
        if (string.IsNullOrEmpty(folderPathConfiguration))
        {
            sharedImageFolder = Path.Combine(_env.WebRootPath, CommonConstants.ImageFolder);
        }
        else
        {
            sharedImageFolder = folderPathConfiguration;
        }

        if (!Directory.Exists(sharedImageFolder))
        {
            Directory.CreateDirectory(sharedImageFolder);
        }

        string fullImagePath = Path.Combine(sharedImageFolder, realFileName);

        using (FileStream fileStream = new FileStream(fullImagePath, FileMode.Create))
        {
            await file.CopyToAsync(fileStream);
        }

        return realFileName;
    }

    public void DeleteFile(string fileName)
    {
        try
        {
            string folderPath = "";
            var folderPathConfiguration = _configuration.GetSection("FolderPath").Value;
            if (string.IsNullOrEmpty(folderPathConfiguration))
            {
                folderPath = Path.Combine(_env.WebRootPath, CommonConstants.ImageFolder);
            }
            else
            {
                folderPath = folderPathConfiguration;
            }
            var filePath = Path.Combine(folderPath, fileName);

            File.Delete(filePath);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error when delete file" + ex.ToString());
            return;
        }
    }

}