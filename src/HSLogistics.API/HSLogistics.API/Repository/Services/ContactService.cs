﻿using DocumentFormat.OpenXml.InkML;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;
using HSLogistics.API.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HSLogistics.API.Repository.Services
{
    public class ContactService : IContactService
    {

        private readonly CoreDbContext _coreDbContext;
        public ContactService(CoreDbContext coreDbContext)
        {
            _coreDbContext = coreDbContext;
        }

        public async Task<Contact> CreateAsync(Contact entity)
        {
            _coreDbContext.Add<Contact>(entity);
            var resultAdd = await _coreDbContext.SaveChangesAsync();
            if (resultAdd > 0)
            {
                return entity;
            }
            return null;
        }

        public async Task<Contact> DeleteAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
            {
                return null;
            }
            _coreDbContext.Remove(entity);
            var resultDel = await _coreDbContext.SaveChangesAsync();
            if (resultDel > 0)
            {
                return entity;
            }
            return null;
        }

        public async Task<PagedList<Contact>> GetAllWithPagingAsync(TableRequestParameter tableRequestParameter)
        {
            var contactQuery = _coreDbContext.Contacts.AsQueryable();

            contactQuery = contactQuery.OrderByDescending(n => n.CreatedDate);

            var totalCount = await contactQuery.CountAsync();

            var result =
                PagedList<Contact>.ToPagedList(contactQuery, tableRequestParameter.PageIndex, tableRequestParameter.PageSize, totalCount);

            return await Task.FromResult(result);
        }

        public async Task<Contact> GetByIdAsync(Guid id)
        {
            var result = await _coreDbContext.Contacts.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public Task<Contact> UpdateAsync(Contact entity)
        {
            throw new NotImplementedException();
        }



    }
}
