﻿namespace HSLogistics.API.Repository.Entities
{
    public class Contact : BaseEntity
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmailAddress { set; get; }
        public string PhoneNumber { set; get; }
        public string? Notes { set; get; }
        public string Level { set; get; }
        public string Subject { set; get; }
        public string? Company { get; set; }
    }
}
