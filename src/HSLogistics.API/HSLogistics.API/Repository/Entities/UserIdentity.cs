using HSLogistics.API.Domains.Enums;
using Microsoft.AspNetCore.Identity;

namespace HSLogistics.API.Repository.Entities;

public class UserIdentity : IdentityUser
{
    public long DateOfBirth { get; set; }
    public required string FullName { get; set; }
    public long CreatedDate { get; set; }
    public long UpdatedDate { get; set; }
    public UserGender Gender { get; set; }
    public UserStatus Status { get; set; }
}