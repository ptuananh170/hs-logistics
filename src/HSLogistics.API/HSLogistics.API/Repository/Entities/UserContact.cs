using HSLogistics.API.Domains.Enums;

namespace HSLogistics.API.Repository.Entities;

public class UserContact : BaseEntity
{
    public string FullName { get; set; }
    public string EmailAddress { get; set; }
    public string PhoneNumber { get; set; }
    public string Subject { get; set; }
    public string? PickUpCity { get; set; }
    public string? DeliverCity { get; set; }
    public FreightType Freight { get; set; }
    public Incoterms Incoterms { get; set; }
    public double? Width { get; set; }
    public double? Height { get; set; }
    public double? Length { get; set; }
    public double? Weight { get; set; }
}