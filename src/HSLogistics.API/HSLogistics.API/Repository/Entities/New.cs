namespace HSLogistics.API.Repository.Entities;

public class New : BaseEntity
{
    public string Title { get; set; }
    public string Content { get; set; }
    public long PublishedTime { get; set; }
    public string CoverImage { get; set; }
    public string? Description { get; set; }
}