﻿using HSLogistics.API.Domains.Enums;

namespace HSLogistics.API.Repository.Entities
{
    public class UserCareer : BaseEntity
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmailAddress { set; get; }
        public string PhoneNumber { set; get; }
        public string CVFile { set; get; }
        public string? CoverLetter { set; get; }
        public UserCareerStatus Status { set; get; }
    }
}
