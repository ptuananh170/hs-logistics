namespace HSLogistics.API.Repository.Entities;

public class BaseEntity
{
    public Guid Id { get; set; }
    public long CreatedDate { get; set; }
    public long UpdatedDate { get; set; }
    public Guid? CreatedBy { get; set; }
    public Guid? UpdatedBy { get; set; }
}