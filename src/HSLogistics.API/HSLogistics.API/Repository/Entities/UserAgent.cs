﻿using HSLogistics.API.Domains.Enums;

namespace HSLogistics.API.Repository.Entities
{
    public class UserAgent : BaseEntity
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmailAddress { set; get; }
        public string PhoneNumber { set; get; }
        public string? Message { set; get; }
        public UserAgentStatus Status { set; get; }
    }
}
