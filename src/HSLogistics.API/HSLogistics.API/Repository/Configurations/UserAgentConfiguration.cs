﻿using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HSLogistics.API.Repository.Configurations
{
    public class UserAgentConfiguration : IEntityTypeConfiguration<UserAgent>
    {
        public void Configure(EntityTypeBuilder<UserAgent> builder)
        {
            builder.ToTable(TableNameConstants.UserAgent);
            builder.HasKey(x => x.Id).HasName(PrimaryKeyConstants.UserAgent);
            builder.Property(x => x.FirstName).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.LastName).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.EmailAddress).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.PhoneNumber).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.Status).HasDefaultValue(UserAgentStatus.New);
            builder.Property(n => n.UpdatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
            builder.Property(n => n.CreatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
        }
    }
}
