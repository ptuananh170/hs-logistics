﻿using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HSLogistics.API.Repository.Configurations
{
    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.ToTable(TableNameConstants.Contact);
            builder.HasKey(x => x.Id).HasName(PrimaryKeyConstants.PK_Contact);
            builder.Property(x => x.FirstName).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.LastName).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.EmailAddress).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.PhoneNumber).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.Level).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.Subject).IsRequired(true).HasMaxLength(500);
            builder.Property(x => x.Company).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.Notes).IsRequired(false).HasMaxLength(5000);
            builder.Property(n => n.UpdatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
            builder.Property(n => n.CreatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
        }
    }
}
