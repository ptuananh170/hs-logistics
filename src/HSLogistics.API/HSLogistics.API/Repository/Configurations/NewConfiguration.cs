using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HSLogistics.API.Repository.Configurations;

public class NewConfiguration : IEntityTypeConfiguration<New>
{
    public void Configure(EntityTypeBuilder<New> builder)
    {
        builder.ToTable(TableNameConstants.New);
        builder.HasKey(n => n.Id).HasName(PrimaryKeyConstants.PK_New);
        builder.Property(n => n.Content).IsRequired();
        builder.Property(n => n.Title).HasMaxLength(1000).IsRequired();
        builder.Property(n => n.PublishedTime).HasDefaultValue(DateTime.UtcNow.Ticks);
        builder.Property(n => n.UpdatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
        builder.Property(n => n.CreatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
        builder.Property(n => n.CoverImage).IsRequired(false);
        builder.Property(n => n.Description).IsRequired(false).HasMaxLength(500);
    }
}