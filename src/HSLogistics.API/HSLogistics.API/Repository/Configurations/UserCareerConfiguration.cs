﻿using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HSLogistics.API.Repository.Configurations
{
    public class UserCareerConfiguration : IEntityTypeConfiguration<UserCareer>
    {
        public void Configure(EntityTypeBuilder<UserCareer> builder)
        {
            builder.ToTable(TableNameConstants.UserCareer);
            builder.HasKey(x => x.Id).HasName(PrimaryKeyConstants.PK_UserCareer);
            builder.Property(x => x.FirstName).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.LastName).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.EmailAddress).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.PhoneNumber).IsRequired(true).HasMaxLength(250);
            builder.Property(x => x.CoverLetter).IsRequired(false);
            builder.Property(x => x.CVFile).IsRequired(true);
            builder.Property(x => x.Status).HasDefaultValue(UserCareerStatus.New);
            builder.Property(n => n.UpdatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
            builder.Property(n => n.CreatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
        }
    }
}
