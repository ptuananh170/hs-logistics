using HSLogistics.API.Domains.Constants;
using HSLogistics.API.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HSLogistics.API.Repository.Configurations;

public class UserContactConfiguration : IEntityTypeConfiguration<UserContact>
{
    public void Configure(EntityTypeBuilder<UserContact> builder)
    {
        builder.ToTable(TableNameConstants.UserContact);
        builder.HasKey(u => u.Id).HasName(PrimaryKeyConstants.PK_UserContact);
        builder.Property(u => u.Incoterms);
        builder.Property(u => u.Freight);
        builder.Property(u => u.Height).IsRequired(false);
        builder.Property(u => u.Weight).IsRequired(false);
        builder.Property(u => u.Width).IsRequired(false);
        builder.Property(u => u.Length).IsRequired(false);
        builder.Property(u => u.Subject).HasMaxLength(255).IsRequired();
        builder.Property(u => u.EmailAddress).HasMaxLength(255).IsRequired();
        builder.Property(u => u.PhoneNumber).HasMaxLength(255).IsRequired();
        builder.Property(u => u.PickUpCity).HasMaxLength(255).IsRequired(false);
        builder.Property(u => u.DeliverCity).HasMaxLength(255).IsRequired(false);
        builder.Property(u => u.UpdatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
        builder.Property(u => u.CreatedDate).HasDefaultValue(DateTime.UtcNow.Ticks);
    }
}