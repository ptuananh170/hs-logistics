using HSLogistics.API.Domains.Models;

namespace HSLogistics.API.Repository.Interfaces;

public interface IFileStorageService
{
    Task<string> UploadFileAsync(IFormFile file);
    void DeleteFile(string fileName);
}