namespace HSLogistics.API.Repository.Interfaces;

public interface IDataBuilderService
{
    Task RunAsync();
}