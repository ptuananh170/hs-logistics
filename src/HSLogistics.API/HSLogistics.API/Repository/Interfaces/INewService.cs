﻿using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;

namespace HSLogistics.API.Repository.Interfaces
{
    public interface INewService
    {
        Task<APIResponseModel> CreateNewAsync(NewModel model);
        Task<APIResponseModel> DeleteNewAsync(Guid newId);
        Task<APIResponseModel> UpdateNewAsync(NewModel model);
        Task<New> GetNewById(Guid newId);
        Task<PagedList<New>> GetAllNewsWithPaging(TableRequestParameter parameter);
    }
}
