﻿using HSLogistics.API.Repository.Entities;

namespace HSLogistics.API.Repository.Interfaces
{
    public interface IContactService : IBasicService<Contact>
    {

    }
}
