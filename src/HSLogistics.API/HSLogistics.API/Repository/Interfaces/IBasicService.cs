﻿using HSLogistics.API.Domains.Models;

namespace HSLogistics.API.Repository.Interfaces
{
    public interface IBasicService<T> where T : class
    {
        Task<T> CreateAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task<T> DeleteAsync(Guid id);
        Task<T> GetByIdAsync(Guid id);
        Task<PagedList<T>> GetAllWithPagingAsync(TableRequestParameter tableRequestParameter);
    }
}
