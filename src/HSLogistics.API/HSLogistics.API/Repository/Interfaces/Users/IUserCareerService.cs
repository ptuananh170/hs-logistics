﻿using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;

namespace HSLogistics.API.Repository.Interfaces
{
    public interface IUserCareerService : IBasicService<UserCareer>
    {
        Task<APIResponseModel> CreateUserCareer(UserCareerModel model);
        Task<APIResponseModel> DeleteUserCareer(Guid id);
    }
}
