﻿using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;

namespace HSLogistics.API.Repository.Interfaces
{
    public interface IUserContactService : IBasicService<UserContact>
    {
        Task<APIResponseModel> CreateUserContact(UserContact entity);
    }
}
