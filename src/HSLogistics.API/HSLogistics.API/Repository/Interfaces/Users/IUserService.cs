using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;

namespace HSLogistics.API.Repository.Interfaces;

public interface IUserService
{
    Task<APIResponseModel> Login(UserLoginModel loginModel);
    Task<string> GenerateToken(UserIdentity user);
    Task SignOutAsync();
}