﻿using HSLogistics.API.Domains.Enums;
using HSLogistics.API.Domains.Models;
using HSLogistics.API.Repository.Entities;

namespace HSLogistics.API.Repository.Interfaces
{
    public interface IUserAgentService : IBasicService<UserAgent>
    {
        Task<APIResponseModel> CreateUserAgentAsync(UserAgent userAgent);
        Task<APIResponseModel> UpdateStatus(UserAgentStatus status, Guid id);
        Task<APIResponseModel> DeleteUserAgent(Guid id);
    }
}
