using HSLogistics.API.Repository.Configurations;
using HSLogistics.API.Repository.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HSLogistics.API.Repository;

public class CoreDbContext(DbContextOptions<CoreDbContext> options) : IdentityDbContext<UserIdentity>(options)
{
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new UserContactConfiguration());
        modelBuilder.ApplyConfiguration(new NewConfiguration());
        modelBuilder.ApplyConfiguration(new UserCareerConfiguration());
        modelBuilder.ApplyConfiguration(new UserAgentConfiguration());
        modelBuilder.ApplyConfiguration(new ContactConfiguration());
        base.OnModelCreating(modelBuilder);
    }

    #region entities

    public DbSet<UserContact> UserContacts { get; set; }
    public DbSet<New> News { get; set; }
    public DbSet<UserCareer> UserCareers { get; set; }
    public DbSet<UserAgent> UserAgents { get; set; }
    public DbSet<Contact> Contacts { get; set; }

    #endregion
    
}